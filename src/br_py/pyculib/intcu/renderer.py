import math as m
import os
import warnings

import numpy as np
from scipy.interpolate import interp1d, RegularGridInterpolator

try:
    import pycuda.driver as cuda
    from pycuda.compiler import SourceModule

except Exception:  # NOQA: BLE001
    warnings.warn("No cuda version")

BLOCKSIZE = 256
PLANCK = 6.626176e-27
CC = 3.00e8  # m/s
GRPH = 2.38049e-24  # cgs
cm2m = 1e2
ergd2wd = 0.1


class CUDAManipulator(object):
    """
    Class for using CUDA, manipulating objects and textures conveniently.
    """

    def load_texture(self, name, arr):
        """
        Loads an array into a texture with a name.

        Address by the name in the kernel code.
        """
        tex = self.mod.get_texref(name)  # x*y*z
        arr = arr.astype("float32")

        if len(arr.shape) == 3:
            carr = arr.copy("F")
            texarray = numpy3d_to_array(carr, "F")
            tex.set_array(texarray)
        else:
            if len(arr.shape) == 1:
                arr = np.expand_dims(arr, 1)
            tex.set_flags(0)
            cuda.matrix_to_texref(arr, tex, order="F")

        tex.set_address_mode(0, cuda.address_mode.CLAMP)
        tex.set_address_mode(1, cuda.address_mode.CLAMP)
        tex.set_flags(cuda.TRSF_NORMALIZED_COORDINATES)
        tex.set_filter_mode(cuda.filter_mode.LINEAR)
        self.textures[name] = tex

    def load_constant(self, name, val):
        """
        Loads a constant into memory by name in kernel code.

        If val is a float, int, char, etc., it must be wrapped by
        np.float32() or np.int32() or similar.
        """
        cuda.memcpy_htod(self.mod.get_global(name)[0], np.array(val))

    def clear_textures(self):
        """
        Removes all textures from memory.
        """
        self.textures = {}

    def __init__(self, cuda_code, include_dirs=[]):
        include_dirs += [
            os.path.join(os.environ["CUDA_LIB"], "ioni/spectanalys/"),
            os.path.join(os.environ["CUDA_LIB"], "ioni/renderer/"),
        ]
        self.mod = SourceModule(cuda_code, no_extern_c=True, include_dirs=include_dirs)


class Renderer(CUDAManipulator):
    """
    Superclass for rendering 3D models.

    Takes care of most useful functions for 3D rendering, such as
    calculating view vectors, calculating start points for individual
    pixels, determining whether or not in box, splitting up large
    datasets along the x-axis, etc.

    Everything else, e.g. iterating through the spaces and integrating
    must be done by the attached CUDA module and the parameter
    spec_render to render().

    To see the variables provided for use in the CUDA module, see
    renderer.h
    """

    projection_x_size = 640  # size of the output array
    projection_y_size = 640
    distance_per_pixel = (
        0.06  # distance (specified by axes) between pixels in the output
    )

    x_pixel_offset = (
        0  # if 0, the output is centered in the box center--shifts output L/R
    )
    y_pixel_offset = 0  # shifts output U/D

    mod = None  # CUDA kernel code
    xaxis = yaxis = zaxis = ixaxis = iyaxis = izaxis = None
    textures = {}  # stores references to textures to prevent them being cleared
    singlaxis = False

    def render(
        self,
        azimuth,
        altitude,
        axis,
        reverse,
        gridsplit,
        consts,
        tables,
        split_tables,
        spec_render,
        stepsize=0.001,
        verbose=True,
    ):
        """
        Renders with a view of azimuth and altitude. Loads constants (list of
        tuples of name, value), tables (list of tuples of name, value). It
        splits up the rendering space along the x-axis, loading a corresponding
        portion of each table in split_tables, and rendering a portion at a
        time by calling spec_render.

        spec_render is func with format spec_render(self, blocksize,
        gridsize) and is where the CUDA kernel is actually called.
        """
        self.clear_textures()
        view_x, view_y, view_vector = view_axes(azimuth, altitude)
        self.stepsize = stepsize  # size of a step along the LOS
        self.maxgridsize = gridsplit * self.yaxis.size * self.zaxis.size
        self.axis = axis

        # add axis inverse lookup tables, as well as x axis lookup table
        tables.extend(
            [
                ("xtex", np.expand_dims(self.xaxis, 1)),
                ("ixtex", np.expand_dims(self.ixaxis, 1)),
                ("iytex", np.expand_dims(self.iyaxis, 1)),
                ("iztex", np.expand_dims(self.izaxis, 1)),
            ]
        )

        for tup in tables:
            self.load_texture(*tup)
        for tup in consts:
            self.load_constant(*tup)

        if verbose:
            print("Loaded textures, computed emissivities")

        xsplitsize = self.maxgridsize / (self.yaxis.size * self.zaxis.size)
        numsplits = (self.xaxis.size + xsplitsize - 1) / xsplitsize

        self.load_constant("viewVector", np.float32(view_vector))
        self.load_constant("viewX", np.float32(view_x))
        self.load_constant("viewY", np.float32(view_y))
        self.load_constant("ds", np.float32(self.stepsize))
        self.load_constant("projectionXsize", np.int32(self.projection_x_size))
        self.load_constant("projectionYsize", np.int32(self.projection_y_size))
        self.load_constant("distancePerPixel", np.float32(self.distance_per_pixel))
        self.load_constant("xPixelOffset", np.float32(self.x_pixel_offset))
        self.load_constant("yPixelOffset", np.float32(self.y_pixel_offset))

        for i in range(0, numsplits):
            xstart = i * xsplitsize
            if xstart + xsplitsize > self.xaxis.size:
                xsplitsize = self.xaxis.size - xstart

            if verbose:
                print(
                    "Rendering x'-coords "
                    + str(xstart)
                    + "-"
                    + str(xstart + xsplitsize)
                    + " of "
                    + str(self.xaxis.size),
                    end="\r",
                    flush=True,
                )

            for tup in split_tables:
                name, table = tup
                self.load_texture(name, table[xstart : xstart + xsplitsize])

            data_size = self.projection_x_size * self.projection_y_size
            grid_size = (data_size + BLOCKSIZE - 1) / BLOCKSIZE

            self.load_constant("xstart", np.int32(xstart))
            self.load_constant("sliceWidth", np.int32(xsplitsize))

            spec_render(self, BLOCKSIZE, grid_size)

    def __init__(self, cuda_code):
        """
        Initialize with a cuda kernel.

        CUDA kernel should include renderer.h to utilize this
        """
        super(Renderer, self).__init__(cuda_code)

    def set_axes(self, xaxis, yaxis, zaxis):
        """
        Sets the x, y, and z axes.

        x, y, z axes are lists correlating indices (in textures) to
        locations in space.
        """
        self.xaxis = xaxis
        self.yaxis = yaxis
        self.zaxis = zaxis
        self.load_constant("xmin", np.float32(xaxis.min()))
        self.load_constant("xmax", np.float32(xaxis.max()))
        self.load_constant("ymin", np.float32(yaxis.min()))
        self.load_constant("ymax", np.float32(yaxis.max()))
        self.load_constant("zmin", np.float32(zaxis.min()))
        self.load_constant("zmax", np.float32(zaxis.max()))
        self.load_constant("xtotalsize", np.int32(xaxis.size))

        self.ixaxis = norm_inverse_axis(xaxis)
        if len(yaxis) > 1:
            self.iyaxis = norm_inverse_axis(yaxis)
        else:
            self.iyaxis = [0]
        self.izaxis = norm_inverse_axis(zaxis)


class SingAxisRenderer(CUDAManipulator):
    zcutoff = (
        -0.5
    )  # the point at which we are considered to be no longer in the chromosphere
    locph = prev_lambd = float("nan")
    snap = 0

    x_pixel_offset = (
        0  # if 0, the output is centered in the box center--shifts output L/R
    )
    y_pixel_offset = 0  # shifts output U/D
    ##nsteps = 600
    distance_per_pixel = (
        0.06  # distance (specified by axes) between pixels in the output
    )
    mod = None  # CUDA kernel code
    xaxis = yaxis = zaxis = ixaxis = iyaxis = izaxis = None
    singlaxis = True
    ##ux = uy = uz = e = r = oscdata = ka_table = opatab = None
    textures = {}  # stores references to textures to prevent them being cleared

    def render(
        self,
        azimuth,
        altitude,
        axis,
        reverse,
        gridsplit,
        consts,
        tables,
        split_tables,
        spec_render,
        stepsize=0.001,
        verbose=True,
    ):
        self.clear_textures()

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size
        self.stepsize = stepsize  # size of a step along the LOS

        tables.extend(
            [
                ("xtex", np.expand_dims(self.xaxis, 1)),
                ("ixtex", np.expand_dims(self.ixaxis, 1)),
                ("iytex", np.expand_dims(self.iyaxis, 1)),
                ("iztex", np.expand_dims(self.izaxis, 1)),
            ]
        )

        self.axis = axis

        if axis == 0:
            intaxis = self.xaxis
            ax_id = 0
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
            view_x, view_y, view_vector = view_axes(0, 0)  # JMS This is not used
        elif axis == 1:
            intaxis = self.yaxis
            ax_id = 1
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
            view_x, view_y, view_vector = view_axes(90, -90)  # JMS  This is not used
        else:
            intaxis = self.zaxis
            ax_id = 2
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size
            view_x, view_y, view_vector = view_axes(0, 180)  # JMS  This is not used

        self.maxgridsize = (
            gridsplit * self.projection_x_size * self.projection_y_size
        )  # This must be a multiple of xsize and ysize
        numsplits = int(input_size / self.maxgridsize + 1)
        splitsize = np.ceil(intaxis.size / numsplits)

        # dz = np.gradient(intaxis) -> split in numsplitsintaxis size, i.e., splitsize timestep
        # in CUDA ds step size within the texture (0,1), int (dz)_ numsplitsintaxis width of
        # broken z. In cuda
        # table_splits['aptex'] = [np.gradient(intaxis) * np.float32(intaxis.size * stepsize) * 1.0e-6 * bifrost_units.u_l * bifrost_units.ergd2wd for numb in range(numsplits)]

        for tup in tables:
            self.load_texture(*tup)
        for tup in consts:
            self.load_constant(*tup)
        self.load_constant("viewVector", np.float32(view_vector))
        self.load_constant("viewX", np.float32(view_x))
        self.load_constant("viewY", np.float32(view_y))
        self.load_constant("axis", np.uint8(axis))
        self.load_constant("reverse", np.int8(reverse))
        ##self.load_constant('nsteps', np.int32(self.nsteps))
        self.load_constant("projectionXsize", np.int32(self.projection_x_size))
        self.load_constant("projectionYsize", np.int32(self.projection_y_size))
        self.load_constant("distancePerPixel", np.float32(self.distance_per_pixel))
        self.load_constant("xPixelOffset", np.float32(self.x_pixel_offset))
        self.load_constant("yPixelOffset", np.float32(self.y_pixel_offset))
        self.load_constant("ds", np.float32(self.stepsize))

        if verbose:
            print("Loaded textures, computed emissivities")

        # JMS the following 5 lines are not in the other Renderer because change on axis cut.
        # split_tables is tables to split, table_splits is list of split tables
        table_splits = {
            name: np.array_split(table, numsplits, ax_id)
            for name, table in split_tables
        }

        if reverse:
            for name in table_splits:
                table_splits[name].reverse()

        for i in range(0, numsplits):
            start = i * splitsize
            if start + splitsize > intaxis.size:
                splitsize = intaxis.size - start

            if verbose:
                print(
                    "Rendering "
                    + str(axis)
                    + "-coords "
                    + str(start)
                    + "-"
                    + str(start + splitsize)
                    + " of "
                    + str(intaxis.size)
                    + " with ds=%8.6f" % np.float32(self.stepsize),
                    end="\r",
                    flush=True,
                )

            for name, table_split in table_splits.items():
                if name == "dstex":
                    # this is the missing splitsize in constant in spectanalys/__init__.py
                    table_split[i][:, :, :] *= splitsize

                self.load_texture(name, table_split[i])
            # for tup in split_tables:
            #    name, table = tup
            #    self.load_texture(name, table[start:start + splitsize])
            data_size = int(self.projection_x_size * self.projection_y_size)
            grid_size = int((data_size + BLOCKSIZE - 1) / BLOCKSIZE)

            self.load_constant("xstart", np.int32(start))
            self.load_constant("sliceWidth", np.int32(self.projection_y_size))

            spec_render(self, BLOCKSIZE, grid_size)

    def __init__(self, cuda_code):
        """
        Initialize with a cuda kernel.

        CUDA kernel should include renderer.h to utilize this
        """
        super(SingAxisRenderer, self).__init__(cuda_code)

    def set_axes(self, xaxis, yaxis, zaxis):
        """
        Sets the x, y, and z axes.

        x, y, z axes are lists correlating indices (in textures) to
        locations in space.
        """
        self.xaxis = xaxis
        self.yaxis = yaxis
        self.zaxis = zaxis
        self.load_constant("xmin", np.float32(xaxis.min()))
        self.load_constant("xmax", np.float32(xaxis.max()))
        self.load_constant("ymin", np.float32(yaxis.min()))
        self.load_constant("ymax", np.float32(yaxis.max()))
        self.load_constant("zmin", np.float32(zaxis.min()))
        self.load_constant("zmax", np.float32(zaxis.max()))
        self.load_constant("xtotalsize", np.int32(xaxis.size))

        self.ixaxis = norm_inverse_axis(xaxis)
        if len(yaxis) > 1:
            self.iyaxis = norm_inverse_axis(yaxis)
        else:
            self.iyaxis = [0]
        self.izaxis = norm_inverse_axis(zaxis)


class SingAxisRendererNONCUDA:
    zcutoff = (
        -0.5
    )  # the point at which we are considered to be no longer in the chromosphere
    locph = prev_lambd = float("nan")
    snap = 0

    x_pixel_offset = (
        0  # if 0, the output is centered in the box center--shifts output L/R
    )
    y_pixel_offset = 0  # shifts output U/D
    ##nsteps = 600
    distance_per_pixel = (
        0.06  # distance (specified by axes) between pixels in the output
    )
    mod = None  # CUDA kernel code
    xaxis = yaxis = zaxis = ixaxis = iyaxis = izaxis = None
    singlaxis = True
    ##ux = uy = uz = e = r = oscdata = ka_table = opatab = None
    textures = {}  # stores references to textures to prevent them being cleared

    def __init__(self):
        """
        Initialize with a cuda kernel.
        """

    def i_tdi_render(
        self,
        axis,
        consts,
        verbose=True,
        opacity=False,
    ):
        """
        Integrate the intensity along the LOS.
        """
        self.axis = axis

        if axis == 0:
            intaxis = self.xaxis
            ax_id = 0
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            intaxis = self.yaxis
            ax_id = 1
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            intaxis = self.zaxis
            ax_id = 2
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        dsc = np.ones_like(self.r)

        if axis == 0:
            dsc *= self.dx
        elif axis == 1:
            dsc *= self.dy
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix, iiy, :] = np.gradient(intaxis)

        if opacity:
            self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte
            kk_int = interp1d(
                self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte,
                self.ka_table,
            )
            kk = kk_int(np.log10(self.tg))

            tauds = (self.r / GRPH) * dsc * kk
            tau = tauds.copy()
            if axis == 0:
                for iix, ix in enumerate(self.x):
                    if iix > 0:
                        tau[iix, ...] = tau[iix - 1, ...] + tauds[iix, ...]
            elif axis == 1:
                for iiy, iy in enumerate(self.y):
                    if iiy > 0:
                        tau[:, iiy, :] = tau[:, iiy - 1, :] + tauds[:, iiy, :]
            else:
                for iiz, iz in enumerate(self.z):
                    if iiz > 0:
                        tau[..., iiz] = tau[..., iiz] + tauds[..., iiz]
        else:
            tau = np.zeros_like(self.r)

        em = self.em * ergd2wd * dsc

        return np.sum(em * np.exp(-tau), axis=ax_id), tau

    def il_tdi_render(
        self,
        axis,
        dnus,
        nfreq,
        nu0,
        dopp_width0,
        verbose=True,
        opacity=False,
    ):
        """
        Integrate the intensity along the LOS.
        """
        self.axis = axis

        if axis == 0:
            intaxis = self.xaxis
            ax_id = 0
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            intaxis = self.yaxis
            ax_id = 1
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            intaxis = self.zaxis
            ax_id = 2
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        dsc = np.ones_like(self.r)

        if axis == 0:
            dsc *= self.dx
        elif axis == 1:
            dsc *= self.dy
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix, iiy, :] = np.gradient(intaxis)

        if opacity:
            self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte
            kk_int = interp1d(
                self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte,
                self.ka_table,
            )
            kk = kk_int(np.log10(self.tg))

            tauds = (self.r / GRPH) * dsc * kk
            tau = tauds.copy()
            if axis == 0:
                for iix, ix in enumerate(self.x):
                    if iix > 0:
                        tau[iix, ...] = tau[iix - 1, ...] + tauds[iix, ...]
            elif axis == 1:
                for iiy, iy in enumerate(self.y):
                    if iiy > 0:
                        tau[:, iiy, :] = tau[:, iiy - 1, :] + tauds[:, iiy, :]
            else:
                for iiz, iz in enumerate(self.z):
                    if iiz > 0:
                        tau[..., iiz] = tau[..., iiz] + tauds[..., iiz]
        else:
            tau = np.zeros_like(self.r)

        dnu = dnus[nfreq]
        dopp_width = np.sqrt(self.tg) * dopp_width0
        shift = (dnu - nu0 * self.uu / CC) / dopp_width
        phi = np.exp(-shift * shift) / (np.sqrt(np.pi) * dopp_width)
        em = self.em * ergd2wd * dsc * phi

        return np.sum(em * np.exp(-tau), axis=ax_id), tau

    def i_se_render(
        self,
        axis,
        goftne,
        do_mit=False,
        opacity=False,
        verbose=True,
        abnd_arr=None,
    ):
        """
        Integrate the intensity along the LOS.
        """
        self.axis = axis

        if axis == 0:
            intaxis = self.xaxis
            ax_id = 0
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            intaxis = self.yaxis
            ax_id = 1
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            intaxis = self.zaxis
            ax_id = 2
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        dsc = np.ones_like(self.r)

        if axis == 0:
            dsc *= self.dx
        elif axis == 1:
            dsc *= self.dy
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix, iiy, :] = np.gradient(intaxis)

        if do_mit:
            nbbins, ntgbin, nedbin = np.shape(goftne)
            ne_axis = self.enmin + np.arange(nedbin) / (nedbin - 1) * self.enrange
            tg_axis = self.tgmin + np.arange(ntgbin) / (ntgbin - 1) * self.tgrange
            bb_axis = self.modbmin + np.arange(nbbins) / (nbbins - 1) * self.modbrange
            goft_interp = RegularGridInterpolator((bb_axis, tg_axis, ne_axis), goftne)
            goft = goft_interp(self.modb, np.log10(self.tg), np.log10(self.ne))
        else:
            ne_axis = (
                self.enmin + np.arange(self.nedbin) / (self.nedbin - 1) * self.enrange
            )
            tg_axis = (
                self.tgmin + np.arange(self.ntgbin) / (self.ntgbin - 1) * self.tgrange
            )
            goft_interp = RegularGridInterpolator(
                (ne_axis, tg_axis), goftne, bounds_error=False, fill_value=None
            )
            goft = goft_interp(
                (
                    np.log10(np.reshape(self.ne, (np.size(self.ne)))),
                    np.log10(np.reshape(self.tg, (np.size(self.tg)))),
                )
            )
            goft = np.reshape(goft, np.shape(self.ne))

        if opacity:
            tg_axis = self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte
            kk_int = interp1d(
                self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte,
                self.ka_table[0, :],
            )
            kk = kk_int(np.log10(self.tg))

            tauds = (self.r / GRPH) * dsc * kk
            tau = tauds.copy()
            if axis == 0:
                for iix, ix in enumerate(intaxis):
                    if iix > 0:
                        tau[iix, ...] = tau[iix - 1, ...] + tauds[iix, ...]
            elif axis == 1:
                for iiy, iy in enumerate(intaxis):
                    if iiy > 0:
                        tau[:, iiy, :] = tau[:, iiy - 1, :] + tauds[:, iiy, :]
            else:
                nz = np.size(intaxis)
                for iiz, iz in enumerate(intaxis):
                    if iiz > 0:
                        tau[..., nz - iiz - 1] = (
                            tau[..., nz - iiz] + tauds[..., nz - iiz - 1]
                        )
        else:
            tau = np.zeros_like(self.r)
        if abnd_arr is None:
            abnd_arr = np.ones(np.shape(self.ne))
        else:
            abnd_arr = abnd_arr[..., self.locph :]
        em = self.ne * (self.r / GRPH) * goft * ergd2wd * dsc * abnd_arr
        return np.sum(em * np.exp(-tau), axis=ax_id), tau

    def il_se_render(
        self,
        axis,
        goftne,
        dnus,
        nfreq,
        nu0,
        dopp_width0,
        do_mit=False,
        opacity=False,
        verbose=True,
    ):
        """
        Integrate the intensity along the LOS.
        """
        self.axis = axis

        if axis == 0:
            intaxis = self.xaxis
            ax_id = 0
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            intaxis = self.yaxis
            ax_id = 1
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            intaxis = self.zaxis
            ax_id = 2
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        dsc = np.ones_like(self.r)

        if axis == 0:
            dsc *= self.dx
        elif axis == 1:
            dsc *= self.dy
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix, iiy, :] = np.gradient(intaxis)

        if do_mit:
            nbbins, ntgbin, nedbin = np.shape(goftne)
            ne_axis = self.enmin + np.arange(nedbin) / (nedbin - 1) * self.enrange
            tg_axis = self.tgmin + np.arange(ntgbin) / (ntgbin - 1) * self.tgrange
            bb_axis = self.modbmin + np.arange(nbbins) / (nbbins - 1) * self.modbrange
            goft_interp = RegularGridInterpolator((bb_axis, tg_axis, ne_axis), goftne)
            goft = goft_interp(self.modb, np.log10(self.tg), np.log10(self.ne))
        else:
            ne_axis = (
                self.enmin + np.arange(self.nedbin) / (self.nedbin - 1) * self.enrange
            )
            tg_axis = (
                self.tgmin + np.arange(self.ntgbin) / (self.ntgbin - 1) * self.tgrange
            )
            goft_interp = RegularGridInterpolator(
                (ne_axis, tg_axis), goftne, bounds_error=False, fill_value=None
            )
            goft = goft_interp(
                (
                    np.log10(np.reshape(self.ne, (np.size(self.ne)))),
                    np.log10(np.reshape(self.tg, (np.size(self.tg)))),
                )
            )
            goft = np.reshape(goft, np.shape(self.ne))

        if opacity:
            tg_axis = self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte
            kk_int = interp1d(
                self.opatab.teinit + np.arange(self.opatab.nte) * self.opatab.dte,
                self.ka_table[0, :],
            )
            kk = kk_int(np.log10(self.tg))

            tauds = (self.r / GRPH) * dsc * kk
            tau = tauds.copy()
            if axis == 0:
                for iix, ix in enumerate(intaxis):
                    if iix > 0:
                        tau[iix, ...] = tau[iix - 1, ...] + tauds[iix, ...]
            elif axis == 1:
                for iiy, iy in enumerate(intaxis):
                    if iiy > 0:
                        tau[:, iiy, :] = tau[:, iiy - 1, :] + tauds[:, iiy, :]
            else:
                nz = np.size(intaxis)
                for iiz, iz in enumerate(intaxis):
                    if iiz > 0:
                        tau[..., nz - iiz - 1] = (
                            tau[..., nz - iiz] + tauds[..., nz - iiz - 1]
                        )
        else:
            tau = np.zeros_like(self.r)

        dnu = dnus[nfreq]
        dopp_width = np.sqrt(self.tg) * dopp_width0
        shift = (dnu - nu0 * self.uu / CC) / dopp_width
        phi = np.exp(-shift * shift) / (np.sqrt(np.pi) * dopp_width)
        em = self.ne * (self.r / GRPH) * goft * ergd2wd * phi

        return np.sum(em * np.exp(-tau), axis=ax_id), tau

    def set_axes(self, xaxis, yaxis, zaxis):
        """
        Sets the x, y, and z axes.

        x, y, z axes are lists correlating indices (in textures) to
        locations in space.
        """
        self.xaxis = xaxis
        self.yaxis = yaxis
        self.zaxis = zaxis

        self.ixaxis = norm_inverse_axis(xaxis)
        if len(yaxis) > 1:
            self.iyaxis = norm_inverse_axis(yaxis)
        else:
            self.iyaxis = [0]
        self.izaxis = norm_inverse_axis(zaxis)


def numpy3d_to_array(np_array, order=None):
    """
    Method for copying a numpy array to a CUDA array.

    If you get a buffer error, run this method on np_array.copy('F')
    """
    from pycuda.driver import Array, ArrayDescriptor3D, Memcpy3D, dtype_to_array_format

    if order is None:
        order = "C" if np_array.strides[0] > np_array.strides[2] else "F"

    if order.upper() == "C":
        d, h, w = np_array.shape
    elif order.upper() == "F":
        w, h, d = np_array.shape
    else:
        raise Exception("order must be either F or C")

    descr = ArrayDescriptor3D()
    descr.width = w
    descr.height = h
    descr.depth = d
    descr.format = dtype_to_array_format(np_array.dtype)
    descr.num_channels = 1
    descr.flags = 0

    device_array = Array(descr)

    copy = Memcpy3D()
    copy.set_src_host(np_array)
    copy.set_dst_array(device_array)
    copy.width_in_bytes = copy.src_pitch = np_array.strides[1]
    copy.src_height = copy.height = h
    copy.depth = d

    copy()

    return device_array


def view_axes(azimuth, altitude):
    """
    The vectors that correspond to a POV with azimuth and altitude.

    view_vector is the line following the LOS, view_x and view_y are the
    directions you shift in space as you move left/right
    """
    altitude = sorted((altitude, 90, -90))[1]
    altitude = -altitude
    azimuth = azimuth % 360 - 180

    azimuth = m.radians(int(azimuth))
    altitude = m.radians(int(altitude))

    view_vector = np.array(
        (
            m.cos(altitude) * m.cos(azimuth),
            m.cos(altitude) * m.sin(azimuth),
            m.sin(altitude),
        )
    )
    view_x = np.array((m.sin(azimuth), -m.cos(azimuth), 0))
    view_y = np.cross(view_x, view_vector)

    return (
        view_x.astype("float32"),
        view_y.astype("float32"),
        view_vector.astype("float32"),
    )


def norm_inverse_axis(axis):
    """
    From an axis (corresponds table indices to real coordinates), generate an
    inverse lookup table that will take real coordinates (normalized to (0, 1))
    and return a normalized (0, 1) lookup index.
    """
    axinverse = interp1d(
        (axis - axis.min()) / np.ptp(axis), np.linspace(0, 1, axis.size)
    )
    return axinverse(np.linspace(0, 1, axis.size * 6)).astype("float32")
