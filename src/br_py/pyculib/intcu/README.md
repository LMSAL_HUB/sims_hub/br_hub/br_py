spectAnlys
==========

CUDA-based tools for fast solar spectra analysis. Useful for processing 2D spectral data.
Capable of:

 * Gaussian regression of spectra
 * Peak detection/counting
 * Asymmetry analysis

br_renderer
===========

Basic framework for ray tracing with CUDA, especially for applications that require splitting of the render domain.
Easily calculates view vectors and start points based on pixel locations, determining whether or not in bounds, etc.

Actual integration and iteration is done by an attached CUDA module (written by the user, and likely will include renderer.cuh)

Ensure that environment variable CUDA_HOME is set correctly.
