"""
Set of plotting and movies tools.
"""
import importlib

# __all__ = ["br_uvrt", "renderer", "spectanalys"]
__all__ = ["uvrt", "renderer"]

spam_spec = importlib.util.find_spec("pycuda")
found = spam_spec is not None

if found:
    try:
        pass
    except Exception:  # NOQA: BLE001
        found = False
        print("br_uvrt is not imported, pycuda.autoinit import failed")

# if found:
from . import uvrt
from . import renderer
