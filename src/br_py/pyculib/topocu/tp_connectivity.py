import os
import numpy as np

from .tp_trace import tracer
from .tp_trace import tracernopars

from pycuda import gpuarray
from pycuda.compiler import SourceModule


class connectivity(tracer):
    def init_connectivity(self):
        """
        Initializes kernel.
        """

        with open(
            os.environ["CUDA_LIB"] + "TopoMaFiCuCo/connectivity.cu"
        ) as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = (
            self.mod.get_function("alternateConnectivity")
            if self.opts.alt
            else self.mod.get_function("connectivity")
        )

    def connectivity(self, plane):
        """
        Executes kernel.
        """

        self.constants = {}

        self.textures = {
            "x": np.atleast_3d(self.arrays["x"]),
            "y": np.atleast_3d(self.arrays["y"]),
            "z": np.atleast_3d(self.arrays["z"]),
            "l": np.atleast_3d(self.arrays["l"]),
        }

        self.load_constants()
        self.load_axes()
        self.load_textures()

        d_c = gpuarray.to_gpu(np.zeros((self.xl, self.yl), dtype=np.float32, order="F"))

        self.kernel(d_c, block=self.block, grid=self.grid)

        return d_c.get()

    def __init__(self):
        super(connectivity, self).__init__()


class connectivitynopars(tracernopars):
    def init_connectivity(self):
        """
        Initializes kernel.
        """

        with open(
            os.environ["CUDA_LIB"] + "TopoMaFiCuCo/connectivity.cu"
        ) as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = (
            self.mod.get_function("alternateConnectivity")
            if self.opts.alt
            else self.mod.get_function("connectivity")
        )

    def connectivity(self, plane):
        """
        Executes kernel.
        """

        self.constants = {}

        self.textures = {
            "x": np.atleast_3d(self.arrays["x"]),
            "y": np.atleast_3d(self.arrays["y"]),
            "z": np.atleast_3d(self.arrays["z"]),
            "l": np.atleast_3d(self.arrays["l"]),
        }

        self.load_constants()
        self.load_axes()
        self.load_textures()

        d_c = gpuarray.to_gpu(np.zeros((self.xl, self.yl), dtype=np.float32, order="F"))

        self.kernel(d_c, block=self.block, grid=self.grid)

        return d_c.get()

    def __init__(self, opts=None, parent=None):
        super(connectivitynopars, self).__init__(opts)
