import numpy as np
from PyQt5 import Qt
from PyQt5 import QtWidgets

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib import cm
from matplotlib.image import NonUniformImage
from mpl_toolkits.axes_grid1 import make_axes_locatable

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from string import Template
from .tp_switch import switchnopars


class plotter(QtWidgets.QDialog):
    initialized = False
    qC = False

    def __init__(self, qC, parent=None):
        super(plotter, self).__init__(parent)

        self.qC = qC
        self.setdefaults()

        self.arr = qC.q
        self.x = qC.axes["x"]
        self.y = qC.axes["y"]
        self.z = qC.axes["z"]
        self.tag = "q"

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)

        self.slider = QtWidgets.QSlider(orientation=Qt.Qt.Horizontal)
        self.slider.setTickInterval(1)
        self.slider.valueChanged.connect(self.plot)

        self.timeslider = QtWidgets.QScrollBar(Qt.Qt.Horizontal)
        self.timerange()
        # sig = pyqtSignal(int, name = "valueChanged")
        self.timeslider.valueChanged.connect(self.plot)
        # self.connect(self.timeslider, QtCore.SIGNAL("valueChanged(int)"), self.plot)

        timew = QtWidgets.QVBoxLayout()
        self.timelabel = QtWidgets.QLabel("Snapshot #:" + str(self.timeslider.value()))
        self.timeslider.valueChanged.connect(self.changetimeandlab)
        # self.connect(self.timeslider, QtCore.SIGNAL("valueChanged(int)"), self.changetimeandlab)
        timew.addWidget(self.timelabel)
        timew.addWidget(self.timeslider)

        self.view_combo = QtWidgets.QComboBox(self)
        self.view_combo.activated[str].connect(self.changeView)
        self.view_combo.addItem("X-Y")
        self.view_combo.addItem("X-Z")
        self.view_combo.addItem("Y-Z")
        self.view = "X-Y"
        self.slide_dimension = 2

        self.DisplaySetting = QtWidgets.QPushButton("Display Set.", self)
        self.DisplaySetting.setGeometry(Qt.QRect(0, 0, 100, 30))
        self.DisplaySetting.clicked.connect(self.dosettingw)
        # self.connect(self.DisplaySetting, Qt.SIGNAL("clicked()"), self.dosettingw)

        self.SaveRange = QtWidgets.QPushButton("Save Range", self)
        self.SaveRange.setGeometry(Qt.QRect(0, 0, 100, 30))
        self.SaveRange.clicked.connect(self.dorsavew)
        # self.connect(self.SaveRange, Qt.SIGNAL("clicked()"), self.dorsavew)

        self.Help = QtWidgets.QPushButton("Help", self)
        self.Help.setGeometry(Qt.QRect(0, 0, 100, 30))
        self.Help.clicked.connect(self.dohelpw)
        # self.connect(self.Help, Qt.SIGNAL("clicked()"), self.dohelpw)

        self.check_defminmax = QtWidgets.QCheckBox("Adjust min/max")
        self.check_defminmax.setChecked(True)
        self.check_defminmax.stateChanged.connect(self.plot)

        self.check_abs = QtWidgets.QCheckBox("Absolute")
        self.check_abs.stateChanged.connect(self.plot)

        self.check_bw = QtWidgets.QCheckBox("B&W")
        self.check_bw.stateChanged.connect(self.plot)

        self.check_log = QtWidgets.QCheckBox("Log")
        self.check_log.stateChanged.connect(self.plot)

        self.hbox = QtWidgets.QHBoxLayout()
        self.hbox.addWidget(self.check_abs)
        self.hbox.addWidget(self.check_log)
        self.hbox.addWidget(self.check_bw)
        self.hbox.addWidget(self.check_defminmax)
        self.hbox.addWidget(self.DisplaySetting)
        self.hbox.addWidget(self.SaveRange)
        self.hbox.addWidget(self.Help)
        self.hbox.addWidget(self.view_combo)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addLayout(self.hbox)
        layout.addLayout(timew)
        layout.addWidget(self.canvas)
        layout.addWidget(self.slider)
        self.setLayout(layout)
        self.wd = None
        self.wsr = None
        self.data = self.arr
        self.initialized = True
        self.plot()

    def setdefaults(self):
        self.step = 1
        self.sdomain = self.qC.snap_range[0]
        self.edomain = self.qC.snap_range[1]
        self.log_offset = 1.0
        self.snap = self.qC.snap
        self.snap_bound_init = self.qC.snap_range[0]
        self.snap_bound_end = self.qC.snap_range[1]
        self.snap_skip = 1
        self.minv = 0.0
        self.maxv = 999999.0
        self.cmaps = [
            (
                "Seq",
                [
                    "Blues",
                    "BuGn",
                    "BuPu",
                    "GnBu",
                    "Greens",
                    "Greys",
                    "Oranges",
                    "OrRd",
                    "PuBu",
                    "PuBuGn",
                    "PuRd",
                    "Purples",
                    "RdPu",
                    "Reds",
                    "YlGn",
                    "YlGnBu",
                    "YlOrBr",
                    "YlOrRd",
                ],
            ),
            (
                "Seq(2)",
                [
                    "afmhot",
                    "autumn",
                    "bone",
                    "cool",
                    "copper",
                    "gist_heat",
                    "gray",
                    "hot",
                    "pink",
                    "spring",
                    "summer",
                    "winter",
                ],
            ),
            (
                "Div",
                [
                    "BrBG",
                    "bwr",
                    "coolwarm",
                    "PiYG",
                    "PRGn",
                    "PuOr",
                    "RdBu",
                    "RdGy",
                    "RdYlBu",
                    "RdYlGn",
                    "Spectral",
                    "seismic",
                ],
            ),
            (
                "Qual",
                [
                    "Accent",
                    "Dark2",
                    "Paired",
                    "Pastel1",
                    "Pastel2",
                    "Set1",
                    "Set2",
                    "Set3",
                ],
            ),
            (
                "Misc",
                [
                    "gist_earth",
                    "terrain",
                    "ocean",
                    "gist_stern",
                    "brg",
                    "CMRmap",
                    "cubehelix",
                    "gnuplot",
                    "gnuplot2",
                    "gist_ncar",
                    "nipy_spectral",
                    "jet",
                    "rainbow",
                    "gist_rainbow",
                    "hsv",
                    "flag",
                    "prism",
                ],
            ),
        ]
        self.color = "jet"

    def keyPressEvent(self, event):
        key = event.key

        if key == Qt.Qt.Key_Right:
            self.slider.setValue(self.slider.value() + 1)
        elif key == Qt.Qt.Key_Left:
            self.slider.setValue(self.slider.value() - 1)

    def goBack(self):
        self.param = get_bifrost_param(self.fpath, -1)
        self.b, self.base_name, self.snap_n = get_bifrost_obj(
            self.fpath, -1, self.check_aux.isChecked()
        )
        self.get_data()
        self.plot()
        where_str, base_name, snap_n = process_file_name(self.fpath)
        snap_n = str(int(snap_n) - 1)
        self.fpath = where_str + base_name + "_" + snap_n + ".snap"

    def goNext(self):
        self.param = get_bifrost_param(self.fpath, 1)
        self.b, self.base_name, self.snap_n = get_bifrost_obj(
            self.fpath, 1, self.check_aux.isChecked()
        )
        self.get_data()
        self.plot()
        where_str, base_name, snap_n = process_file_name(self.fpath)
        snap_n = str(int(snap_n) + 1)
        self.fpath = where_str + base_name + "_" + snap_n + ".snap"

    def changeView(self, text):
        self.view = text
        self.get_data()
        self.plot()
        print("View changed to: ", self.view)

    def comboActivated(self, text):
        self.tag = text
        self.data = self.get_var(self.tag, self.snap)
        self.plot()

    def get_data(self):
        self.data = self.arr
        if self.view == "X-Y":
            self.slide_dimension = 2
        elif self.view == "X-Z":
            self.slide_dimension = 1
        elif self.view == "Y-Z":
            self.slide_dimension = 0

        self.slider.setMinimum(0)
        self.slider.setMaximum(self.data.shape[self.slide_dimension] - 1)

        if self.slider.value() >= self.data.shape[self.slide_dimension]:
            self.slider.setValue(self.data.shape[self.slide_dimension] - 1)
        elif self.slider.value() < 0:
            self.slider.setValue(0)

    def home(self):
        self.toolbar.home()

    def zoom(self):
        self.toolbar.zoom()

    def pan(self):
        self.toolbar.pan()

    def select_colorbar(self, text):
        foundcolor = False
        for a in range(len(self.cmaps)):
            for b in range(len(self.cmaps[a][1])):
                if self.cmaps[a][0] + "/" + self.cmaps[a][1][b] == text:
                    self.ws.color = self.cmaps[a][1][b]
                    foundcolor = True
        if foundcolor is False:
            self.ws.color = self.color

    def dosettingw(self):
        self.ws = Qt.QWidget()
        self.ws.setWindowTitle("Display Setup")
        self.ws.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.ws.vbox = QtWidgets.QVBoxLayout()

        snaplabel = QtWidgets.QLabel("Snapsht #")
        self.ws.snap = QtWidgets.QSpinBox()
        self.ws.snap.setMinimum(self.qC.snap_range[0])
        self.ws.snap.setMaximum(self.qC.snap_range[1])
        self.ws.snap.setSingleStep(1)
        self.ws.snap.setValue(self.snap)
        snapw = QtWidgets.QHBoxLayout()
        snapw.addWidget(snaplabel)
        snapw.addWidget(self.ws.snap)

        channellabel = QtWidgets.QLabel("Select Colorbar")
        self.ws.colorbar = QtWidgets.QComboBox(self.ws)
        # self.ws.colorbar.setValue(self.cmaps)
        for a in range(len(self.cmaps)):
            for b in range(len(self.cmaps[a][1])):
                self.ws.colorbar.addItem(self.cmaps[a][0] + "/" + self.cmaps[a][1][b])
        self.ws.color = self.color
        self.ws.colorbar.activated[str].connect(self.select_colorbar)
        colorbarw = QtWidgets.QHBoxLayout()
        colorbarw.addWidget(channellabel)
        colorbarw.addWidget(self.ws.colorbar)

        minvlabel = QtWidgets.QLabel("Min Val")
        self.ws.minv = QtWidgets.QDoubleSpinBox()
        self.ws.minv.setSingleStep(0.1)
        self.ws.minv.setMinimum(-9.0e14)
        self.ws.minv.setMaximum(9.0e14)
        self.ws.minv.setValue(self.minv)
        minvw = QtWidgets.QHBoxLayout()
        minvw.addWidget(minvlabel)
        minvw.addWidget(self.ws.minv)

        maxvlabel = QtWidgets.QLabel("Max Val")
        self.ws.maxv = QtWidgets.QDoubleSpinBox()
        self.ws.maxv.setSingleStep(0.1)
        self.ws.maxv.setMinimum(-9.0e14)
        self.ws.maxv.setMaximum(9.0e14)
        self.ws.maxv.setValue(self.maxv)
        maxvw = QtWidgets.QHBoxLayout()
        maxvw.addWidget(maxvlabel)
        maxvw.addWidget(self.ws.maxv)

        dynlabel = QtWidgets.QLabel("Dynamic range")
        self.ws.dynamicrange = QtWidgets.QDoubleSpinBox()
        self.ws.dynamicrange.setMinimum(0.001)
        self.ws.dynamicrange.setMaximum(100.0)
        self.ws.dynamicrange.setSingleStep(0.1)
        self.ws.dynamicrange.setValue(self.log_offset)
        dynw = QtWidgets.QHBoxLayout()
        dynw.addWidget(dynlabel)
        dynw.addWidget(self.ws.dynamicrange)

        self.ws.vbox.addLayout(snapw)
        self.ws.vbox.addLayout(colorbarw)
        self.ws.vbox.addLayout(minvw)
        self.ws.vbox.addLayout(maxvw)
        self.ws.vbox.addLayout(dynw)

        save = QtWidgets.QPushButton("Save", self.ws)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            if self.ws.snap.value() != self.snap:
                self.snap = self.ws.snap.value()
                self.timeslider.setValue(self.snap)
                self.changelabel()
                print("Snapshot # changed to ", self.snap)
            if self.ws.color != self.color:
                print("color changed to ", self.ws.color)
                self.color = self.ws.color
                self.check_bwresettofalse()
            if self.ws.maxv.value() != self.maxv:
                self.maxv = self.ws.maxv.value()
                self.check_defminmaxresettofalse()
            if self.ws.minv.value() != self.minv:
                self.minv = self.ws.minv.value()
                self.check_defminmaxresettofalse()
            if self.ws.dynamicrange.value() != self.log_offset:
                self.log_offset = self.ws.dynamicrange.value()
                self.check_logresettofalse()
            self.plot()
            self.ws.close()

        # sig2 = pyqtSignal(name = 'clicked()')
        save.clicked.connect(showText)
        # self.ws.connect(save, QtCore.SIGNAL('clicked()'), showText)

        layouta = QtWidgets.QVBoxLayout()
        layouta.addLayout(self.ws.vbox)
        layouta.addWidget(save)
        self.ws.setLayout(layouta)

        self.ws.show()

    def dorsavew(self):
        self.wsr = Qt.QWidget()
        self.wsr.setWindowTitle("Range Save")
        self.wsr.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wsr.vbox = QtWidgets.QVBoxLayout()
        self.wsr.savefilename = (
            QtWidgets.QFileDialog.getSaveFileName()
        )  #'Open', self.wsr.pathdiag)
        if not self.wsr.savefilename:
            return

        snap_rangelabel = QtWidgets.QLabel("Snap range (min/max/step)")
        self.wsr.snap_bound_init = QtWidgets.QSpinBox()
        self.wsr.snap_bound_init.setMinimum(self.qC.snap_range[0])
        self.wsr.snap_bound_init.setMaximum(self.qC.snap_range[1])
        self.wsr.snap_bound_init.setSingleStep(1)
        self.wsr.snap_bound_init.setValue(self.snap_bound_init)
        self.wsr.snap_bound_end = QtWidgets.QSpinBox()
        self.wsr.snap_bound_end.setMinimum(self.qC.snap_range[0])
        self.wsr.snap_bound_end.setMaximum(self.qC.snap_range[1])
        self.wsr.snap_bound_end.setSingleStep(1)
        self.wsr.snap_bound_end.setValue(self.snap_bound_end)
        self.wsr.snap_skip = QtWidgets.QSpinBox()
        self.wsr.snap_skip.setMinimum(1)
        self.wsr.snap_skip.setMaximum(9999)
        self.wsr.snap_skip.setSingleStep(1)
        self.wsr.snap_skip.setValue(self.snap_skip)
        snaprw = QtWidgets.QHBoxLayout()
        snaprw.addWidget(snap_rangelabel)
        snaprw.addWidget(self.wsr.snap_bound_init)
        snaprw.addWidget(self.wsr.snap_bound_end)
        snaprw.addWidget(self.wsr.snap_skip)

        save = QtWidgets.QPushButton("Save", self.wsr)
        save.setGeometry(10, 40, 80, 20)

        def showText():
            self._renderrangefromdialog(self.wsr)
            self.wsr.close()

        save.clicked.connect(showText)
        # self.wsr.connect(save,Qt.SIGNAL('clicked()'), showText)

        layouta = QtWidgets.QVBoxLayout()
        layouta.addLayout(self.wsr.vbox)
        layouta.addLayout(snaprw)
        layouta.addWidget(save)
        self.wsr.setLayout(layouta)
        self.wsr.show()

    def _renderrangefromdialog(self, srd):
        snap_bounds = sorted(
            (int(srd.snap_bound_init.value()), int(srd.snap_bound_end.value()))
        )
        snap_skip = int(srd.snap_skip.value())
        snap_range = range(snap_bounds[0], snap_bounds[1], snap_skip)

        save_loc = srd.savefilename

        Template(save_loc)
        """
        If len(snap_range) > 1 and '${num}' not in save_loc or len(channel_ids)
        > 1 and '${chan}' not in save_loc:

        print('Missing "${num}" or "${chan}" in file descriptor') return
        """

        orig_snap = self.snap

        for snap in snap_range:
            opts = self.qC.opts
            opts.dir = self.qC.directory
            opts.temp = self.qC.temp
            opts.plane = self.qC.plane
            opts.save = self.qC.save
            opts.snap = snap
            switchnopars(opts)
            save_file = save_loc + str(snap)
            self.qC.save_data(save_file, self.data)
            print("wrote", save_file)
        self.snap = orig_snap
        self.raw_data = self.raw_spectra = None
        self.plot()

    def dohelpw(self):
        # print "Opening a new popup window..."
        self.wsr = Qt.QLabel("Help info")
        self.wsr.setGeometry(Qt.QRect(100, 100, 400, 200))
        self.wsr.show()

    def changetimeandlab(self):
        self.changelabel()
        self.changetime()

    def changetime(self):
        opts = self.qC.opts
        opts.dir = self.qC.directory
        opts.temp = self.qC.temp
        opts.plane = self.qC.plane
        opts.save = self.qC.save
        opts.snap = self.timeslider.value()
        self.snap = opts.snap
        self.qC.opts = opts
        out = switchnopars(opts)

        self.qC = out
        self.qC.opts = opts
        self.arr = out.q
        self.data = self.arr
        self.plot()

    def changelabel(self):
        self.timelabel.setText("Snapshot #:" + str(self.timeslider.value()))

    def check_defminmaxresettofalse(self):
        self.check_defminmax.setChecked(False)

    def check_logresettofalse(self):
        self.check_log.setChecked(False)

    def check_bwresettofalse(self):
        self.check_bw.setChecked(False)

    def timerange(self):
        # self.timeslider.setMinimum(self.rend.snap_range[0])
        # self.timeslider.setMaximum(self.rend.snap_range[1])
        self.timeslider.setMinimum(self.qC.snap_range[0])
        self.timeslider.setMaximum(self.qC.snap_range[1])
        self.timeslider.setSingleStep(1)

    def plot(self):
        plt.clf()
        ax = self.figure.add_subplot(111)
        if self.view == "X-Y":
            image = self.data[:, :, self.slider.value()]
            "z = %f " % self.z[self.slider.value()]
            ax.set_ylabel("Y")
            ax.set_xlabel("X")

        elif self.view == "X-Z":
            image = self.data[:, self.slider.value(), :]
            "y = %f " % self.y[self.slider.value()]
            ax.set_ylabel("Z")
            ax.set_xlabel("X")

        elif self.view == "Y-Z":
            image = self.data[self.slider.value(), :, :]
            "x = %f " % self.x[self.slider.value()]
            ax.set_ylabel("Z")
            ax.set_xlabel("Y")

        (np.nanmin(image), np.nanmax(image))
        # bounds = (np.min(self.raw_data), np.max(self.raw_data))

        label = "Value"

        if self.check_abs.isChecked():
            image = np.absolute(image)
            label = "ABS( %s )" % label

        if self.check_log.isChecked():
            minval = np.min(image)
            if minval == 0:
                minvalarr = np.absolute(np.ones((len(image), len(image[0]))) / 1.0e20)
            elif minval < 0:
                print("Warning var has negative values:", np.min(image))
                minvalarr = np.absolute(
                    np.ones((len(image), len(image[0]))) * minval * 1.01
                )
                print("added ", np.min(minvalarr), "everywhere")
            else:
                minvalarr = np.zeros((len(image), len(image[0])))
            image = np.log10(image + minvalarr)
            label = "Log10( %s )" % label
            self.log_offset = 1.0
        else:
            # self.log_offset = self.ws.dynamicrange.value()
            image = image**self.log_offset

        if self.check_bw.isChecked():
            self.color = "Greys_r"

        if self.check_defminmax.isChecked():
            self.minv = np.min(image)
            self.maxv = np.max(image)
        minval = np.ones((len(image), len(image[0]))) * self.minv
        np.ones((len(image), len(image[0]))) * self.maxv
        # image=np.maximum(image,minval)
        # image=np.minimum(image,maxval)
        ax.axis("equal")
        if self.view == "X-Y":
            ax.set_ylabel("Y [Mm]")
            ax.set_xlabel("X [Mm]")
            im = NonUniformImage(
                ax,
                interpolation="bilinear",
                extent=(self.x.min(), self.x.max(), self.y.min(), self.y.max()),
                cmap=cm.get_cmap(self.color),
            )
            im.set_data(self.x, self.y, np.fliplr(list(zip(*image[::-1]))))
            # im.set_data(self.x, self.y, np.fliplr(zip(*image[::-1])))
            ax.images.append(im)
            ax.set_xlim(self.x.min(), self.x.max())
            ax.set_ylim(self.y.min(), self.y.max())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(4)))
        elif self.view == "X-Z":
            ax.set_ylabel("Z [Mm]")
            ax.set_xlabel("X [Mm]")
            im = NonUniformImage(
                ax,
                interpolation="bilinear",
                extent=(self.x.min(), self.x.max(), self.z.min(), self.z.max()),
                cmap=cm.get_cmap(self.color),
            )
            # im.set_data(self.x, self.z, np.flipud(np.fliplr(image)))
            im.set_data(self.x, self.z[::-1], np.flipud(np.fliplr(zip(*image[::-1]))))
            ax.images.append(im)
            ax.set_xlim(self.x.min(), self.x.max())
            ax.set_ylim(self.z.max(), self.z.min())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(2)))
        elif self.view == "Y-Z":
            ax.set_ylabel("Z [Mm]")
            ax.set_xlabel("Y [Mm]")
            im = NonUniformImage(
                ax,
                interpolation="bilinear",
                extent=(self.y.min(), self.y.max(), self.z.min(), self.z.max()),
                cmap=cm.get_cmap(self.color),
            )
            # im.set_data(self.y, self.z, np.flipud(np.fliplr(image)))
            im.set_data(self.y, self.z[::-1], np.flipud(np.fliplr(zip(*image[::-1]))))
            ax.images.append(im)
            ax.set_xlim(self.y.min(), self.y.max())
            ax.set_ylim(self.z.max(), self.z.min())
            ax.xaxis.set_major_locator(ticker.MultipleLocator(int(4)))
            ax.yaxis.set_major_locator(ticker.MultipleLocator(int(2)))

        divider = make_axes_locatable(ax)
        # im = ax.imshow(image, interpolation='none', origin='lower', cmap=color, extent=extension)
        # ax.text(0.025, 0.025, (r'$\langle  B_{z}  \rangle = %2.2e$'+'\n'+r'$\langle |B_{z}| \rangle = %2.2e$') % (np.average(img),np.average(np.absolute(img))), ha='left', va='bottom', transform=ax.transAxes)
        # divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(im, cax=cax, label=label)
        self.canvas.draw()
