import os
import math
import numpy as np

from .tp_data import getData, getDatanopars
from .tp_cuda import cudaManipulator

from pycuda import gpuarray
from pycuda.compiler import SourceModule


class tracer(getData, cudaManipulator):
    def init_trace(self):
        """
        Initializes kernel.
        """

        with open(os.environ["CUDA_LIB"] + "TopoMaFiCuCo/trace.cu") as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = self.mod.get_function("trace")
        self.load_axes()

    def trace_slice(self, lower, upper, i):
        """
        Executes kernel Traces the field between lower and upper bounds If i !=
        0, initializes tracing at plane i.
        """

        self.constants = {
            "step": 0.0004,
            "mintheta": 0.0000005,
            "maxlength": 200000,
            "upper": self.axes["z"][upper],
            "lower": self.axes["z"][lower],
            "offset": upper,
        }

        self.textures = {
            "bxc": self.data["bxc"][:, :, upper:lower],
            "byc": self.data["byc"][:, :, upper:lower],
            "bzc": self.data["bzc"][:, :, upper:lower],
        }

        self.load_constants()
        self.load_textures()
        self.load_axes()
        d_arrays = {}
        d_arrays.update(
            {a: gpuarray.to_gpu(self.arrays[a]) for a in self.arrays.keys()}
        )

        self.kernel(np.intc(i), *d_arrays.values(), block=self.block, grid=self.grid)
        self.arrays.update({a: d_arrays[a].get() for a in d_arrays.keys()})

    def trace_plane(self, plane):
        """
        Traces field lines passing through [plane] until they return to it.

        Also records length.
        """

        # Initializes data arrays

        array_names = ["x", "y", "z", "d", "l"]
        template = np.zeros((self.xl, self.yl), dtype=np.float32, order="F")

        self.arrays = {}
        self.arrays.update({a: np.zeros_like(template) for a in array_names})

        # Calculates domain splits

        # Upper limit for array size on Ibiza
        # split_size = math.floor(256 * 256 * 128 / float(self.xl * self.yl))
        split_size = math.floor(256)
        splits = np.arange(0, plane, split_size)
        splits = np.append(splits, plane)[::-1]

        # Traces

        for j in range(0, splits.size - 1):
            self.trace_slice(splits[j], splits[j + 1], plane if j == 0 else 0)

        splits = splits[::-1]

        for j in range(1, splits.size - 1):
            self.trace_slice(splits[j], splits[j + 1], 0)

    def trace_snapshot(self):
        self.init_trace()

        self.q_data = {}

        for i in range(0, self.zlen):
            self.init_trace()
            self.trace_plane(self.plane if self.opts.slice else i)
            self.q_data.update({i: self.arrays})

    def __init__(self):
        super(tracer, self).__init__()


class tracernopars(getDatanopars, cudaManipulator):
    def init_trace(self):
        """
        Initializes kernel.
        """

        with open(os.environ["CUDA_LIB"] + "TopoMaFiCuCo/trace.cu") as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = self.mod.get_function("trace")
        self.load_axes()

    def trace_slice(self, lower, upper, i):
        """
        Executes kernel Traces the field between lower and upper bounds If i !=
        0, initializes tracing at plane i.
        """

        self.constants = {
            "step": 0.0004,
            "mintheta": 0.0000005,
            "maxlength": 200000,
            "upper": self.axes["z"][upper],
            "lower": self.axes["z"][lower],
            "offset": upper,
        }

        self.textures = {
            "bxc": self.data["bxc"][:, :, upper:lower],
            "byc": self.data["byc"][:, :, upper:lower],
            "bzc": self.data["bzc"][:, :, upper:lower],
        }

        self.load_constants()
        self.load_textures()
        self.load_axes()

        d_arrays = {}
        d_arrays.update(
            {a: gpuarray.to_gpu(self.arrays[a]) for a in self.arrays.keys()}
        )

        self.kernel(np.intc(i), *d_arrays.values(), block=self.block, grid=self.grid)

        self.arrays.update({a: d_arrays[a].get() for a in d_arrays.keys()})

    def trace_plane(self, plane):
        """
        Traces field lines passing through [plane] until they return to it.

        Also records length.
        """

        # Initializes data arrays

        array_names = ["x", "y", "z", "d", "l"]
        template = np.zeros((self.xl, self.yl), dtype=np.float32, order="F")

        self.arrays = {}
        self.arrays.update({a: np.zeros_like(template) for a in array_names})

        # Calculates domain splits

        # Upper limit for array size on Ibiza
        # split_size = math.floor(256 * 256 * 128 / float(self.xl * self.yl))
        split_size = math.floor(256)
        splits = np.arange(0, plane, split_size)
        splits = np.append(splits, plane)[::-1]
        # Traces

        for j in range(0, splits.size - 1):
            self.trace_slice(splits[j], splits[j + 1], plane if j == 0 else 0)

        splits = splits[::-1]

        for j in range(1, splits.size - 1):
            self.trace_slice(splits[j], splits[j + 1], 0)

    def trace_snapshot(self):
        self.init_trace()

        self.q_data = {}

        for i in range(0, self.zlen):
            self.init_trace()
            self.trace_plane(self.plane if self.opts.slice else i)
            self.q_data.update({i: self.arrays})

    def __init__(self, opts=None, parent=None):
        super(tracernopars, self).__init__(opts)
