import numpy as np
from .tp_parse import parseArgs
import os
# from plot import plotter


class getData(parseArgs):
    def get_data(self):
        """
        Loads data arrays.
        """

        def inverse(axis):
            """
            Creates inverse lookup table normalized from 0 to 1.
            """

            from scipy.interpolate import interp1d

            axinverse = interp1d(
                (axis - axis.min()) / np.ptp(axis), np.linspace(0, 1, axis.size)
            )
            axis = axinverse(np.linspace(0, 1, axis.size * 8)).astype("float32")

            return axis

        from helita.sim.bifrost import BifrostData

        self.BifrostData = BifrostData(self.temp, fdir=self.directory, snap=self.snap)

        data = ["bxc", "byc", "bzc"]
        axes = ["x", "y", "z"]

        if self.opts.integrate:
            data.extend(["ix", "iy", "iz"])

        self.data = {}
        self.axes = {}

        self.data.update({d: self.BifrostData.get_var(d, self.snap) for d in data})
        self.axes.update({a: self.BifrostData.get_var(a, self.snap) for a in axes})
        self.axes.update({"i" + a: inverse(self.axes[a]) for a in self.axes.keys()})

        self.global_constants = {
            "xmin": min(self.axes["x"]),
            "ymin": min(self.axes["y"]),
            "zmin": min(self.axes["z"]),
            "xmax": max(self.axes["x"]),
            "ymax": max(self.axes["y"]),
            "zmax": max(self.axes["z"]),
            "xlen": len(self.axes["x"]),
            "ylen": len(self.axes["y"]),
            "zlen": len(self.axes["z"]),
        }

        self.xl = self.global_constants["xlen"]
        self.yl = self.global_constants["ylen"]
        self.zl = self.global_constants["zlen"]
        self.zlen = 1 if self.opts.slice else self.zl
        print("a", self.zlen)
        self.block = (8, 8, 1)
        self.grid = (int(self.xl / 8), int(self.yl / 8), 1)

        self.q = np.zeros((self.xl, self.yl, self.zlen), dtype=np.float32, order="F")

        print("Loaded data for snap", self.snap)

    def save_data(self, name, array):
        """
        Saves irender output in binary format.

        File has format (int dimensions (2)), (int xsize), (int ysize),
        (data array), (int x-axis size), (int y-axis size), (int z-axis
        size), (x array), (y array), (z array)
        """
        savearray(name, array)
        with open(name, mode="ab") as newfile:
            newfile.write(bytes(self.axes["x"].astype("float32").data))
            newfile.write(bytes(self.axes["y"].astype("float32").data))
            newfile.write(bytes(self.axes["z"].astype("float32").data))
            newfile.write(
                bytes(
                    np.array(
                        (self.axes["x"].size, self.axes["y"].size, self.axes["z"].size),
                        dtype="int32",
                    ).data
                )
            )

    def __init__(self):
        super(getData, self).__init__()
        self.get_data()


# class getDatanopars(ploter):
class getDatanopars:
    slice = False
    rcalc = False
    save = False

    # Data location
    file = None
    directory = "/sanhome/juanms/mpi3drun/OSCB1/"
    temp = "qsmag_bb01_it"
    snap = 140
    plane = 25

    # Calculation settings
    q = True
    alt = True
    integrate = True
    connectivity = True

    def get_data(self):
        """
        Loads data arrays.
        """

        def inverse(axis):
            """
            Creates inverse lookup table normalized from 0 to 1.
            """

            from scipy.interpolate import interp1d

            axinverse = interp1d(
                (axis - axis.min()) / np.ptp(axis), np.linspace(0, 1, axis.size)
            )
            axis = axinverse(np.linspace(0, 1, axis.size * 8)).astype("float32")

            return axis

        from helita.sim.bifrost import BifrostData
        # from bifrost import BifrostData

        self.BifrostData = BifrostData(self.temp, fdir=self.directory, snap=self.snap)

        data = ["bxc", "byc", "bzc"]
        axes = ["x", "y", "z"]

        if self.opts.integrate:
            data.extend(["ix", "iy", "iz"])

        self.data = {}
        self.axes = {}

        self.data.update({d: self.BifrostData.get_var(d, self.snap) for d in data})
        self.axes.update({a: self.BifrostData.get_var(a, self.snap) for a in axes})
        self.axes.update({"i" + a: inverse(self.axes[a]) for a in axes})

        self.global_constants = {
            "xmin": min(self.axes["x"]),
            "ymin": min(self.axes["y"]),
            "zmin": min(self.axes["z"]),
            "xmax": max(self.axes["x"]),
            "ymax": max(self.axes["y"]),
            "zmax": max(self.axes["z"]),
            "xlen": len(self.axes["x"]),
            "ylen": len(self.axes["y"]),
            "zlen": len(self.axes["z"]),
        }

        self.xl = self.global_constants["xlen"]
        self.yl = self.global_constants["ylen"]
        self.zl = self.global_constants["zlen"]
        self.zlen = 1 if slice else self.zl
        self.block = (8, 8, 1)
        self.grid = (int(self.xl / 8), int(self.yl / 8), 1)

        self.q = np.zeros((self.xl, self.yl, self.zlen), dtype=np.float32, order="F")

        print("Loaded data for snap", self.snap)

    def save_data(self, name, array):
        """
        Saves irender output in binary format.

        File has format (int dimensions (2)), (int xsize), (int ysize),
        (data array), (int x-axis size), (int y-axis size), (int z-axis
        size), (x array), (y array), (z array)
        """
        savearray(name, array)
        with open(name, mode="ab") as newfile:
            newfile.write(
                bytes(
                    np.array(
                        (self.axes["x"].size, self.axes["y"].size, self.axes["z"].size),
                        dtype="int32",
                    ).data
                )
            )
            newfile.write(bytes(self.axes["x"].data))
            newfile.write(bytes(self.axes["y"].data))
            newfile.write(bytes(self.axes["z"].data))

    def __init__(self, opts=None):
        # super(getDatanopars, self).__init__()

        # from bifrost import BifrostData
        if os.environ.get("CUDA_LIB", "null") == "null":
            os.environ["CUDA_LIB"] = (
                os.environ["LMSAL_HUB"] + "/sims_hub/br_hub/br_cuda/"
            )

        self.snap = opts.snap
        self.directory = opts.dir
        self.temp = opts.temp
        self.plane = opts.plane
        self.opts = opts
        self.save = opts.save

        self.get_data()


def savearray(name, array):
    """
    Saves an array in a binary format.

    File has format (int number of dimensions), (int xsize), (int ysize) .... (int lastdimensionsize),
    (lots of float32s that make up the remainder of the data, in C array format).
    """
    with open(name, mode="wb") as newfile:
        newfile.write(
            bytes(np.array((len(array.shape),) + array.shape, dtype="int32").data)
        )
        newfile.write(bytes(array.data))
