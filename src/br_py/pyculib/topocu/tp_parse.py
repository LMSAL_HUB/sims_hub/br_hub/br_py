import os


class parseArgs(object):
    def parse_args(self):
        """
        Parses arguments and locates snapshots.
        """

        def parse_file(self, file):
            """
            Parses filename and determines extent of snapshot series.
            """

            # String manipulations

            directory = os.path.dirname(file)
            file = os.path.basename(file)
            file = os.path.splitext(file)[0]
            temp = file.rstrip("0123456789")
            self.temp = temp
            snap = int(file[len(temp) :])
            temp += "%%0%ii" % len(file[len(temp) :])

            self.directory = directory

            # Searches for other snapshots from the same simulation

            self.snap = snap_min = snap_max = snap
            while os.path.exists(directory + "/" + temp % (snap_min - 1) + ".idl"):
                snap_min -= 1
            while os.path.exists(directory + "/" + temp % (snap_max + 1) + ".idl"):
                snap_max += 1
            self.snap_range = (snap_min, snap_max)

        from optparse import OptionParser

        if os.environ.get("CUDA_LIB", "null") == "null":
            os.environ["CUDA_LIB"] = (
                os.environ["LMSAL_HUB"] + "/sims_hub/br_hub/br_cuda/"
            )

        p = OptionParser()

        # General settings

        p.add_option(
            "-l",
            "--slice",
            dest="slice",
            default=False,
            action="store_true",
            help="Maps whole snapshot",
        )
        p.add_option(
            "-r",
            "--rcalc",
            dest="rcalc",
            default=False,
            action="store_true",
            help="Forces rcalculation and overwrites cache",
        )
        p.add_option(
            "-w",
            "--save",
            dest="save",
            default=False,
            action="store_true",
            help="Saves to current working directory",
        )

        # Data location

        p.add_option(
            "-f", "--file", dest="file", default=None, help="Path to simulation"
        )
        p.add_option(
            "-d",
            "--dir",
            dest="dir",
            default="/sanhome/juanms/mpi3drun/OSCB1/",
            help="Simulation directory",
        )
        p.add_option(
            "-t", "--temp", dest="temp", default="qsmag_bb01_it", help="Snapshot prefix"
        )
        p.add_option("-s", "--snap", dest="snap", default="140", help="Initial snap")
        p.add_option("-p", "--plane", dest="plane", default=25, help="Initial plane")

        # Calculation settings

        p.add_option(
            "-q",
            "--qfactor",
            dest="q",
            default=False,
            action="store_true",
            help="Calculates Q",
        )
        p.add_option(
            "-a",
            "--alt",
            dest="alt",
            default=False,
            action="store_true",
            help="Alternate formula for Q",
        )
        p.add_option(
            "-i",
            "--integrate",
            dest="integrate",
            default=False,
            action="store_true",
            help="Integrates along field",
        )
        p.add_option(
            "-c",
            "--connectivity",
            dest="connectivity",
            default=False,
            action="store_true",
            help="Maps connectivity",
        )

        (opts, args) = p.parse_args()

        self.opts = opts
        self.plane = int(opts.plane)

        parse_file(self, opts.file if opts.file else opts.dir + opts.temp + opts.snap)

    def __init__(self):
        self.parse_args()
