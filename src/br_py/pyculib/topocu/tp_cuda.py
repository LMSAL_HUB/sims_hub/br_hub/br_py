import numpy as np

import pycuda
import pycuda.autoinit


class cudaManipulator(object):
    def load_axes(self):
        """
        Loads all axes.
        """

        def load_axis(mod, name, axis):
            """
            Loads a single axis onto the GPU as a 1D texture Axes prefixed with
            'i' are assumed to be inverses, and are loaded with normalized
            coordinates.
            """

            tex = mod.get_texref(name + "tex")
            tex.set_flags(0)
            pycuda.driver.matrix_to_texref(
                np.expand_dims(axis, 0).astype(np.float32), tex, order="C"
            )
            # pycuda.driver.memcpy_htod(tex,axis)
            tex.set_address_mode(0, pycuda.driver.address_mode.CLAMP)
            tex.set_address_mode(1, pycuda.driver.address_mode.CLAMP)
            tex.set_filter_mode(pycuda.driver.filter_mode.LINEAR)

            if name[0] == "i":
                tex.set_flags(pycuda.driver.TRSF_NORMALIZED_COORDINATES)

            return tex

        self.loaded_axes = {}
        self.loaded_axes.update(
            {a: load_axis(self.mod, a, self.axes[a]) for a in self.axes.keys()}
        )

    def load_constants(self):
        """
        Loads all global constants Loads all specified constants.
        """

        def load_constant(mod, name, value):
            """
            Loads a single float32 constant to the GPU.
            """

            return pycuda.driver.memcpy_htod(
                mod.get_global(name)[0], np.array(np.float32(value))
            )

        self.loaded_constants = {}
        self.loaded_constants.update(
            {
                c: load_constant(self.mod, c, self.constants[c])
                for c in self.constants.keys()
            }
        )
        self.loaded_constants.update(
            {
                c: load_constant(self.mod, c, self.global_constants[c])
                for c in self.global_constants.keys()
            }
        )

    def load_textures(self):
        """
        Loads all specified textures.
        """

        def load_texture(mod, name, texture):
            """
            Loads a single 3D texture onto the GPU.
            """

            def array_to_gpu(arr, order=None):
                """
                Copies an array to the GPU.
                """

                if order is None:
                    order = "C" if arr.strides[0] > arr.strides[2] else "F"
                if order.upper() == "C":
                    d, h, w = arr.shape
                elif order.upper() == "F":
                    w, h, d = arr.shape
                else:
                    raise Exception("order must be either F or C")

                descr = pycuda.driver.ArrayDescriptor3D()
                descr.width = w
                descr.height = h
                descr.depth = d
                descr.format = pycuda.driver.dtype_to_array_format(arr.dtype)
                descr.num_channels = 1
                descr.flags = 0

                d_arr = pycuda.driver.Array(descr)

                copy = pycuda.driver.Memcpy3D()
                copy.set_src_host(arr)
                copy.set_dst_array(d_arr)
                copy.width_in_bytes = copy.src_pitch = arr.strides[1]
                copy.src_height = copy.height = h
                copy.depth = d

                copy()

                return d_arr

            tex = mod.get_texref(name)

            texture = texture.astype("float32").copy("F")
            texture = array_to_gpu(texture, "F")
            tex.set_array(texture)

            tex.set_address_mode(0, pycuda.driver.address_mode.WRAP)
            tex.set_address_mode(1, pycuda.driver.address_mode.WRAP)
            tex.set_address_mode(2, pycuda.driver.address_mode.WRAP)
            tex.set_filter_mode(pycuda.driver.filter_mode.LINEAR)

            return tex

        self.loaded_textures = {}
        self.loaded_textures.update(
            {
                t: load_texture(self.mod, t, self.textures[t])
                for t in self.textures.keys()
            }
        )

    def __init__(self):
        pass
