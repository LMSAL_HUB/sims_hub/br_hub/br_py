import os
import numpy as np
import helita.sim.cstagger as cs

from .tp_trace import tracer
from .tp_trace import tracernopars

from pycuda import gpuarray
from pycuda.compiler import SourceModule


class qCalculator(tracer):
    def init_q(self):
        """
        Initializes kernel.
        """

        with open(os.environ["CUDA_LIB"] + "TopoMaFiCuCo/q.cu") as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = (
            self.mod.get_function("complexQ")
            if self.opts.alt
            else self.mod.get_function("simpleQ")
        )

    def calculate_q(self, plane):
        """
        Executes kernel.
        """

        xar = np.reshape(
            np.ravel(np.atleast_3d(self.arrays["x"]), order="F"),
            (self.xl, self.yl, 1),
            order="F",
        )
        yar = np.reshape(
            np.ravel(np.atleast_3d(self.arrays["y"]), order="F"),
            (self.xl, self.yl, 1),
            order="F",
        )

        self.constants = {}

        self.textures = {
            "x": np.atleast_3d(self.arrays["x"]),
            "y": np.atleast_3d(self.arrays["y"]),
            "z": np.atleast_3d(self.arrays["z"]),
            "A": cs.ddxup(xar),
            "B": cs.ddyup(xar),
            "C": cs.ddxup(yar),
            "D": cs.ddyup(yar),
        }

        if self.opts.alt:
            self.textures.update(
                {
                    "bxc": np.atleast_3d(self.data["bxc"][:, :, plane]),
                    "byc": np.atleast_3d(self.data["byc"][:, :, plane]),
                    "bzc": np.atleast_3d(self.data["bzc"][:, :, plane]),
                }
            )

        self.load_constants()
        self.load_axes()
        self.load_textures()

        d_q = gpuarray.to_gpu(np.zeros((self.xl, self.yl), dtype=np.float32, order="F"))

        self.kernel(d_q, block=self.block, grid=self.grid)

        return d_q.get()

    def __init__(self):
        super(qCalculator, self).__init__()


class qCalculatornopars(tracernopars):
    def init_q(self):
        """
        Initializes kernel.
        """

        with open(os.environ["CUDA_LIB"] + "TopoMaFiCuCo/q.cu") as kernelfile:
            src_module = kernelfile.read()

        self.mod = SourceModule(src_module)
        self.kernel = (
            self.mod.get_function("complexQ")
            if self.opts.alt
            else self.mod.get_function("simpleQ")
        )

    def calculate_q(self, plane):
        """
        Executes kernel.
        """

        xar = np.reshape(
            np.ravel(np.atleast_3d(self.arrays["x"]), order="F"),
            (self.xl, self.yl, 1),
            order="F",
        )
        yar = np.reshape(
            np.ravel(np.atleast_3d(self.arrays["y"]), order="F"),
            (self.xl, self.yl, 1),
            order="F",
        )

        self.constants = {}

        self.textures = {
            "x": np.atleast_3d(self.arrays["x"]),
            "y": np.atleast_3d(self.arrays["y"]),
            "z": np.atleast_3d(self.arrays["z"]),
            "A": cs.ddxup(xar),
            "B": cs.ddyup(xar),
            "C": cs.ddxup(yar),
            "D": cs.ddyup(yar),
        }

        if self.opts.alt:
            self.textures.update(
                {
                    "bxc": np.atleast_3d(self.data["bxc"][:, :, plane]),
                    "byc": np.atleast_3d(self.data["byc"][:, :, plane]),
                    "bzc": np.atleast_3d(self.data["bzc"][:, :, plane]),
                }
            )

        self.load_constants()
        self.load_axes()
        self.load_textures()

        d_q = gpuarray.to_gpu(np.zeros((self.xl, self.yl), dtype=np.float32, order="F"))

        self.kernel(d_q, block=self.block, grid=self.grid)

        return d_q.get()

    def __init__(self, opts=None, parent=None):
        super(qCalculatornopars, self).__init__(opts)
