import os

import numpy as np
from scipy.interpolate import interp1d

import pycuda.driver as cuda
from pycuda.compiler import SourceModule
from helita.sim.bifrost import BifrostData, Bifrost_units
from helita.sim.muram import MuramAtmos

units = Bifrost_units()

with open(os.path.join(os.environ["CUDA_LIB"], "dem/dem.cu")) as kernelfile:  # NOEOS
    SERCUDACODE = kernelfile.read()

BLOCKSIZE = 256


class DEMCUDAManipulator(object):
    """
    Class for using CUDA, manipulating objects and textures conveniently.
    """

    def load_texture(self, name, arr):
        """
        Loads an array into a texture with a name.

        Address by the name in the kernel code.
        """
        tex = self.mod.get_texref(name)  # x*y*z
        arr = arr.astype("float32")

        if len(arr.shape) == 3:
            carr = arr.copy("F")
            texarray = numpy3d_to_array(carr, "F")
            tex.set_array(texarray)
        else:
            if len(arr.shape) == 1:
                arr = np.expand_dims(arr, 1)
            tex.set_flags(0)
            cuda.matrix_to_texref(arr, tex, order="F")

        tex.set_address_mode(0, cuda.address_mode.CLAMP)
        tex.set_address_mode(1, cuda.address_mode.CLAMP)
        tex.set_flags(cuda.TRSF_NORMALIZED_COORDINATES)
        tex.set_filter_mode(cuda.filter_mode.LINEAR)
        self.textures[name] = tex

    def load_constant(self, name, val):
        """
        Loads a constant into memory by name in kernel code.

        If val is a float, int, char, etc., it must be wrapped by
        np.float32() or np.int32() or similar.
        """
        cuda.memcpy_htod(self.mod.get_global(name)[0], np.array(val))

    def clear_textures(self):
        """
        Removes all textures from memory.
        """
        self.textures = {}

    def __init__(self, cuda_code, include_dirs=[]):
        include_dirs += [
            os.path.join(os.environ["CUDA_LIB"], "dem/"),
            os.path.join(os.environ["CUDA_LIB"], "ioni/renderer/"),
        ]
        self.mod = SourceModule(cuda_code, no_extern_c=True, include_dirs=include_dirs)


class DEMSingAxisRenderer(DEMCUDAManipulator):
    locph = float("nan")
    snap = 0

    x_pixel_offset = (
        0  # if 0, the output is centered in the box center--shifts output L/R
    )
    y_pixel_offset = 0  # shifts output U/D
    ##nsteps = 600
    mod = None  # CUDA kernel code
    xaxis = yaxis = zaxis = ixaxis = iyaxis = izaxis = None
    singlaxis = True
    ##ux = uy = uz = e = r = oscdata = ka_table = opatab = None
    textures = {}  # stores references to textures to prevent them being cleared

    def render(
        self,
        axis,
        gridsplit,
        consts,
        split_tables,
        dem_render,
        stepsize=0.001,
        verbose=False,
    ):
        self.clear_textures()

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size
        self.stepsize = stepsize  # size of a step along the LOS

        tables = [
            ("xtex", np.expand_dims(self.xaxis, 1)),
            ("ixtex", np.expand_dims(self.ixaxis, 1)),
            ("iytex", np.expand_dims(self.iyaxis, 1)),
            ("iztex", np.expand_dims(self.izaxis, 1)),
        ]

        self.axis = axis

        if axis == 0:
            intaxis = self.xaxis
            ax_id = 0
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            intaxis = self.yaxis
            ax_id = 1
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            intaxis = self.zaxis
            ax_id = 2
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        self.maxgridsize = (
            gridsplit * self.projection_x_size * self.projection_y_size
        )  # This must be a multiple of xsize and ysize
        numsplits = int(input_size / self.maxgridsize + 1)
        splitsize = np.ceil(intaxis.size / numsplits)

        # dz = np.gradient(intaxis) -> split in numsplitsintaxis size, i.e., splitsize timestep
        # in CUDA ds step size within the texture (0,1), int (dz)_ numsplitsintaxis width of
        # broken z. In cuda
        # table_splits['aptex'] = [np.gradient(intaxis) * np.float32(intaxis.size * stepsize) * 1.0e-6 * Bifrost_units.u_l * Bifrost_units.ergd2wd for numb in range(numsplits)]

        for tup in tables:
            self.load_texture(*tup)
        for tup in consts:
            self.load_constant(*tup)
        self.load_constant("axis", np.uint8(axis))
        self.load_constant("projectionXsize", np.int32(self.projection_x_size))
        self.load_constant("projectionYsize", np.int32(self.projection_y_size))
        self.load_constant("ds", np.float32(self.stepsize))

        if verbose:
            print("Loaded textures, computed emissivities", end="\r", flush=True)

        # JMS the following 5 lines are not in the other Renderer because change on axis cut.
        # split_tables is tables to split, table_splits is list of split tables

        table_splits = {
            name: np.array_split(table, numsplits, ax_id)
            for name, table in split_tables
        }

        for i in range(0, numsplits):
            start = i * splitsize
            if start + splitsize > intaxis.size:
                splitsize = intaxis.size - start

            if verbose:
                print(
                    "Rendering "
                    + str(axis)
                    + "-coords "
                    + str(start)
                    + "-"
                    + str(start + splitsize)
                    + " of "
                    + str(intaxis.size)
                    + " with ds=%8.6f" % np.float32(self.stepsize),
                    end="\r",
                    flush=True,
                )

            for name, table_split in table_splits.items():
                if name == "dstex":
                    # this is the missing splitsize in constant in spectanalys/__init__.py
                    table_split[i][:, :, :] *= splitsize

                self.load_texture(name, table_split[i])
            # for tup in split_tables:
            #    name, table = tup
            #    self.load_texture(name, table[start:start + splitsize])
            data_size = int(self.projection_x_size * self.projection_y_size)
            grid_size = int((data_size + BLOCKSIZE - 1) / BLOCKSIZE)

            self.load_constant("xstart", np.int32(start))
            self.load_constant("sliceWidth", np.int32(self.projection_y_size))

            dem_render(self, BLOCKSIZE, grid_size)

    def __init__(self, cuda_code):
        """
        Initialize with a cuda kernel.

        CUDA kernel should include renderer.h to utilize this
        """
        super(DEMSingAxisRenderer, self).__init__(cuda_code)

    def set_axes(self, xaxis, yaxis, zaxis):
        """
        Sets the x, y, and z axes.

        x, y, z axes are lists correlating indices (in textures) to
        locations in space.
        """
        self.xaxis = xaxis
        self.yaxis = yaxis
        self.zaxis = zaxis
        self.load_constant("xmin", np.float32(xaxis.min()))
        self.load_constant("xmax", np.float32(xaxis.max()))
        self.load_constant("ymin", np.float32(yaxis.min()))
        self.load_constant("ymax", np.float32(yaxis.max()))
        self.load_constant("zmin", np.float32(zaxis.min()))
        self.load_constant("zmax", np.float32(zaxis.max()))
        self.load_constant("xtotalsize", np.int32(xaxis.size))

        self.ixaxis = norm_inverse_axis(xaxis)
        if len(yaxis) > 1:
            self.iyaxis = norm_inverse_axis(yaxis)
        else:
            self.iyaxis = [0]
        self.izaxis = norm_inverse_axis(zaxis)


class DEMRenderer(DEMSingAxisRenderer):
    ixcutbot = float("nan")
    snap = None

    def __init__(
        self,
        snaprange,
        name_template,
        axis=2,
        data_dir="./",
        snap=None,
        cstagop=True,
        xcut=None,
        ycut=None,
        zcut=None,
        obj=None,
        var=None,
        _DataClass=BifrostData,
    ):
        DEMSingAxisRenderer.__init__(self, SERCUDACODE)

        self.data_dir = data_dir
        self.template = name_template
        self.snap_range = snaprange

        self.cstagop = cstagop

        self.xcut = xcut
        self.ycut = ycut
        self.zcut = zcut

        if snap is None:
            snap = self.snap_range[0]
        self.DataClass = _DataClass
        if obj is None:
            if self.DataClass == BifrostData:
                self.obj = self.DataClass(
                    self.template,
                    fdir=self.data_dir,
                    cstagop=self.cstagop,
                    snap=snap,
                    sel_units="cgs",
                )
            elif self.DataClass == MuramAtmos:
                print("Muram")
                self.obj = self.DataClass(
                    template=self.template, fdir=self.data_dir, iz0=400
                )

        else:
            self.obj = obj
        self.axis = axis
        self.set_snap(snap, var=var)

    def set_snap(self, snap, var=None):
        """
        Sets the timestamp with which to view the data.

        snap should be an integer between snap_range[0] and
        snap_range[1], inclusive
        """

        if self.snap == snap:
            return
        self.snap = snap
        if snap > self.snap_range[1] or snap < self.snap_range[0]:
            raise ValueError(
                "Time must be in the interval ("
                + str(self.snap_range[0])
                + ", "
                + str(self.snap_range[1])
                + ")"
            )

        if self.ixcutbot != self.ixcutbot:  # axes not loaded yet
            self.update_axes(self.xcut, self.ycut, self.zcut)

        self.tg = np.log10(
            self.obj.trans2comm("tg", self.snap)[
                self.ixcutbot : self.ixcuttop,
                self.iycutbot : self.iycuttop,
                self.izcutbot : self.izcuttop,
            ]
        )  # NOEOS
        self.ems = self.obj.trans2comm("emiss")[
            self.ixcutbot : self.ixcuttop,
            self.iycutbot : self.iycuttop,
            self.izcutbot : self.izcuttop,
        ]  # NOEOS

        self.dx = self.obj.dx
        self.dy = self.obj.dy

        if var is not None:
            var = self.obj.trans2comm(var)[
                self.ixcutbot : self.ixcuttop,
                self.iycutbot : self.iycuttop,
                self.izcutbot : self.izcuttop,
            ]  # NOEOS
            self.ems *= var
        # km/s
        if self.axis == 0:
            self.uu = (
                self.obj.trans2comm("ux", self.snap)[
                    self.ixcutbot : self.ixcuttop,
                    self.iycutbot : self.iycuttop,
                    self.izcutbot : self.izcuttop,
                ]
                / 1e5
            )
            self.ds = self.obj.dx1d[self.ixcutbot : self.ixcuttop]
        elif self.axis == 1:
            self.uu = (
                self.obj.trans2comm("uy", self.snap)[
                    self.ixcutbot : self.ixcuttop,
                    self.iycutbot : self.iycuttop,
                    self.izcutbot : self.izcuttop,
                ]
                / 1e5
            )
            self.ds = self.obj.dy1d[self.iycutbot : self.iycuttop]
        else:
            self.uu = (
                self.obj.trans2comm("uz", self.snap)[
                    self.ixcutbot : self.ixcuttop,
                    self.iycutbot : self.iycuttop,
                    self.izcutbot : self.izcuttop,
                ]
                / 1e5
            )
            self.ds = self.obj.dz1d[self.izcutbot : self.izcuttop]

        dims = ["i", "j", "k"]
        self.ems = np.einsum(
            "ijk,{0:s}->ijk".format(dims[self.axis]), self.ems, self.ds
        )

    def load_snap(self, tg, uu, ems):
        """
        Sets the timestamp with which to view the data.

        snap should be an integer between snap_range[0] and
        snap_range[1], inclusive
        """

        if self.ixcutbot != self.ixcutbot:  # axes not loaded yet
            self.update_axes(self.xcut, self.ycut, self.zcut)

        self.tg = np.log10(
            tg[
                self.ixcutbot : self.ixcuttop,
                self.iycutbot : self.iycuttop,
                self.izcutbot : self.izcuttop,
            ]
        )  # NOEOS
        self.uu = uu[
            self.ixcutbot : self.ixcuttop,
            self.iycutbot : self.iycuttop,
            self.izcutbot : self.izcuttop,
        ]  # NOEOS
        self.ems = ems[
            self.ixcutbot : self.ixcuttop,
            self.iycutbot : self.iycuttop,
            self.izcutbot : self.izcuttop,
        ]  # NOEOS

        self.dx = self.obj.dx
        self.dy = self.obj.dy

    def update_axes(self, xcut=None, ycut=None, zcut=None):
        """
        Loads/reloads the axes, changing the z-cutoff if zcut is not None.
        """
        if zcut is not None:
            self.zcut = zcut

        xaxis = self.obj.x.astype("float32")
        yaxis = self.obj.y.astype("float32")
        zaxis = self.obj.z.astype("float32")

        if self.xcut is not None:
            self.ixcutbot = np.argmin(np.abs(xaxis - self.xcut[0]))
            self.ixcuttop = np.argmin(np.abs(xaxis - self.xcut[1]))

        else:
            self.ixcutbot = 0
            self.ixcuttop = len(xaxis)

        xaxis = xaxis[self.ixcutbot : self.ixcuttop]

        if self.ycut is not None:
            self.iycutbot = np.argmin(np.abs(yaxis - self.ycut[0]))
            self.iycuttop = np.argmin(np.abs(yaxis - self.ycut[1]))

        else:
            self.iycutbot = 0
            self.iycuttop = len(yaxis)

        yaxis = yaxis[self.iycutbot : self.iycuttop]

        if self.zcut is not None:
            self.izcutbot = np.argmin(np.abs(zaxis - self.zcut[0]))
            self.izcuttop = np.argmin(np.abs(zaxis - self.zcut[1]))

        else:
            self.izcutbot = 0
            self.izcuttop = len(zaxis)

        zaxis = zaxis[self.izcutbot : self.izcuttop]
        self.set_axes(xaxis, yaxis, zaxis)

    def load_minmax(self, tgmin, tgmax, uumin, uumax):
        self.uumin = uumin
        self.uumax = uumax
        self.tgmin = tgmin
        self.tgmax = tgmax

    def dem_render(self, axis, verbose=True, fw=None, gridsplit=20, stepsize=0.001):
        self.stepsize = stepsize  # size of a step along the LOS
        if axis == 0:
            gridsize_adj = np.float32(stepsize) * self.dx
            intaxis = self.xaxis
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            gridsize_adj = np.float32(stepsize) * self.dy
            intaxis = self.yaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            gridsize_adj = np.float32(stepsize)
            intaxis = self.zaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size

        self.maxgridsize = (
            gridsplit * self.projection_x_size * self.projection_y_size
        )  # This must be a multiple of xsize and ysize
        numsplits = int(input_size / self.maxgridsize + 1)
        np.ceil(intaxis.size / numsplits)
        # It is in cgs

        dsc = np.ones_like(
            self.tg
        )  # dsc contains the grid spacing of the selected axis (in 3D array)
        if axis == 0:
            dsc *= gridsize_adj
        elif axis == 1:
            dsc *= gridsize_adj
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix, iiy, :] *= (np.gradient(intaxis))[:] * gridsize_adj

        consts = [
            ("tgmin", np.float32(self.tgmin)),
            ("tgmax", np.float32(self.tgmax)),
            ("uumin", np.float32(self.uumin)),
            ("uumax", np.float32(self.uumax)),
        ]

        split_tables = [
            ("emstex", self.ems * 1.0),  # NOEOS
            ("uutex", self.uu),  # NOEOS
            ("tgtex", self.tg),
            ("dstex", dsc),
        ]  # NOEOS

        tempout = np.zeros(
            (self.projection_x_size, self.projection_y_size), dtype="float32"
        )

        def idem_render(self, blocksize, gridsize):
            frender = self.mod.get_function("demRender")

            # integrates to find the emission
            frender(
                cuda.Out(tempout),
                block=(int(blocksize), 1, 1),
                grid=(int(gridsize), 1, 1),
            )
            idem_render.datout += tempout

        idem_render.datout = np.zeros_like(tempout)

        self.render(
            axis, gridsplit, consts, split_tables, idem_render, stepsize, verbose
        )

        return idem_render.datout

    def demb_render(self, axis, verbose=True, fw=None, gridsplit=20, stepsize=0.001):
        self.stepsize = stepsize  # size of a step along the LOS
        if axis == 0:
            gridsize_adj = np.float32(stepsize) * self.dx
            intaxis = self.xaxis
            self.projection_x_size = self.yaxis.size
            self.projection_y_size = self.zaxis.size
        elif axis == 1:
            gridsize_adj = np.float32(stepsize) * self.dy
            intaxis = self.yaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.zaxis.size
        else:
            gridsize_adj = np.float32(stepsize)
            intaxis = self.zaxis
            self.projection_x_size = self.xaxis.size
            self.projection_y_size = self.yaxis.size

        input_size = self.xaxis.size * self.yaxis.size * self.zaxis.size

        self.maxgridsize = (
            gridsplit * self.projection_x_size * self.projection_y_size
        )  # This must be a multiple of xsize and ysize
        numsplits = int(input_size / self.maxgridsize + 1)
        np.ceil(intaxis.size / numsplits)
        # It is in cgs

        dsc = np.ones_like(
            self.tg
        )  # dsc contains the grid spacing of the selected axis (in 3D array)
        if axis == 0:
            dsc *= gridsize_adj
        elif axis == 1:
            dsc *= gridsize_adj
        else:
            for iix in range(self.projection_x_size):
                for iiy in range(self.projection_y_size):
                    dsc[iix, iiy, :] *= (np.gradient(intaxis))[:] * gridsize_adj

        consts = [
            ("tgmin", np.float32(self.tgmin)),
            ("tgmax", np.float32(self.tgmax)),
            ("nelmin", np.float32(self.nelmin)),
            ("nelmax", np.float32(self.nelmax)),
            ("bmin", np.float32(self.bmin)),
            ("bmax", np.float32(self.bmax)),
        ]

        split_tables = [
            ("emstex", self.ems * 1.0),  # NOEOS
            ("btex", self.modb),  # NOEOS
            ("neltex", self.nel),  # NOEOS
            ("tgtex", self.tg),
            ("dstex", dsc),
        ]  # NOEOS

        tempout = np.zeros(
            (self.projection_x_size, self.projection_y_size), dtype="float32"
        )

        def idem_render(self, blocksize, gridsize):
            frender = self.mod.get_function("dembRender")

            # integrates to find the emission
            frender(
                cuda.Out(tempout),
                block=(int(blocksize), 1, 1),
                grid=(int(gridsize), 1, 1),
            )
            idem_render.datout += tempout

        idem_render.datout = np.zeros_like(tempout)

        self.render(
            axis, gridsplit, consts, split_tables, idem_render, stepsize, verbose
        )

        return idem_render.datout


def numpy3d_to_array(np_array, order=None):
    """
    Method for copying a numpy array to a CUDA array.

    If you get a buffer error, run this method on np_array.copy('F')
    """
    from pycuda.driver import Array, ArrayDescriptor3D, Memcpy3D, dtype_to_array_format

    if order is None:
        order = "C" if np_array.strides[0] > np_array.strides[2] else "F"

    if order.upper() == "C":
        d, h, w = np_array.shape
    elif order.upper() == "F":
        w, h, d = np_array.shape
    else:
        raise Exception("order must be either F or C")

    descr = ArrayDescriptor3D()
    descr.width = w
    descr.height = h
    descr.depth = d
    descr.format = dtype_to_array_format(np_array.dtype)
    descr.num_channels = 1
    descr.flags = 0

    device_array = Array(descr)

    copy = Memcpy3D()
    copy.set_src_host(np_array)
    copy.set_dst_array(device_array)
    copy.width_in_bytes = copy.src_pitch = np_array.strides[1]
    copy.src_height = copy.height = h
    copy.depth = d

    copy()

    return device_array


def norm_inverse_axis(axis):
    """
    From an axis (corresponds table indices to real coordinates), generate an
    inverse lookup table that will take real coordinates (normalized to (0, 1))
    and return a normalized (0, 1) lookup index.
    """
    axinverse = interp1d(
        (axis - axis.min()) / np.ptp(axis), np.linspace(0, 1, axis.size)
    )
    return axinverse(np.linspace(0, 1, axis.size * 6)).astype("float32")
