"""
Set of plotting and movies tools.
"""
import importlib

__all__ = ["dem"]

spam_spec = importlib.util.find_spec("pycuda")
found = spam_spec is not None

if found:
    try:
        pass
    except Exception:  # NOQA: BLE001
        found = False
        print("br_dem is not imported, pycuda.autoinit import failed")

if found:
    from . import dem
