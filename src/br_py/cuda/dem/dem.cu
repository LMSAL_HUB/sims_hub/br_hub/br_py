#include "renderer.cuh"

/**
This code (which the amount of data could be reduce (aptex not used anymore,
dtex and entex could be combined)  calculates the intensities along
the axis assuming SE
**/
texture<float, 3, cudaReadModeElementType> emstex; // 3D texture
texture<float, 3, cudaReadModeElementType> dstex;  // 3D texture
texture<float, 3, cudaReadModeElementType> tgtex;  // 3D texture
texture<float, 3, cudaReadModeElementType> uutex;  // 3D texture


__device__ float3 pointSpecificEmiss(float x, float y, float z) {
    float dst = tex3D(dstex, x, y, z);             // cgs
    float ems = tex3D(emstex, x, y, z);            // cgs

    return make_float3(ems * dst,0,0);

}
extern "C" {
    __global__ void demRender(float *out) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx % projectionYsize;
        int xpixel = idx / projectionYsize;
        float emiss = 0;

        float3 positionIncrement = viewVector * ds;
        float3 currentPosition = initialCP(xpixel, ypixel);
        float3 np; //slice-normalized, scaled position

        if (currentPosition.x == INFINITY) {
            out[idx] = 0;
            return;
        }

        do {
            np = realToNormalized(currentPosition);

            emiss += pointSpecificEmiss(np.x, np.y, np.z).x;

            currentPosition += positionIncrement;
        } while (isInSlice(currentPosition));

        out[idx] = emiss;
    }
}
