__constant__ float xlen;
__constant__ float ylen;
__constant__ float zlen;

__constant__ float xmin;
__constant__ float ymin;
__constant__ float zmin;

__constant__ float xmax;
__constant__ float ymax;
__constant__ float zmax;

texture<float, 1, cudaReadModeElementType> xtex;
texture<float, 1, cudaReadModeElementType> ytex;
texture<float, 1, cudaReadModeElementType> ztex;

texture<float, 1, cudaReadModeElementType> ixtex;
texture<float, 1, cudaReadModeElementType> iytex;
texture<float, 1, cudaReadModeElementType> iztex;

texture<float, cudaTextureType3D, cudaReadModeElementType> bxc;
texture<float, cudaTextureType3D, cudaReadModeElementType> byc;
texture<float, cudaTextureType3D, cudaReadModeElementType> bzc;

texture<float, cudaTextureType3D, cudaReadModeElementType> x;
texture<float, cudaTextureType3D, cudaReadModeElementType> y;
texture<float, cudaTextureType3D, cudaReadModeElementType> z;
texture<float, cudaTextureType3D, cudaReadModeElementType> l;

texture<float, cudaTextureType3D, cudaReadModeElementType> A;
texture<float, cudaTextureType3D, cudaReadModeElementType> B;
texture<float, cudaTextureType3D, cudaReadModeElementType> C;
texture<float, cudaTextureType3D, cudaReadModeElementType> D;

__device__ int arrayIndex(int3 a) {

  /*
  Flattens 3D array lookup <a> to 1D index <idx>
  */

  long idx = a.x + xlen * a.y; // + xlen * ylen * a.z;
  return idx;

}

__device__ float3 textureIndex(float3 a) {

  /*
  Converts from real- to index- space coordinates
  */

  a.x = fmodf(a.x, xmax - xmin);
  a.y = fmodf(a.y, ymax - ymin);

  a.x = tex1D(ixtex, (a.x - xmin) / (xmax - xmin)) * (xlen-1) + 0.5f;
  a.y = tex1D(iytex, (a.y - ymin) / (ymax - ymin)) * (ylen-1) + 0.5f;
  a.z = tex1D(iztex, (a.z - zmin) / (zmax - zmin)) * (zlen-1) + 0.5f;

  return a;

}

__device__ float4 derivatives(int3 a){

  /*
  Gets the derivatives (textures A, B, C, D) at <a>
  */

  a.x = a.x + 0.5f;
  a.y = a.y + 0.5f;
  a.z = a.z + 0.5f;

  float4 b;

  b.x = tex3D(A, a.x, a.y, a.z);
  b.y = tex3D(B, a.x, a.y, a.z);
  b.z = tex3D(C, a.x, a.y, a.z);
  b.w = tex3D(D, a.x, a.y, a.z);

  return b;

}

__device__ float2 magnitudes(int3 a){

  /*
  Gets the magnitude of the magnetic field where the line passing through <a> intersects the plane (two points)
  */

  float3 b, c;
  float2 m;

  a.x = a.x + 0.5f;
  a.y = a.y + 0.5f;
  a.z = a.z + 0.5f;

  b.x = tex3D(x, a.x, a.y, a.z);
  b.y = tex3D(y, a.x, a.y, a.z);
  b.z = 0;

  b = textureIndex(b);

  c.x = tex3D(bxc, a.x, a.y, a.z);
  c.y = tex3D(byc, a.x, a.y, a.z);
  c.z = tex3D(bzc, a.x, a.y, a.z);

  m.x = sqrt(c.x * c.x + c.y * c.y + c.z * c.z);

  c.x = tex3D(bxc, b.x, b.y, b.z);
  c.y = tex3D(byc, b.x, b.y, b.z);
  c.z = tex3D(bzc, b.x, b.y, b.z);

  m.y = sqrt(c.x * c.x + c.y * c.y + c.z * c.z);

  return m;

}

__global__ void simpleQ(float *d_q){

  /*
  Simpler calculation for Q
  */

  int3 a;

  a.x = (blockIdx.x * blockDim.x) + threadIdx.x;
  a.y = (blockIdx.y * blockDim.y) + threadIdx.y;
  a.z = 0;

  long idx = arrayIndex(a);
  float4 d = derivatives(a);

  float numerator, denominator;

  numerator = (d.x * d.x + d.y * d.y + d.z * d.z + d.w * d.w);
  denominator = abs(d.x * d.w - d.y * d.z);

  float qVal = numerator / denominator;

  d_q[idx] = qVal;

}

__global__ void complexQ(float *d_q){

  /*
  More complicated calculation for Q
  */

  int3 a;

  a.x = (blockIdx.x * blockDim.x) + threadIdx.x;
  a.y = (blockIdx.y * blockDim.y) + threadIdx.y;
  a.z = 0;

  long idx = arrayIndex(a);
  float4 d = derivatives(a);
  float2 m = magnitudes(a);

  float numerator, denominator;

  numerator = (d.x * d.x + d.y * d.y + d.z * d.z + d.w * d.w);
  denominator = abs(m.x / m.y);

  float qVal = numerator / denominator;

  d_q[idx] = qVal;

}
