"""
Set of programs to read and interact with output from BifrostData simulations
focus on optically thin radiative transfer and similar.
"""

import numpy as np
import os
import datetime
from helita.sim.bifrost import Bifrost_units, Opatab
from helita.sim.bifrost import BifrostData as OrigBifrostData
from helita.sim.muram import MuramAtmos, Muram_units
from helita.sim.pypluto import PlutoData, Pypluto_paolo_units
from .dem_tools import get_vdem, get_radyn_vdem, get_vdem_highrun
from glob import glob
import time
import importlib
import pickle
import shlex
from collections import namedtuple
import xarray as xr
import scipy.ndimage as ndimage
import struct
import torch
from typing import Callable
import inspect

spam_spec = importlib.util.find_spec("pycuda")
found = spam_spec is not None

# goft_path = os.path.join(os.environ.get('LMSAL_HUB'),'sims_hub/br_intcu/br_ioni/data/')

try:
    goft_path = os.environ.get("GOFT_PATH")
    print("GOFT_PATH try {}".format(goft_path))
except Exception:  # NOQA: BLE001
    goft_path = os.path.join(
        os.environ.get("LMSALHUB"), "/sims_hub/br_intcu/br_ioni/data/"
    )
    print("GOFT_PATH except {}".format(goft_path))

genxpath = "IDL/data/lines/emissivity_functions/"

units = Bifrost_units()
# all cgs below
cm2AA = 1.0e8
EE = units.ev_to_erg  # 1.602189e-12
HH = units.hplanck  # 6.626176e-27
CC = units.clight  # 2.99792458e10 cm/s
CCA = CC * cm2AA  # AA/s
HCE = HH / EE * CC * cm2AA

Egi = namedtuple("Egi", ["ev", "g", "label", "ion"])  # Einsten coeff for OOE
Trn = namedtuple("Trn", ["irad", "jrad", "alamb", "a_ul", "f"])  # Coeff for OOE


def UVOTRTData(*init_args, _class="bifrost", **init_kwargs):
    return SelectHel(_class=_class)(*init_args, **init_kwargs)


def SelectHel(_class="bifrost"):
    if _class == "muram":
        BifrostData = MuramAtmos
        units = Muram_units()
    elif _class == "pypluto":
        BifrostData = PlutoData
        units = Pypluto_paolo_units()
    elif _class == "bifrost":
        BifrostData = OrigBifrostData
        units = Bifrost_units()

    class _UVOTRTData(BifrostData):
        """
        Class that operates radiative transfer form BifrostData simulations in
        native format.
        """

        def __init__(self, *args, **kwargs):
            super(_UVOTRTData, self).__init__(
                *args, sel_units="cgs", **kwargs, verbose=False
            )

        def load_intny_module(
            self,
            axis=2,
            azimuth=None,
            altitude=None,
            ooe=False,
            do_mit=None,
            zcutoff=0,
            abnd_arr=None,
            *args,
            **kwargs,
        ):
            if found:
                if os.environ.get("CUDA_LIB", "null") == "null":
                    os.environ["CUDA_LIB"] = os.path.join(os.environ["BIFROST"], "CUDA")

            if azimuth is not None or altitude is not None:  # For any angle
                if ooe:
                    choice = "tdi"
                else:
                    choice = "static"
                if azimuth is not None:
                    azimuth = 90.0
                if altitude is not None:
                    altitude = 0.0
            else:  # For a specific axis, this is setup with axis =[0,1,2]
                if ooe:
                    choice = "satdi"
                else:
                    choice = "sastatic"
                azimuth = 90.0
                altitude = 0.0
            opts = int_options()
            opts.infile = self.file_root
            opts.snap = self.snap
            opts.rendtype = choice
            opts.simdir = self.fdir
            data_dir = self.fdir  # (opts.simdir if opts.simdir else askdirectory(
            # title='Simulation Directory')).rstrip('/')
            if ooe:
                opts.tdiparam = self.get_param("atomfl").strip(" ")
            print("load_intny_module: snap = ", opts.snap)

            snap_range = (self.snap, self.snap)
            # + '_' + '%%0%ii' % np.max((len(str(self.snap)),3))
            template = opts.infile

            # from br_ioni import RenderGUI
            # OOE along any angle
            if opts.rendtype == "tdi":
                from ..pyculib.intcu.uvrt import TDIEmRenderer

                tdi_paramfile_abs = (
                    opts.tdiparam
                    if opts.tdiparam
                    else askopenfilename(title="OOE Ionization Paramfile")
                )
                tdi_paramfile = os.path.relpath(tdi_paramfile_abs, data_dir)
                self.intcudamod = TDIEmRenderer(
                    snap_range,
                    template,
                    data_dir=data_dir,
                    paramfile=tdi_paramfile,
                    snap=opts.snap,
                    cstagop=self.cstagop,
                )

            # Statistical Equibilibrium along specific axis: x, y or z
            elif opts.rendtype == "sastatic":
                from ..pyculib.intcu.uvrt import SAStaticEmRenderer

                self.intcudamod = SAStaticEmRenderer(
                    snap_range,
                    template,
                    axis=axis,
                    data_dir=data_dir,
                    snap=opts.snap,
                    cstagop=self.cstagop,
                    _DataClass=BifrostData,
                    do_mit=do_mit,
                    zcutoff=zcutoff,
                )

            # OOE along specific axis, i.e., x, y or z
            elif opts.rendtype == "satdi":
                from ..pyculib.intcu.uvrt import SATDIEmRenderer

                tdi_paramfile_abs = (
                    opts.tdiparam
                    if (opts.tdiparam)
                    else askopenfilename(title="OOE Ionization Paramfile")
                )
                tdi_paramfile = os.path.relpath(tdi_paramfile_abs, data_dir)
                self.intcudamod = SATDIEmRenderer(
                    snap_range,
                    template,
                    axis=axis,
                    data_dir=data_dir,
                    paramfile=tdi_paramfile,
                    snap=opts.snap,
                    cstagop=self.cstagop,
                )

            # Statistical Equibilibrium along any direction
            else:
                from ..pyculib.intcu.uvrt import StaticEmRenderer

                self.intcudamod = StaticEmRenderer(
                    snap_range,
                    template,
                    data_dir=data_dir,
                    snap=opts.snap,
                    zcutoff=zcutoff,
                    cstagop=self.cstagop,
                )

        def get_intny(
            self,
            spline,
            nlamb=141,
            axis=2,
            rend_opacity=False,
            dopp_width_range=5e1,
            azimuth=None,
            gridsplit=128,
            altitude=None,
            ooe=False,
            stepsize=0.01,
            do_mit=None,
            zcutoff=0,
            abnd_arr=None,
            *args,
            **kwargs,
        ):
            """
            Calculates intensity profiles from the simulation quantiables.

            Parameters
            ----------
            spline - string
                Name of the spectral line to calculate. In order to know the
                format, $BIFROST/PYTHON/br_int/br_ioni/data
                contains files with the G(T,ne), usually name.opy. spline must be
                name, e.g., 'fe_8_108.073'. If you dont have any, rung form
                helita.sim.atom_tools create_goftne_tab (very simple to run).
            nlamb - integer
                Number of points along the wavelength axis.
            dopp_width_range - float number
                It selects the wavelength range. The value means.
            axis - integer number: 0 = x, 1 = y, 2 = z
                It allows to chose the LOS integration axis
            rend_opacity - allows to calculate opacities.
                Right now uses a very obsolete table; so I suggest to wait until
                further improvements.
            azimuth -  This allows to trace rays for any angle. In this cases uses
                none sa modules. In this case, axis parameter is not need to
                be used.
            altitude -  This allows to trace rays for any angle. In this
                cases uses none sa modules.
                In this case, axis parameter is not need to be used.
            ooe -  True or False. Default is False.
                If true, uses Olluri out of equilibrium ionization output
            Returns
            -------
            array - ndarray
                Array with the dimensions of the 2D spatial from the simulation
                and spectra.

            Notes
            -----
                uses cuda
            """
            rend_reverse = False

            print("tools get_intny ", ooe)
            if not ooe:
                acont_filenames = self.acont_opy_filepath(spline)
                channel = 0
            else:
                wvl_trans = float(spline.split("_")[2].split(".opy")[0])
                trns = self.ratom(self.get_param("atomfl").strip(" "))
                for it, trans in enumerate(trns):
                    if np.abs(trans.alamb - wvl_trans) < 0.1:
                        level = it
                acont_filenames = None
                channel = level

            if not hasattr(self, "intcudamod"):
                self.load_intny_module(
                    axis=axis,
                    azimuth=azimuth,
                    altitude=altitude,
                    ooe=ooe,
                    do_mit=do_mit,
                    zcutoff=zcutoff,
                )

            self.intcudamod.acont_tables = []
            if not ooe:
                self.intcudamod.save_accontfiles(acont_filenames)
            return self.intcudamod.il_render(
                channel,
                azimuth,
                altitude,
                axis,
                reverse=rend_reverse,
                gridsplit=gridsplit,
                nlamb=nlamb,
                dopp_width_range=dopp_width_range,
                opacity=rend_opacity,
                verbose=self.verbose,
                stepsize=stepsize,
                abnd_arr=abnd_arr,
                do_mit=do_mit,
            )

        def load_intny(self, spline, snap=None, axis=2, dir="./"):
            """
            Load saved profiles from a binary file.
            """
            if snap is None:
                snap = self.snap
            if axis == 0:
                axis_txt = "_yz_"
            elif axis == 1:
                axis_txt = "_xz_"
            else:
                axis_txt = "_xy_"

            self.snap = snap
            spfilebin = os.path.join(
                dir, spline + axis_txt + "it" + str(self.snap) + ".bin"
            )
            fp = np.memmap(spfilebin, dtype="int32", mode="r", shape=3)
            nwvl = fp[2]
            nx = fp[0]
            ny = fp[1]
            intny = {}
            fp = np.memmap(
                spfilebin, dtype="float32", mode="r", shape=nwvl, offset=3 * 4
            )
            intny[1] = fp
            fp = np.memmap(
                spfilebin,
                dtype="float32",
                mode="r",
                shape=(nx, ny, nwvl),
                offset=3 * 4 + nwvl * 4,
            )
            intny[0] = fp

            self.wvl = intny[1]
            self.get_lambda(spline, load_iontab=False)

            return intny

        def save_intny(
            self,
            spline,
            snap=None,
            nlamb=141,
            axis=2,
            rend_opacity=False,
            dopp_width_range=5e1,
            azimuth=None,
            dir="./",
            altitude=None,
            ooe=False,
            stepsize=0.01,
            gridsplit=1024,
            *args,
            **kwargs,
        ):
            """
            Calculate and save profiles in a binary file.
            """
            nlines = np.size(spline)
            if snap is None:
                snap = self.snap

            nt = np.size(snap)
            t0 = time.time()
            if axis == 0:
                axis_txt = "_yz_"
            elif axis == 1:
                axis_txt = "_xz_"
            else:
                axis_txt = "_xy_"

            for it in range(0, nt):
                self.set_snap(snap[it])
                if hasattr(self, "intcudamod"):
                    delattr(self, "intcudamod")
                time.time()
                if nlines == 1:
                    intny = self.get_intny(
                        spline,
                        nlamb=nlamb,
                        axis=axis,
                        rend_opacity=rend_opacity,
                        dopp_width_range=dopp_width_range,
                        azimuth=azimuth,
                        altitude=altitude,
                        ooe=ooe,
                        stepsize=stepsize,
                        gridsplit=gridsplit,
                    )

                    self.wvl = intny[1]
                    self.get_lambda(spline)

                    # make filename
                    savedFile = os.path.join(
                        dir, spline + axis_txt + "it{:03d}.bin".format(self.snap)
                    )
                    # write to file
                    nx = np.shape(intny[0])[0]
                    ny = np.shape(intny[0])[1]
                    nwvl = np.shape(intny[0])[2]
                    fout = open(savedFile, "wb")
                    fout.write(struct.pack("iii", nx, ny, nwvl))
                    fout.write(intny[1])
                    fout.write(intny[0])
                    fout.close()
                    if self.verbose:
                        print(
                            "done ",
                            spline,
                            " it=",
                            self.snap,
                            ",time used:",
                            time.time() - t0,
                        )
                else:
                    for iline in range(0, nlines):
                        time.time()
                        intny = self.get_intny(
                            spline[iline],
                            nlamb=nlamb,
                            axis=axis,
                            rend_opacity=rend_opacity,
                            dopp_width_range=dopp_width_range,
                            azimuth=azimuth,
                            altitude=altitude,
                            ooe=ooe,
                            stepsize=stepsize,
                            gridsplit=gridsplit,
                        )

                        # make filename
                        savedFile = (
                            spline[iline] + axis_txt + "it" + str(self.snap) + ".bin"
                        )
                        # write to file
                        nx = np.shape(intny[0])[0]
                        ny = np.shape(intny[0])[1]
                        nwvl = np.shape(intny[0])[2]
                        fout = open(savedFile, "wb")
                        fout.write(struct.pack("iii", nx, ny, nwvl))
                        fout.write(intny[1])
                        fout.write(intny[0])
                        fout.close()
                        if self.verbose:
                            print(
                                "done ",
                                spline[iline],
                                " it=",
                                self.snap,
                                ",time used:",
                                time.time() - t0,
                            )

        def get_int(
            self,
            spline,
            axis=2,
            rend_opacity=False,
            azimuth=None,
            altitude=None,
            ooe=False,
            stepsize=0.001,
            gridsplit=350,
            do_mit=None,
            abnd_arr=None,
            zcutoff=0,
            *args,
            **kwargs,
        ):
            """
            Calculates intensity from the simulation quantiables.

            Parameters
            ----------
            spline - string
                Name of the spectral line to calculate. In order to know the f
                ormat, $BIFROST/PYTHON/br_int/br_ioni/data
                contains files with the G(T,ne), usually name.opy. spline must
                be name, e.g., 'fe_8_108.073'.
            axis - integer number: 0 = x, 1 = y, 2 = z
                allows to chose the LOS integration axis (see azimuth and altitude)
            rend_opacity - allows to calculate opacities.
                Right now uses a very obsolete table; so I suggest to wait until
                further improvements.
            azimuth -  This allows to trace rays for any angle. In this cases
                uses none sa modules.
                In this case, axis parameter is not need to be used.
            altitude -  This allows to trace rays for any angle. In this cases
                uses none sa modules.
                In this case, axis parameter is not need to be used.
            ooe -  True or False. Default is False.
                If true, uses Olluri out of equilibrium ionization output
            Returns
            -------
            array - ndarray
                Array with the dimensions of the 2D spatial from the simulation
                and spectra.

            Notes
            -----
                uses cuda
            """

            if found:
                if os.environ.get("CUDA_LIB", "null") == "null":
                    os.environ["CUDA_LIB"] = os.environ["BIFROST"] + "CUDA/"

            # Calculation settings
            if azimuth is not None or altitude is not None:  # For any angle
                if ooe:
                    choice = "tdi"
                else:
                    choice = "static"
                if azimuth is not None:
                    azimuth = 90.0
                if altitude is not None:
                    altitude = 0.0
            else:  # For a specific axis, this is setup with axis =[0,1,2]
                if ooe:
                    choice = "satdi"
                else:
                    choice = "sastatic"
                azimuth = 90.0
                altitude = 0.0

            opts = int_options()
            opts.infile = self.file_root
            opts.snap = self.snap
            opts.choice = choice
            opts.simdir = self.fdir
            data_dir = self.fdir  # (opts.simdir if opts.simdir else askdirectory(
            # title='Simulation Directory')).rstrip('/')

            acont_filenames = [
                os.path.relpath(i, os.path.dirname(""))
                for i in glob(os.path.join(goft_path, spline + ".opy"))
            ]
            channel = 0
            if len(acont_filenames) == 0:
                raise ValueError("(EEE) get_int: GOFT file does not exist", spline)

            snap_range = (self.snap, self.snap)
            # + '_' + '%%0%ii' % np.max((len(str(self.snap)),3))
            template = opts.infile

            # from br_ioni import RenderGUI

            # OOE along any angle
            if opts.rendtype == "tdi":
                from ..pyculib.intcu.uvrt import TDIEmRenderer

                tdi_paramfile_abs = (
                    opts.tdiparam
                    if opts.tdiparam
                    else askopenfilename(title="OOE Ionization Paramfile")
                )
                tdi_paramfile = os.path.relpath(tdi_paramfile_abs, data_dir)
                s = TDIEmRenderer(
                    data_dir=data_dir, paramfile=tdi_paramfile, snap=opts.snap
                )

            # Statistical Equibilibrium along specific axis: x, y or z
            elif opts.rendtype == "sastatic":
                from ..pyculib.intcu.uvrt import SAStaticEmRenderer

                s = SAStaticEmRenderer(
                    snap_range,
                    template,
                    data_dir=data_dir,
                    snap=opts.snap,
                    do_mit=do_mit,
                    zcutoff=zcutoff,
                    _DataClass=BifrostData,
                )

            # OOE along specific axis, i.e., x, y or z
            elif opts.rendtype == "satdi":
                from ..pyculib.intcu.uvrt import SATDIEmRenderer

                tdi_paramfile_abs = (
                    opts.tdiparam
                    if (opts.tdiparam)
                    else askopenfilename(title="OOE Ionization Paramfile")
                )
                tdi_paramfile = os.path.relpath(tdi_paramfile_abs, data_dir)

                s = SATDIEmRenderer(
                    data_dir=data_dir,
                    paramfile=tdi_paramfile,
                    snap=opts.snap,
                    zcutoff=zcutoff,
                )

            # Statistical Equibilibrium for any angle
            else:
                from ..pyculib.intcu.uvrt import StaticEmRenderer

                s = StaticEmRenderer(
                    snap_range, template, data_dir=data_dir, snap=opts.snap
                )

            rend_reverse = False

            s.save_accontfiles(acont_filenames)
            return s.i_render(
                channel,
                azimuth,
                altitude,
                axis,
                reverse=rend_reverse,
                gridsplit=gridsplit,
                tau=None,
                opacity=rend_opacity,
                do_mit=do_mit,
                abnd_arr=abnd_arr,
                verbose=self.verbose,
                stepsize=stepsize,
            )

        def ratom(self, paramfile):
            egis = []  # array of (ev, g, ion)
            trns = []

            with open(paramfile) as tdiparamf:
                data = [line for line in tdiparamf.readlines() if line[0] != "*"]

            # parse the tdiparamfile
            self.element_name = data.pop(0).strip()
            self.ab, self.awgt = (float(i) for i in data.pop(0).split())
            nk, nlines, _, _ = (int(i) for i in data.pop(0).split())

            for _ in range(0, nk):
                datstring = shlex.split(data.pop(0))
                ev = float(datstring[0]) * CC * HH / EE
                g = float(datstring[1])
                label = datstring[2]
                ion = int(datstring[3])
                egis.append(Egi(ev, g, label, ion))

            for _ in range(0, nlines):
                j, i, f, _, _, _, _, _, _, _ = (float(i) for i in data.pop(0).split())
                j = int(j)
                i = int(i)

                dn, up = i, j
                if egis[j - 1].ev < egis[i - 1].ev:
                    dn, up = j, i
                irad = dn - 1
                jrad = up - 1

                alamb = HCE / (egis[jrad].ev - egis[irad].ev)
                a_ul = f * 6.6702e15 * egis[irad].g / (egis[jrad].g * alamb**2)

                trns.append(Trn(irad, jrad, alamb, a_ul, f))
            return trns

        def get_ion(self, spline):
            acont_filename = os.path.relpath(os.path.join(goft_path, spline + ".opy"))

            opfile = open(acont_filename, "rb")
            return pickle.load(opfile)

        def acont_opy_filepath(self, spline):
            """
            Finds file path and name(s) for a synthetic file(s) and verify that
            exists.
            """
            acont_filename = [
                os.path.relpath(i, os.path.dirname(""))
                for i in glob(os.path.join(goft_path, spline + ".opy"))
            ]

            if len(acont_filename) == 0:
                raise ValueError(
                    "(EEE) get_intny: GOFT file does not exist",
                    os.path.join(goft_path, spline + ".opy"),
                )

            return acont_filename

        def calc_synvdemint(self):
            """
            Calculate profile intensity for each velocity bin.
            """

            if not hasattr(self, "vdem"):
                get_vdem()
            self.synvdemint = {}
            for ilines in self.synlinfiles.keys():
                thisg = np.interp(
                    self.lgtaxis,
                    np.log10(self.sglin[ilines]["TEMP"]),
                    self.sglin[ilines]["G"],
                )
                self.synvdemint[ilines] = {}
                self.synvdemint[ilines]["intvtg"] = (
                    self.vdem.reshape(self.nlnt, self.ndop * self.nx * self.ny)
                    * thisg.reshape(self.nlnt, 1)
                ).reshape(self.nlnt, self.ndop, self.nx, self.ny)
                self.synvdemint[ilines]["intens"] = np.sum(
                    np.sum(self.synvdemint[ilines]["intvtg"], axis=0), axis=0
                )

        def calc_profmoments(self, intny):
            """
            Calculates 0, 1st and 2nd moment of intensity profiles.
            """
            dny = intny[1]
            np.size(dny)
            np.shape(intny[0])[0]
            np.shape(intny[0])[1]

            prof_mom = {}

            prof_mom["itot"] = np.trapz(intny[0], dny, axis=2)
            prof_mom["utot"] = np.trapz(
                intny[0] * dny[None, None, :], dny, axis=2
            ) / np.clip(prof_mom["itot"], 1.0e-20, np.max(prof_mom["itot"]))
            prof_mom["wtot"] = 2.0 * (
                np.trapz(intny[0] * dny[None, None, :] ** 2, dny, axis=2)
                / np.clip(prof_mom["itot"], 1.0e-20, np.max(prof_mom["itot"]))
                - prof_mom["utot"] ** 2
            )
            # prof_mom['wtot']=np.sqrt(np.trapz(intny[0]*(prof_mom['utot']-dny[None,None,:])**2,dny,axis=2)/np.clip(prof_mom['itot'],1.e-20,np.max(prof_mom['itot'])))

            prof_mom["utot"] *= self.ny2vel
            prof_mom["wtot"] *= self.ny2vel * self.ny2vel

            # for iy in range(0,ny_muse_res):
            # for ix in range(0,noffset):
            #  secondmom[iwvl,ii*noffset+ix,iy]=dfunc(tsqrt(tsum(
            #        cfunc(syn_troch[ix,:,iy]*
            #      (dopp[ii,:]-firstmom[iwvl,ii*noffset+ix,iy])**2))/cfunc(zeromom[iwvl,ii*noffset+ix,iy])))

            return prof_mom

        def get_lambda(self, spline, load_iontab=True):
            """
            Calculates wavelength in AA and km/s.
            """
            CC = units.clight * units.cm_to_m  # 2.99792458e8 m/s
            CC * 1e10
            if load_iontab:
                iontab = self.get_ion(spline)
                self.wvl0 = iontab.Gofnt["wvl"]
            else:
                self.wvl0 = float(spline.split("_")[2])
            self.lambd = (
                self.wvl0
                / (self.wvl * self.wvl0 / 1.0e13 + units.clight / 1.0e5)
                * units.clight
                / 1.0e5
            )
            self.ny2vel = self.wvl0 * 1.0e-13
            self.wvldop = self.wvl * self.ny2vel

        def calc_vdemmoments(self):
            """
            Calculate moments for each lines in sglin using the true VDEM
            raster.
            """

            if not hasattr(self, "synvdemint"):
                self.calc_synvdemint()

            for ilines in self.synlinfiles.keys():
                trthisprofile = np.transpose(
                    self.synvdemint[ilines]["intvtg"], (1, 0, 2, 3)
                ).reshape(self.ndop, self.nlnt * self.nx * self.ny)

                self.synprofras[ilines]["intens"] = np.sum(
                    np.sum(self.synvdemint[ilines]["intvtg"], axis=0), axis=0
                )  # ph/s/pix

                self.synprofras[ilines]["firstmom"] = (
                    np.sum(
                        np.sum(
                            np.reshape(
                                trthisprofile.reshape(
                                    self.ndop, self.nlnt * self.nx * self.ny
                                )
                                * self.dopaxis.reshape(self.ndop, 1),
                                (self.ndop, self.nlnt, self.nx, self.ny),
                            ),
                            axis=0,
                        ),
                        axis=0,
                    )
                    / self.synprofsolras[ilines]["intens"]
                ).reshape(self.nslits * np.size(self.ioffset), self.ny)

                self.synprofras[ilines]["secondmom"] = np.sqrt(
                    np.sum(
                        np.sum(
                            np.reshape(
                                trthisprofile.reshape(
                                    self.ndop, self.nlnt * self.nx * self.ny
                                )
                                * self.dopaxis.reshape(self.ndop, 1) ** 2,
                                (self.ndop, self.nlnt, self.nx, self.ny),
                            ),
                            axis=0,
                        ),
                        axis=0,
                    )
                    / self.synprofsolras[ilines]["intens"]
                ).reshape(self.nslits * np.size(self.ioffset), self.ny)

                thirdmom = (
                    np.sum(
                        np.sum(
                            np.reshape(
                                trthisprofile.reshape(
                                    self.ndop, self.nlnt * self.nx * self.ny
                                )
                                * self.dopaxis.reshape(self.ndop, 1) ** 3,
                                (self.ndop, self.nlnt, self.nx, self.ny),
                            ),
                            axis=0,
                        ),
                        axis=0,
                    )
                    / self.synprofsolras[ilines]["intens"]
                ).reshape(self.nslits * np.size(self.ioffset), self.ny)

                sign = np.zeros((self.nx, self.ny))
                sign[:, :] = 1
                sign[np.where(thirdmom < 0)] = -1.0

                self.synprofras[ilines]["thirdmom"] = (np.abs(thirdmom)) ** (
                    1.0 / 3.0
                ) * sign

        def load_vdem_npz(self, vdemfile):
            """
            Reads the ground true VDEM.

            The npz file can be
                generated using bifrost.py for Bifrost simulations.
                It needs to be called prior doing the inversions:
                VDEMSInversionCode, or all_comb
            VDEM function shape (T, v, nx, ny).
            """

            vdem_str = np.load(vdemfile)
            self.vdem = vdem_str["vdem"]
            self.dopaxis = vdem_str["dopaxis"]
            self.lgtaxis = vdem_str["lgtaxis"]
            self.posaxis = vdem_str["posaxis"]

            self.nlnt = np.size(self.lgtaxis)
            self.ndop = np.size(self.dopaxis)

            return {
                "lgtaxis": self.lgtaxis,
                "dopaxis": self.dopaxis,
                "posaxis": np.array(("lgtaxis", "dopaxis")),
                "vdem": self.vdem,
            }

        def vdem_cuda(
            self,
            axis=2,
            dopaxis=np.linspace(-100, 100, 21),
            lgtaxis=np.linspace(4, 9, 25),
            zcut=None,
            stepsize=0.001,
            var=None,
        ):
            """
            Calculates DEM as a function of temperature and velocity, i.e.,
            VDEM.

            Parameters
            ----------
            axis - integer  x = 0, y = 1, z = 2
            dopaxis - float vector  velocity axis in km/s
            lgtaxis - float vector  temperature axis in log10/K scale
            zcut - float    used to cut the convection zone
            Returns
            -------
            array - ndarray
                Array with the dimensions of the 2D spatial from the simulation
                and temperature and velocity bins.
            """
            if found:
                gridsplit = 128

                if os.environ.get("CUDA_LIB", "null") == "null":
                    os.environ["CUDA_LIB"] = os.environ["BIFROST"] + "CUDA/"

                opts = int_options()
                opts.infile = self.file_root
                opts.snap = self.snap
                opts.simdir = self.fdir
                data_dir = (
                    opts.simdir
                    if opts.simdir
                    else askdirectory(title="Simulation Directory")
                ).rstrip("/")

                snap_range = (self.snap, self.snap)
                # + '_' + '%%0%ii' % np.max((len(str(self.snap)),3))

                nvel = len(dopaxis)
                ntg = len(lgtaxis)
                if axis == 0:
                    nx = self.ny
                    ny = self.nz
                elif axis == 1:
                    nx = self.nx
                    ny = self.nz
                else:
                    nx = self.nx
                    ny = self.ny

                delta_tg = lgtaxis[1] - lgtaxis[0]
                delta_vel = dopaxis[1] - dopaxis[0]

                from ..pyculib.demcu.dem import DEMRenderer

                self.intcudamod = DEMRenderer(
                    snap_range,
                    opts.infile,
                    axis=axis,
                    data_dir=data_dir,
                    snap=opts.snap,
                    zcut=zcut,
                    var=var,
                    _DataClass=BifrostData,
                )
                vdem = np.zeros((ntg, nvel, nx, ny))
                for itg in range(0, ntg):
                    for ivel in range(0, nvel):
                        self.intcudamod.load_minmax(
                            lgtaxis[itg] - delta_tg / 2.0,
                            lgtaxis[itg] + delta_tg / 2.0,
                            (dopaxis[ivel] - delta_vel / 2.0),
                            (dopaxis[ivel] + delta_vel / 2.0),
                        )
                        vdem[itg, ivel, :, :] = self.intcudamod.dem_render(
                            axis,
                            gridsplit=gridsplit,
                            verbose=self.verbose,
                            stepsize=stepsize,
                        )
                        print(
                            "%.1f %% done"
                            % (100.0 * (itg * nvel + ivel) / (1.0 * ntg * nvel)),
                            end="\r",
                            flush=True,
                        )
                self.vdem = vdem
                self.lgtaxis = lgtaxis
                self.dopaxis = dopaxis
                self.nlnt = np.size(lgtaxis)
                self.ndop = np.size(dopaxis)
                self.posaxis = np.array(("lgtaxis", "dopaxis"))
                vdem_str = {
                    "lgtaxis": lgtaxis,
                    "dopaxis": dopaxis,
                    "posaxis": np.array(("lgtaxis", "dopaxis")),
                    "vdem": vdem,
                }

                return vdem_str

            else:
                print(
                    "I am so sorry... but you need pycuda:\n"
                    + "1, install latest CUDA at \n"
                    + "https://developer.nvidia.com/cuda-downloads\n "
                    + "2, pycuda: https://wiki.tiker.net/PyCuda/Installation\n"
                    + "no warranty that this will work on non-NVIDIA"
                )

        def from_hel2vdem(
            self,
            snap,
            dopaxis,
            lgtaxis,
            logdensaxis=None,
            tg=[],
            vel=[],
            dens=[],
            ems=[],
            axis=2,
            interp_vdem=False,
            radyn=False,
            highrun=False,
            xyrotation=None,
            opacity=None,
            trac=False,
            trac_coeff=1,
            novelcut=False,
            **kwargs,
        ):
            """
            Computes vdem directly.

            Parameters
            snap - integer  snapshot number
            dopaxis - float vector  velocity axis in km/s
            lgtaxis - float vector  temperature axis in log10/K scale
            axis - integer  x = 0, y = 1, z = 2: LOS integration

            Returns
            -------
            array - ndarray (nx,ny,ntg,nvel)
            Array with the dimensions of the 2D spatial from the simulation
            and temperature and velocity bins.
            """

            vdem_ds = xr.Dataset()

            if np.size(snap) > 1:
                for isnap in snap:
                    vdem_temp, x, y = self.hel2vdem(
                        isnap,
                        dopaxis,
                        lgtaxis,
                        logdensaxis=logdensaxis,
                        axis=axis,
                        interp_vdem=interp_vdem,
                        radyn=radyn,
                        highrun=highrun,
                        xyrotation=xyrotation,
                        opacity=opacity,
                        trac=trac,
                        trac_coeff=trac_coeff,
                        novelcut=novelcut,
                        **kwargs,
                    )

                    if isnap == snap[0]:
                        vdem = np.reshape(vdem_temp, np.append(1, np.shape(vdem_temp)))
                        time = self.time
                    else:
                        vdem = np.append(
                            vdem,
                            np.reshape(vdem_temp, np.append(1, np.shape(vdem_temp))),
                            axis=0,
                        )
                        time = np.append(time, self.time)
                if logdensaxis is None:
                    coords = dict(logT=lgtaxis, vdop=dopaxis, x=x, y=y, time=time)
                    dims = ["time", "logT", "vdop", "x", "y"]
                else:
                    coords = dict(
                        logT=lgtaxis,
                        vdop=dopaxis,
                        logD=logdensaxis,
                        x=x,
                        y=y,
                        time=time,
                    )
                    dims = ["time", "logT", "vdop", "logD", "x", "y"]
                vdem_ds["vdem"] = xr.DataArray(
                    vdem,
                    dims=dims,
                    coords=coords,
                    attrs=dict(
                        description="DEM(T,vel,x,y)",
                        units="1e27 / cm5",
                    ),
                )
                vdem_ds.time.attrs["units"] = "s"
            else:
                vdem, x, y = self.hel2vdem(
                    snap,
                    dopaxis,
                    lgtaxis,
                    logdensaxis=logdensaxis,
                    tg=tg,
                    vel=vel,
                    dens=dens,
                    ems=ems,
                    axis=axis,
                    interp_vdem=interp_vdem,
                    radyn=radyn,
                    highrun=highrun,
                    xyrotation=xyrotation,
                    opacity=opacity,
                    trac=trac,
                    trac_coeff=trac_coeff,
                    novelcut=novelcut,
                    **kwargs,
                )

                if logdensaxis is None:
                    coords = dict(logT=lgtaxis, vdop=dopaxis, x=x, y=y)
                    dims = ["logT", "vdop", "x", "y"]
                else:
                    coords = dict(
                        logT=lgtaxis, vdop=dopaxis, logD=logdensaxis, x=x, y=y
                    )
                    dims = ["logT", "vdop", "logD", "x", "y"]
                vdem_ds["vdem"] = xr.DataArray(
                    vdem,
                    dims=dims,
                    coords=coords,
                    attrs=dict(
                        description="DEM(T,vel,x,y)", units="1e27 / cm5", time=snap
                    ),
                )
            self.vdem = vdem[:, ::-1, ...]  # swap from vel to vdop
            self.lgtaxis = lgtaxis
            self.dopaxis = -dopaxis[::-1]
            self.nlnt = np.size(self.lgtaxis)
            self.ndop = np.size(self.dopaxis)
            self.posaxis = np.array(("lgtaxis", "dopaxis"))
            if logdensaxis is not None:
                self.logD = logdensaxis
                self.ndens = np.size(self.logdensaxis)
                vdem_ds.logD.attrs["long_name"] = r"eDensity"
                vdem_ds.logD.attrs["units"] = r"cm^3"
            vdem_ds.x.attrs["long_name"] = "X"
            vdem_ds.y.attrs["long_name"] = "Y"
            vdem_ds.logT.attrs["long_name"] = r"log$_{10}$(T)"
            vdem_ds.vdop.attrs["long_name"] = r"v$_{Doppler}$"
            vdem_ds.x.attrs["units"] = "cm"
            vdem_ds.y.attrs["units"] = "cm"
            vdem_ds.logT.attrs["units"] = r"log$_{10}$ (K)"
            vdem_ds.vdop.attrs["units"] = "km/s"

            add_history(vdem_ds, locals(), self.from_hel2vdem)

            return vdem_ds

        def hel2vdem(
            self,
            snap,
            dopaxis,
            lgtaxis,
            logdensaxis=None,
            tg=[],
            vel=[],
            dens=[],
            ems=[],
            axis=2,
            interp_vdem=False,
            radyn=False,
            highrun=False,
            xyrotation=None,
            opacity=None,
            trac=False,
            trac_coeff=1,
            novelcut=False,
            **kwargs,
        ):
            if radyn:
                return get_radyn_vdem(
                    self, snap, axis=axis, dopaxis=dopaxis, lgtaxis=lgtaxis, **kwargs
                )

            if len(tg) == 0:
                tg = self.trans2comm("tg", snap)
            izcut = np.argmin(np.abs(self.z))
            if axis == 0:
                x = self.y
                y = self.z[izcut:]
                if np.size(self.x) == 1:
                    ds = np.array([self.dx])
                else:
                    ds = self.dx1d
            elif axis == 1:
                y = self.z[izcut:]
                x = self.x
                if np.size(self.y) == 1:
                    ds = np.array([self.dy])
                else:
                    ds = self.dy1d
            elif axis == 2:
                x = self.x
                y = self.y
                ds = self.dz1d[izcut:]
            if interp_vdem:
                vdem = (
                    self.vdem_cuda(
                        axis=axis,
                        dopaxis=dopaxis,
                        lgtaxis=lgtaxis,
                        zcut=[self.z[izcut], self.z[-1]],
                        stepsize=0.001,
                    )
                )["vdem"]

                if xyrotation:
                    vdem = np.transpose(vdem, (0, 1, 3, 2))
                    x_temp = x.copy()
                    x = y.copy()
                    y = x_temp.copy()
            else:
                # trans2comm allows will provide the array in a common format through out the code.
                self.hion = False
                if len(ems) == 0:
                    ems = self.trans2comm(
                        "emiss", snap
                    )  # Non-integrated emission measure (1/cm^6) = n_e * n_H
                if len(vel) == 0:
                    str_axis = ["x", "y", "z"]
                    vel = self.trans2comm("u" + str_axis[axis], snap)  # velocity cm/s

                tg_temp = tg[:, :, izcut:]
                vel_temp = (
                    vel[:, :, izcut:] / 1e5
                )  # below computes the VDEM. Velocity from cm/s -> km/s
                ems_temp = ems[:, :, izcut:]

                if logdensaxis is not None:
                    dens = self.trans2comm("nel", snap)
                    dens_temp = dens[:, :, izcut:]
                else:
                    dens_temp = None

                if opacity is not None:
                    rho = (self.trans2comm("rho", snap))[:, :, izcut:]
                    CC = units.clight * units.cm_to_m  # 2.99792458e8 m/s
                    CC * 1e10
                    GRPH = 2.38049e-24
                    opatab = Opatab(fdir=goft_path)
                    ka_table = opatab.h_he_absorb(opacity)
                    np.size(ka_table)
                    logtemp = opatab.temp
                    absorption = np.interp(tg_temp, 10**logtemp, ka_table)
                    tau = np.zeros_like(tg_temp)
                    ns = np.size(ds)
                    for iiz, iz in enumerate(ds[::-1]):
                        if iiz > 0:
                            if axis == 0:
                                tau[ns - iiz - 1, ...] = (
                                    absorption[ns - iiz - 1, ...]
                                    * iz
                                    * rho[ns - iiz - 1, ...]
                                    / GRPH
                                    + tau[ns - iiz, ...]
                                )
                            elif axis == 1:
                                tau[:, ns - iiz - 1, :] = (
                                    absorption[:, ns - iiz - 1, :]
                                    * iz
                                    * rho[:, ns - iiz - 1, :]
                                    / GRPH
                                    + tau[:, ns - iiz, :]
                                )
                            elif axis == 2:
                                tau[..., ns - iiz - 1] = (
                                    absorption[..., ns - iiz - 1]
                                    * iz
                                    * rho[..., ns - iiz - 1]
                                    / GRPH
                                    + tau[..., ns - iiz]
                                )
                    ems_temp *= np.exp(-tau)

                if trac:
                    bx = self.trans2comm("bx", snap)[:, :, izcut:]
                    by = self.trans2comm("by", snap)[:, :, izcut:]
                    bz = self.trans2comm("bz", snap)[:, :, izcut:]
                    dx = self.dx
                    dy = self.dy
                    dz = self.dz
                    tg_dep, qloss_dep = self.read_qloss()
                    Lt = np.interp(tg_temp, tg_dep, qloss_dep)
                    Q_Chianti = ems_temp * Lt * 1e27
                    modb = np.sqrt(bx**2 + by**2 + bz**2)
                    kappa_spitzer = 1e-6 * tg_temp**2.5
                    L_R = (
                        trac_coeff
                        * (np.abs(bx) * dx + np.abs(by) * dy + np.abs(bz) * dz)
                        / modb
                    )
                    fmin = np.sqrt(kappa_spitzer * tg_temp / Q_Chianti) / (2.0 * L_R)
                    fmin[fmin > 1] = 1.0
                    ems_temp *= fmin

                if xyrotation:
                    tg_temp = np.transpose(tg_temp, (1, 0, 2))
                    vel_temp = np.transpose(vel_temp, (1, 0, 2))
                    ems_temp = np.transpose(ems_temp, (1, 0, 2))

                    if logdensaxis is not None:
                        dens_temp = np.transpose(dens_temp, (1, 0, 2))

                    x_temp = x.copy()
                    x = y.copy()
                    y = x_temp.copy()

                if highrun:
                    vdem = get_vdem_highrun(
                        tg_temp,
                        vel_temp,
                        ems_temp,
                        ds,
                        dens=dens_temp,
                        logdensaxis=logdensaxis,
                        dopaxis=dopaxis,
                        lgtaxis=lgtaxis,
                        axis=axis,
                        novelcut=novelcut,
                    )
                else:
                    vdem = get_vdem(
                        tg_temp,
                        vel_temp,
                        ems_temp,
                        ds,
                        dens=dens_temp,
                        logdensaxis=logdensaxis,
                        dopaxis=dopaxis,
                        lgtaxis=lgtaxis,
                        axis=axis,
                        novelcut=novelcut,
                    )
            return vdem, x, y

        def read_qloss(file="Radloss_Chianti.dat"):
            f = open("Radloss_Chianti.dat", "r")
            first_line = f.readline()
            ntg = int(first_line.strip())
            tg = np.zeros(ntg)
            qloss = np.zeros(ntg)

            for ii, line in enumerate(f):
                tg[ii], qloss[ii] = np.float64(line.strip().split())

            return tg, qloss

        def get_vdem_moment(self, axis_name, mom=0):
            """
            Calculate moments along Doppler velocity.
            """

            axis = int(np.where(self.posaxis == axis_name)[0])
            v_axis = self.dopaxis**mom

            if not hasattr(v_axis, "device"):
                v_axis = torch.tensor(v_axis)

            if mom > 0:
                norm = 1.0 / (self.vdem.sum(axis=axis) + 1e-30)

            if hasattr(self.vdem, "device"):
                if self.vdem.device.type == "cuda":
                    v_axis = v_axis.cuda()
                cube = torch.einsum("tvij,v->tij", self.vdem.float(), v_axis.float())
                if mom > 0:
                    cube = torch.einsum("tij,tij->tij", cube, norm) ** (1.0 / mom)
                if cube.device.type == "cuda":
                    cube = cube.detach().cpu().numpy()
            else:
                cube = np.einsum("tvij,v->tij", self.vdem, v_axis)
                if mom > 0:
                    cube = np.einsum("tij,tij->tij", cube, norm) ** (1.0 / mom)

            return cube

        def get_em3d(self, axis=2, zcut=None, unitsnorm=1e27, *args, **kwargs):
            """
            Calculates emissivity (EM).

            Parameters
            ----------
            axis - integer  x = 0, y = 1, z = 2
            zcut - float    used to cut the convection zone
            Returns
            -------
            array - ndarray
                Array with the dimensions of the 3D spatial from the simulation
                of the emission measure c.g.s units.
            """
            # Important, the object should be initialized with cgs units.
            ems = self.trans2comm("emiss")

            if axis == 0:
                ds = self.dx1d
                oper = "ijk,i->ijk"
            elif axis == 1:
                ds = self.dy1d
                oper = "ijk,j->ijk"
            else:
                ds = self.dz1d
                oper = "ijk,k->ijk"

            ems = np.einsum(oper, ems, ds)

            if zcut is not None:
                for iz in range(0, self.nz):
                    if self.z[iz] < zcut:
                        izcut = iz
                ems[:, :, :izcut] = 0.0
            return ems

        def get_emiss(self, spline, axis=2, order=1, zcut=None, *args, **kwargs):
            """
            Calculates emissivity (EM[x,y,z]).

            Parameters
            ----------
            spline - string
                Name of the spectral line to calculate. In order to know the
                format, $BIFROST/PYTHON/br_int/br_ioni/data
                contains files with the G(T,ne), usually name.opy. spline must
                be name, e.g., 'fe_8_108.073'.
            Returns
            -------
            array - ndarray
                Array with the dimensions of the 3D spatial from the simulation
                SI units.
            Notes
            -----
                Uses cuda
            """

            g = self.get_Gofnt(spline, order=order, *args, **kwargs)

            em3d = self.get_em3d(axis=axis, zcut=zcut, *args, **kwargs)

            return em3d * g

        def get_Gofnt(self, spline, order=1, *args, **kwargs):
            """
            Calculates G(T,ne)[x,y,z] at every location.

            Parameters
            ----------
            spline - string
                Name of the spectral line to calculate. In order to know the
                format, $BIFROST/PYTHON/br_int/br_ioni/data
                contains files with the G(T,ne), usually name.opy. spline must
                be name, e.g., 'fe_8_108.073'.
            Returns
            -------
            array - ndarray
                Array containing normalized G(T,ne) with the dimensions of the
                3D spatial from the simulation.
            Notes
            -------
            Currently works for single line only, look to get_emiss method for how
            to code if more than one line desired.
            """

            cc = units.clight * units.cm_to_m  # 2.99792458e8 m/s
            CCA = cc * 1e10  # AA/s
            nspline = np.size(spline)

            if nspline > 1:
                print("(WWW) get_Gofnt: Only single line G(T,ne) currently supported.")
            else:
                acont_filename = self.acont_opy_filepath(spline)

            filehandler = open(acont_filename[0], "rb")
            ion = pickle.load(filehandler)
            filehandler.close()

            ntgbin = len(ion.Gofnt["temperature"])
            nedbin = len(ion.Gofnt["eDensity"])
            tgmin = np.min(np.log10(ion.Gofnt["temperature"]))
            tgrange = np.max(np.log10(ion.Gofnt["temperature"])) - tgmin
            enmin = np.min(ion.Gofnt["eDensity"])
            enrange = np.max(ion.Gofnt["eDensity"]) - enmin

            lgtaxis = np.linspace(tgmin, tgmin + tgrange, ntgbin)
            en_axis = np.linspace(enmin, enmin + enrange, nedbin)
            self.ny0 = CCA / ion.Gofnt["wvl"]
            self.awgt = ion.mass

            self.acont_table = np.array(np.transpose(ion.Gofnt["gofnt"]))

            if self.verbose:
                print(
                    (
                        "(WWW) get_Gofnt: Using ...%s G(T,ne) table"
                        % acont_filename[0][-19:-5]
                    )
                )

            tg = self.trans2comm("tg")
            en = self.trans2comm("ne")

            # warnings for values outside of table
            enminv = np.min(en)
            enmaxv = np.max(en)
            tgminv = np.min(np.log10(tg))
            tgmaxv = np.max(np.log10(tg))
            if enminv < enmin:
                print(
                    "(WWW) tab_interp: electron density outside table bounds."
                    + "Table ne min=%.3e, requested ne min=%.3e" % (enmin, enminv)
                )
            if enmaxv > enmin + enrange:
                print(
                    "(WWW) tab_interp: electron density outside table bounds. "
                    + "Table ne max=%.1f, requested ne max=%.1f"
                    % (enmin + enrange, enmaxv)
                )
            if tgminv < tgmin:
                print(
                    "(WWW) tab_interp: tg outside of table bounds. "
                    + "Table tg min=%.2f, requested tg min=%.2f" % (tgmin, tgminv)
                )
            if tgmaxv > tgmin + tgrange:
                print(
                    "(WWW) tab_interp: tg outside of table bounds. "
                    + "Table tg max=%.2f, requested tg max=%.2f"
                    % (tgmin + tgrange, tgmaxv)
                )

            # translate to table coordinates
            y = (np.log10(tg) - lgtaxis[0]) / (lgtaxis[1] - lgtaxis[0])
            x = (en - en_axis[0]) / (en_axis[1] - en_axis[0])
            g = ndimage.map_coordinates(
                self.acont_table, [x, y], order=order, mode="nearest"
            )

            return g

    return _UVOTRTData


def add_history(ds: xr.Dataset, local_vars: dict, func: Callable) -> None:
    """
    Adds a history entry to the dataset.

    Parameters
    ----------
    ds : `xarray.Dataset`
        Dataset to add the history entry to.
    local_vars : `dict`
        Dictionary of local variables.
    func : `Callable`
        Function to add to the history entry.
    """
    string_vals = []
    for arg in local_vars:
        if arg in inspect.signature(func).parameters:
            if isinstance(local_vars[arg], (xr.Dataset, xr.DataArray, np.ndarray)):
                if isinstance(local_vars[arg], np.ndarray) and local_vars[
                    arg
                ].shape in [(), (1,)]:
                    string_vals.append(f"{arg}={local_vars[arg].tolist()}")
                elif (
                    isinstance(local_vars[arg], xr.DataArray)
                    and local_vars[arg].size == 1
                ):
                    string_vals.append(f"{arg}={local_vars[arg].values.tolist()}")
                else:
                    string_vals.append(f"{arg}={arg}")
            else:
                string_vals.append(f"{arg}={local_vars[arg]}")
    ds.attrs["HISTORY"] = [
        ", ".join(
            [*ds.attrs.get("HISTORY", []), f"{func.__name__}({', '.join(string_vals)})"]
        )
    ]
    today = datetime.datetime.now(tz=datetime.timezone.utc)
    if "date created" in ds.attrs:
        ds.attrs["date modified"] = today.strftime("%d-%b-%Y")
    else:
        ds.attrs["date created"] = today.strftime("%d-%b-%Y")


# Calculation settings
class int_options:
    def __init__(self):
        self.rendtype = "sastatic"
        self.tdiparam = False
        self.simdir = ""
        self.snap = 1
        self.infile = ""
