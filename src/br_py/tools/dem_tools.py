import numpy as np
import importlib
import os
import torch
from math import ceil
from scipy.sparse import coo_matrix
import pickle
from .func_tools import refine

spam_spec = importlib.util.find_spec("pycuda")
found = spam_spec is not None

try:
    goft_path = os.environ.get("GOFT_PATH")
    print("GOFT_PATH try {}".format(goft_path))
except Exception:  # NOQA: BLE001
    goft_path = os.path.join(
        os.environ.get("LMSALHUB"), "/sims_hub/br_intcu/br_ioni/data/"
    )
    print("GOFT_PATH except {}".format(goft_path))


# Crucial the above uses a function similar to the following. It is crucial to use same function
# or at least save with same names and format in order to use the synthetic code.
def get_vdem(
    tg,
    vel,
    ems,
    ds,
    dens=None,
    logdensaxis=None,
    axis=2,
    dopaxis=np.linspace(-100, 100, 21),
    lgtaxis=np.linspace(4, 9, 25),
    novelcut=False,
):
    """
    Calculates DEM as a function of temperature and velocity, i.e., VDEM.

    Parameters
    ----------
    tg - 3D array of gas temperature
    vel- 3D array of velocity
    ems- 3D array of n_e*n_H
    ds - 1D array of cell length along "axis" dimension
    axis - integer  x = 0, y = 1, z = 2
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    Returns
    -------
    array - ndarray
    Array with the dimensions of the 2D spatial from the simulation
    and temperature and velocity bins.
    """
    assert tg.shape[axis] == len(ds)
    dims = ["i", "j", "k"]

    nvel = len(dopaxis)
    ntg = len(lgtaxis)

    # Functions for converting between numpy arrays and torch tensors
    if torch.cuda.is_available():

        def cfunc(x):
            return torch.tensor(x).cuda()

        def dfunc(x):
            return x.cpu().numpy()
    else:

        def cfunc(x):
            return torch.tensor(x)

        def dfunc(x):
            return x.numpy()

    # Convert to torch tensors
    ds = cfunc(ds.astype(np.float32))
    vel, tg, ems, dopaxis, lgtaxis = [
        cfunc(q) for q in [vel, tg, ems, dopaxis, lgtaxis]
    ]

    # The VDEM array will be of shape [ntg, nvel, dim0, dim1] (assume line-of-sight integration performed)
    vdem_shape = np.delete(vel.shape, axis).tolist()
    vdem_shape.insert(0, nvel)
    vdem_shape.insert(0, ntg)

    delta_tg = lgtaxis[1] - lgtaxis[0]
    delta_vel = dopaxis[1] - dopaxis[0]

    if logdensaxis is not None:
        ndens = len(logdensaxis)
        dens, logdensaxis = [cfunc(q) for q in [dens, logdensaxis]]
        logdensaxis[1] - logdensaxis[0]
        vdem_shape.insert(0, ndens)

    vdem = torch.zeros(size=vdem_shape)
    for itg in range(0, ntg):
        masktg = tg >= 10.0 ** (lgtaxis[itg] - delta_tg / 2.0)
        masktg *= tg < 10.0 ** (lgtaxis[itg] + delta_tg / 2.0)
        if itg % 2 == 0:
            print(
                "(get_vdem) working on lgT bin({}/{})".format(itg, ntg),
                end="\r",
                flush=True,
            )
        for ivel in range(0, nvel):
            if ivel == 0 and novelcut:
                maskvel = masktg
            else: 
                maskvel = masktg * (vel >= dopaxis[ivel] - delta_vel / 2.0)
            if not(ivel == nvel - 1 and novelcut):
                maskvel *= vel < dopaxis[ivel] + delta_vel / 2.0
            if logdensaxis is not None:
                for idens in range(0, ndens):
                    maskdens = maskvel * dens >= 10.0 ** (
                        logdensaxis[itg] - logdensaxis / 2.0
                    )
                    maskdens *= dens < 10.0 ** (logdensaxis[itg] - logdensaxis / 2.0)
                    temp = torch.einsum(
                        "ijkl,{0:s}->ijkl".format(dims[axis]), ems * maskdens, ds
                    )  # Ne*NH*ds along line-of-sight
                    vdem[itg, ivel, idens, ...] = (temp).sum(axis=axis)
            else:
                temp = torch.einsum(
                    "ijk,{0:s}->ijk".format(dims[axis]), ems * maskvel, ds
                )  # Ne*NH*ds along line-of-sight
                vdem[itg, ivel, ...] = (temp).sum(axis=axis)

    # Convert to numpy arrays
    tg, ems, lgtaxis, ds, vdem, vel, dopaxis = [
        dfunc(q) for q in [tg, ems, lgtaxis, ds, vdem, vel, dopaxis]
    ]

    if logdensaxis is not None:
        dens, logdensaxis = [dfunc(q) for q in [dens, logdensaxis]]

    # Clear out CUDA cache
    if torch.cuda.is_available():
        torch.cuda.empty_cache()

    return vdem


def get_vdem_highrun(
    tg,
    vel,
    ems,
    ds,
    dens=None,
    logdensaxis=None,
    axis=2,
    dopaxis=np.linspace(-100, 100, 21),
    lgtaxis=np.linspace(4, 9, 25),
    novelcut=False,
):
    """
    Calculates DEM as a function of temperature and velocity, i.e., VDEM.

    Parameters
    ----------
    tg - 3D array of gas temperature
    vel- 3D array of velocity
    ems- 3D array of n_e*n_H
    ds - 1D array of cell length along "axis" dimension
    axis - integer  x = 0, y = 1, z = 2
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    Returns
    -------
    array - ndarray
    Array with the dimensions of the 2D spatial from the simulation
    and temperature and velocity bins.
    """
    assert tg.shape[axis] == len(ds)

    nvel = len(dopaxis)
    ntg = len(lgtaxis)

    # The VDEM array will be of shape [ntg, nvel, dim0, dim1] (assume line-of-sight integration performed)
    vdem_shape = np.delete(vel.shape, axis).tolist()
    vdem_shape.insert(0, nvel)
    vdem_shape.insert(0, ntg)

    # Functions for converting between numpy arrays and torch tensors
    def cfunc(x):
        return np.array(x)

    def dfunc(x):
        return x

    np.einsum
    # vdem = np.zeros(vdem_shape)
    if torch.cuda.is_available():

        def cfunc(x):
            return torch.tensor(x).cuda()

        def dfunc(x):
            return x.cpu().numpy()

        torch.einsum
        if axis == 2:
            temp_x = np.zeros((vdem_shape[-2], vdem_shape[-1]))
        elif axis == 1:
            temp_x = np.zeros((vdem_shape[-2], vdem_shape[-1]))
        else:
            temp_x = np.zeros((vdem_shape[-2], vdem_shape[-1]))
        # vdem = torch.zeros(size=vdem_shape)
    # Convert to torch tensors

    # vel, tg, ems, dopaxis, lgtaxis, ds = [cfunc(q) for q in [vel, tg, ems, dopaxis, lgtaxis, ds]]

    delta_tg = lgtaxis[1] - lgtaxis[0]
    delta_vel = dopaxis[1] - dopaxis[0]
    if axis == 0:
        ems *= ds[:, np.newaxis, np.newaxis]
    elif axis == 1:
        ems *= ds[np.newaxis, :, np.newaxis]
    else:
        ems *= ds[np.newaxis, np.newaxis, :]

    if logdensaxis is not None:
        ndens = len(logdensaxis)
        dens, logdensaxis = [cfunc(q) for q in [dens, logdensaxis]]
        logdensaxis[1] - logdensaxis[0]
        vdem_shape.insert(0, ndens)

    binsize = 50

    for itg in range(0, ntg):
        # if itg % 2 == 0: print('(get_vdem) working on lgT bin({}/{})'.format(itg,ntg),end="\r",flush=True)
        masktg = tg >= 10.0 ** (lgtaxis[itg] - delta_tg / 2.0)
        masktg *= tg < 10.0 ** (lgtaxis[itg] + delta_tg / 2.0)
        for ivel in range(0, nvel):
            print(
                "(get_vdem) working on ilgT ivel bin({}/{},{}/{})".format(
                    itg, ntg, ivel, nvel
                ),
                end="\r",
                flush=True,
            )

            if ivel == 0 and novelcut:
                maskvel = masktg
            else: 
                maskvel = masktg * (vel >= dopaxis[ivel] - delta_vel / 2.0)
            if not(ivel == nvel - 1 and novelcut):
                maskvel *= vel < dopaxis[ivel] + delta_vel / 2.0

            if logdensaxis is not None:
                for idens in range(0, ndens):
                    maskdens = maskvel * dens >= 10.0 ** (
                        logdensaxis[itg] - logdensaxis / 2.0
                    )
                    maskdens *= dens < 10.0 ** (logdensaxis[itg] - logdensaxis / 2.0)
                    if torch.cuda.is_available():
                        if axis > 0:
                            for iix in range(0, vdem_shape[-2], binsize):
                                nx = np.min([vdem_shape[-2], iix + binsize])
                                temp_x[iix:nx, :] = dfunc(
                                    cfunc(ems[iix:nx, ...] * maskdens[iix:nx, ...]).sum(
                                        axis=axis
                                    )
                                )
                                torch.cuda.empty_cache()
                        else:
                            for iiz in range(0, vdem_shape[-1], binsize):
                                nz = np.min([vdem_shape[-1], iiz + binsize])
                                temp_x[:, iiz:nz] = dfunc(
                                    cfunc(ems[..., iiz:nz] * maskdens[..., iiz:nz]).sum(
                                        axis=axis
                                    )
                                )
                                torch.cuda.empty_cache()
                    else:
                        temp_x = np.sum(ems * maskdens, axis=axis)
                    if itg == 0 and ivel == 0:
                        vdem_shape = temp_x.shape
                        vdem_shape = np.insert(vdem_shape, 0, nvel)
                        vdem_shape = np.insert(vdem_shape, 0, ntg)
                        vdem_shape = np.insert(vdem_shape, 0, ndens)
                        vdem = np.zeros(vdem_shape)
                    vdem[itg, ivel, idens, ...] = temp_x
            else:
                if torch.cuda.is_available():
                    if axis > 0:
                        for iix in range(0, vdem_shape[-2], binsize):
                            nx = np.min([vdem_shape[-2], iix + binsize])
                            temp_x[iix:nx, :] = dfunc(
                                cfunc(ems[iix:nx, ...] * maskvel[iix:nx, ...]).sum(
                                    axis=axis
                                )
                            )
                            torch.cuda.empty_cache()
                    else:
                        for iiz in range(0, vdem_shape[-1], binsize):
                            nz = np.min([vdem_shape[-1], iiz + binsize])
                            temp_x[:, iiz:nz] = dfunc(
                                cfunc(ems[..., iiz:nz] * maskvel[..., iiz:nz]).sum(
                                    axis=axis
                                )
                            )
                            torch.cuda.empty_cache()
                else:
                    temp_x = np.sum(ems * maskvel, axis=axis)
                if itg == 0 and ivel == 0:
                    vdem_shape = temp_x.shape
                    vdem_shape = np.insert(vdem_shape, 0, nvel)
                    vdem_shape = np.insert(vdem_shape, 0, ntg)
                    vdem = np.zeros(vdem_shape)
                vdem[itg, ivel, ...] = temp_x

    return vdem


def get_radyn_vdem(
    self,
    snap=0,
    axis=2,
    dopaxis=np.linspace(-100, 100, 21),
    lgtaxis=np.linspace(4, 9, 25),
    **kwargs,
):
    """
    Purpose: Given a RADYN model, remap model onto a Cartesian Grid, including VDEM

    self: helita/RADYN data object
    snap: index of timestamp
    axis: integer  x = 0, y = 1, z = 2
    dopaxis: float vector  velocity axis in km/s
    lgtaxis: float vector  temperature axis in log10/K scale
    loop_width: in units of dx of the Cartesian grid (default = 1.0). Strictly speaking, loop_width should be <= 1.0 for a thin loop model to make sense
    dopaxis: centers of Doppler velocity bins [km/s]
    lgtaxis: centers of log T bins [K]
    dx: dx of Cartesian grid [cm]
    dz: dz of Cartesian grid [cm]
    sparse: Whether to store weights as a sparse matrix. Good idea when Cartesian grid much bigger than 100x100

    Output:
    vdem: ne*nH [cm^-6] as an array of shape [Nv,NlgT,Nx,Ny]
    lgtaxis: centers of log T bins
    dopaxis: centers of Doppler velocity bins km/s
    """

    loop_width, dx, dz, sparse, unitsnorm = [1, 1.0e7, 1.0e7, True, 1e27]

    if hasattr(self, "trans_dz"):
        dz = self.trans_dz
    if hasattr(self, "trans_dx"):
        dx = self.trans_dx
    if hasattr(self, "trans_loop_width"):
        loop_width = self.trans_loop_width
    if hasattr(self, "trans_sparse"):
        sparse = self.trans_sparse

    for key, value in kwargs.items():
        if key == "dx":
            dx = value
        if key == "dz":
            dz = value
        if key == "loop_width":
            loop_width = value
        if key == "loop_width":
            loop_width = value
        if key == "unitsnorm":
            unitsnorm = value

    # Semicircular loop
    s = self.rdobj.zm[snap]
    good = s >= 0.0
    s = s[good]
    ne = self.rdobj.ne1[snap][good]
    vz = self.rdobj.vz1[snap][good]
    tg = self.rdobj.tg1[snap][good]
    grph = 2.27e-24
    nh = self.rdobj.d1[snap][good] / grph

    ##GrahamKerr & JMS -- Sometimes zll is slightly different to the max of zm which causes problems on the assumption of a 1/4 loop.
    ##                    max(zm) fix the problem
    # smax = self.rdobj.cdf['zll'][snap]
    smax = np.max(self.rdobj.__getattr__("zm"))
    R = 2 * smax / np.pi
    x = np.cos(s / R) * R
    z = np.sin(s / R) * R

    nlgt, nv = [len(lgtaxis), len(dopaxis)]
    shape = (ceil(x.max() / dx), 1, ceil(z.max() / dz) + 1, nv, nlgt)

    # array holding n_e^2 of shape (Nx,Ny,Nz,Nv,NlgT)
    vdem_temp = np.zeros(shape, dtype=self.rdobj.ne1.dtype)

    # In the RADYN model in the corona, successive grid points may be several pixels away from each other.
    # In this case, need to refine loop.
    maxdl = np.abs(z[1:] - z[0:-1]).max()
    factor = ceil(2 * maxdl / np.min([dx, dz]))
    if factor > 1:
        ss, x = refine(s, x, factor=factor)
        ss, z = refine(s, z, factor=factor)
        ss, ne = refine(s, np.log(ne), factor=factor, unscale=np.exp)
        ss, nh = refine(s, np.log(nh), factor=factor, unscale=np.exp)
        ss, vz = refine(s, vz, factor=factor)
        ss, tg = refine(s, np.log(tg), factor=factor, unscale=np.exp)
    else:
        ss = s

    omega = ss / R

    # Arc lengths (in radians)
    dA = np.abs(omega[1:] - omega[0:-1])
    dA = dA.tolist()
    dA.insert(0, dA[0])
    dA.append(dA[-1])
    dA = np.array(dA)
    dA = 0.5 * (dA[1:] + dA[0:-1])
    # dA = R*dA*(loop_width*dx)
    dA = (
        0.5 * dA * ((R + 0.5 * loop_width * dx) ** 2 - (R - 0.5 * loop_width * dx) ** 2)
    )

    xind = np.floor(x / dx).astype(np.int64)
    zind = np.clip(np.floor(z / dz).astype(np.int64), 0, shape[2] - 1)

    # Define matrix with column coordinate corresponding to point along loop
    # and row coordinate corresponding to position in Cartesian grid
    col = np.arange(len(z), dtype=np.int64)
    row = xind * shape[2] + zind

    if sparse:
        M = coo_matrix(
            (dA / (dx * dz), (row, col)),
            shape=(shape[0] * shape[2], len(ss)),
            dtype=np.float,
        )
        M = M.tocsr()
    else:
        M = np.zeros(shape=(shape[0] * shape[2], len(ss)), dtype=np.float)
        M[row, col] = dA / (dx * dz)  # weighting by area of arc segment

    dv = dopaxis[1] - dopaxis[0]
    dlgt = lgtaxis[1] - lgtaxis[0]

    # Componnets of velocity in the x and z directions
    if axis == 0:
        vel = -vz * np.sin(omega) / 1e5  # velocity in the x directions km/s
        ds = dx
    elif axis == 1:
        print("Wrong axis!")
    elif axis == 2:
        vel = vz * np.cos(omega) / 1e5  # velocity in the z directions km/s
        ds = dz

    vind = (np.floor((vel - dopaxis[0]) / dv)).astype(np.int64)
    tind = (np.floor((np.log10(tg) - lgtaxis[0]) / dlgt)).astype(np.int64)

    for c in range(len(ss)):
        if vind[c] >= 0 and vind[c] < nv:
            if tind[c] >= 0 and tind[c] < nlgt:
                vdem_temp[..., vind[c], tind[c]] = vdem_temp[..., vind[c], tind[c]] + (
                    M[:, c].toarray() * ne[c] * nh[c]
                ).reshape(shape[0:3])  # need to replace by ne*nH

    vdem_temp /= unitsnorm

    vdem_shape = np.delete(shape[0:3], axis).tolist()
    vdem_shape.insert(0, nv)
    vdem_shape.insert(0, nlgt)

    vdem = torch.zeros(size=vdem_shape)

    # Functions for converting between numpy arrays and torch tensors
    def cfunc(x):
        return torch.tensor(x)

    def dfunc(x):
        return np.array(x)

    if torch.cuda.is_available():

        def cfunc(x):
            return torch.tensor(x).cuda()

        def dfunc(x):
            return x.cpu().numpy()

    # Convert to torch tensors
    ds, vdem_temp = [cfunc(q) for q in [ds, vdem_temp]]

    for itg in range(nlgt):
        if itg % 2 == 0:
            print(
                "(get_vdem) working on lgT bin({}/{})".format(itg, len(lgtaxis)),
                end="\r",
                flush=True,
            )
        for ivel in range(nv):
            vdem[itg, ivel, ...] = (vdem_temp[..., ivel, itg] * ds).sum(axis=axis)

    # Convert to numpy arrays
    ds, vdem = [dfunc(q) for q in [ds, vdem]]

    return vdem


def get_ion(spline):
    acont_filename = os.path.relpath(os.path.join(goft_path, spline + ".opy"))

    opfile = open(acont_filename, "rb")
    return pickle.load(opfile)


def hel2raster(mom, itrast=0.5):
    """
    Allows, for moments, to "raster" the observations in time. So, lets
    consider that to raster a region, the time require to scan will be
    equivalent to 3 snapshots (itrast=3). Then this routine will scan the
    region using 3 consecutive snapshots.

    Inputs
    -------
        mom - narray of the moments as a function of time (nt,nm,nw,nx,ny)
                 where nt is time
                 nm is the 3 moments, i.e., intensity, Doppler and width
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
        itrast - number of snapshots use to raster the full region

    Returns
    -------
        array - ndarray (nt_reduced,nm,nw,nx,ny)
                 nt_reduced is time, but now it will be a factor of nt/itrast
                 nm is the 3 moments, i.e., intensity, Doppler and width
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
    """
    nrast = int(np.shape(mom)[3] / 37)
    nt_nw = int(np.shape(mom)[0] / itrast)
    mom_ras_shape = np.append(nt_nw, np.shape(mom)[1:])
    mom_ras = np.zeros((mom_ras_shape))
    nt = np.shape(mom)[0]
    count = 0
    it_new = -1
    for it in range(nt):
        for icount in range(int(nrast / itrast)):
            if count % nrast == 0:
                count = 0
                it_new += 1
            try:
                mom_ras[it_new, :, :, np.arange(37) * nrast + count, :] = mom[
                    it, :, :, np.arange(37) * nrast + count, :
                ]
                count += 1
            except Exception:  # NOQA: BLE001
                print("missing last points")
    return mom_ras


def load_vdem_npz(self, vdemfile):
    """
    Reads the ground true VDEM.

    The npz file can be
        generated using bifrost.py for Bifrost simulations.
        It needs to be called prior doing the inversions:
        VDEMSInversionCode, or all_comb
    VDEM function shape (T, v, nx, ny).
    """

    vdem_str = np.load(vdemfile)
    self.vdem = vdem_str["vdem"]
    self.dopaxis = vdem_str["dopaxis"]
    self.lgtaxis = vdem_str["lgtaxis"]
    self.posaxis = vdem_str["posaxis"]

    self.nlnt = np.size(self.lgtaxis)
    self.ndop = np.size(self.dopaxis)

    return {
        "lgtaxis": self.lgtaxis,
        "dopaxis": self.dopaxis,
        "posaxis": np.array(("lgtaxis", "dopaxis")),
        "vdem": self.vdem,
    }


def load_vdem_str(self, vdem_str):
    """
    Reads the ground true VDEM.

    It needs to be called prior doing the inversions:
        VDEMSInversionCode, or all_comb
    VDEM function shape (T, v, nx, ny).
    """

    self.vdem = vdem_str["vdem"]
    self.dopaxis = vdem_str["dopaxis"]
    self.lgtaxis = vdem_str["lgtaxis"]
    self.posaxis = vdem_str["posaxis"]

    self.nlnt = np.size(self.lgtaxis)
    self.ndop = np.size(self.dopaxis)

    return {
        "lgtaxis": self.lgtaxis,
        "dopaxis": self.dopaxis,
        "posaxis": np.array(("lgtaxis", "dopaxis")),
        "vdem": self.vdem,
    }


def viggosplot(
    dd,
    ddsyn,
    qje,
    lprof,
    mom,
    iwvl=1,
    dx=0.167,
    dy=0.167,
    vartitle=r"$J^2/e$",
    axis=1,
    gamint=1.0,
    gamma=0.3,
    tiny=1.0e-5,
    widfix=0.0,
    velmax=100.0,
    zloc=[0.0, 2.0],
    bz=[[None], [None]],
):
    from time import strftime
    from time import gmtime
    from matplotlib.gridspec import GridSpec
    from matplotlib import pyplot as plt
    from matplotlib.colorbar import Colorbar
    from matplotlib import cm, colors
    from matplotlib import rc

    rc("xtick", labelsize="x-large")
    rc("ytick", labelsize="x-large")
    ncols = 3
    nrows = 2
    cc = 3.0e5
    arcsec2Mm = 0.725
    dspos = 4.45  # arcsec
    nslits = 37
    dspos * arcsec2Mm
    s1pos = 2.5 / arcsec2Mm
    title = ["Fe XIX [DN]", "Fe IX [DN]", "Fe XV [DN]"]
    if iwvl == 0:
        lambdao = 108.354
    elif iwvl == 1:
        lambdao = 171.073
    elif iwvl == 2:
        lambdao = 284.163
    elif iwvl == 3:
        lambdao = ddsyn.syn_sg[3]["LINE_WVL"]

    fontsz = "xx-large"

    if gamint > 0.99:
        title1 = " " + r"$\int I_{\nu}d_{\nu}$"
    else:
        title1 = " " + r"$(\int I_{{\nu}}d_{{\nu}})^{{\gamma={:0.2f}}}$".format(gamint)
    lam = ((ddsyn.syn_sg[iwvl]["DETWV"][0, :] - lambdao) / lambdao * cc)[
        : np.shape(lprof)[-1]
    ]

    int(np.shape(lprof)[-1] / 2)
    x = np.linspace(0, np.shape(mom)[-2] * dx, np.shape(mom)[-2])
    y = np.linspace(0, np.shape(mom)[-1] * dy, np.shape(mom)[-1])

    xyz_lim = (x.min(), x.max(), y.min(), y.max())
    hgt_ratio = (xyz_lim[3] - xyz_lim[2]) / (xyz_lim[1] - xyz_lim[0])

    fig_size = 8
    fig = plt.figure(
        figsize=(ncols * fig_size, fig_size * ((nrows + 1) * hgt_ratio + 0.04))
    )

    fig.suptitle(
        r"EUV %s Moments and Profiles vs %s" % (title[iwvl], vartitle),
        y=0.998,
        fontsize=fontsz,
    )

    (x.min(), x.max(), y.min(), y.max())
    xlabel = "x [arcsec]"
    ylabel = "y [arcsec]"

    height_ratio = []
    for j in range(0, nrows * 2, 2):
        height_ratio.append(0.04)
        height_ratio.append(hgt_ratio)

    gs = GridSpec(
        nrows * 2,
        ncols,
        width_ratios=[1 for i in range(ncols)],
        height_ratios=height_ratio,
        top=0.96,
        bottom=0.51,
    )
    for irow in range(0, nrows * 2, 2):
        cbax = [plt.subplot(gs[irow, i]) for i in range(ncols)]
        ax = [fig.add_subplot(gs[irow + 1, i]) for i in range(ncols)]

    extent_xy = (x.min(), x.max(), y.min(), y.max())
    xy_lim = (x.min(), x.max(), y.min(), y.max())

    for i in range(ncols):
        ax[i].set_xlim(xy_lim[0], xy_lim[1])
        ax[i].set_ylim(xy_lim[0], xy_lim[1])
        ax[i].set_xlabel(xlabel, fontsize=fontsz)
        ax[i].set_ylabel(ylabel, fontsize=fontsz)

    if iwvl == 0:
        ticks = [1, 3, 10, 30, 100, 300, 1000]
    else:
        ticks = [1, 300, 1000, 3000, 10000, 30000, 100000]
    vmin = 1.0
    #    vmin = np.max([np.min(mom[0,iwvl,...]),1.0])
    vmax = np.max(mom[0, iwvl, ...]) / 5.0
    im1 = ax[0].imshow(
        np.rot90((mom[0, iwvl, ...]), k=axis),
        cmap="jet",
        extent=extent_xy,
        norm=colors.PowerNorm(gamint, vmin=vmin, vmax=vmax),
    )
    print("min {}/max {}".format(vmin, vmax))
    cb1 = Colorbar(
        ax=cbax[0],
        mappable=im1,
        orientation="horizontal",
        ticklocation="bottom",
        ticks=ticks,
    )
    cb1.ax.tick_params(labelsize="x-large")
    #    cb1.ax.locator_params(nbins=5)
    cb1.set_label(
        title[iwvl] + title1 + " snap {}".format(dd.snap), labelpad=20, fontsize=fontsz
    )

    im2 = ax[1].imshow(
        np.rot90((mom[1, iwvl, ...]), k=axis),
        cmap=cm.RdBu_r,
        vmin=-1.0 * velmax,
        vmax=velmax,
        extent=extent_xy,
    )
    cb2 = Colorbar(
        ax=cbax[1], mappable=im2, orientation="horizontal", ticklocation="bottom"
    )
    cb2.ax.tick_params(labelsize="x-large")
    cb2.set_label(title[iwvl] + r" $u_{\rm Dopp}$ [km/s]", labelpad=20, fontsize=fontsz)

    im3 = ax[2].imshow(
        np.rot90((mom[2, iwvl, ...]), k=axis),
        cmap=cm.plasma,
        vmin=np.min(mom[2, iwvl, ...]),
        vmax=np.max(mom[2, iwvl, ...] - widfix),
        extent=extent_xy,
    )
    cb3 = Colorbar(
        ax=cbax[2], mappable=im3, orientation="horizontal", ticklocation="bottom"
    )
    cb3.ax.tick_params(labelsize="x-large")
    cb3.set_label(
        title[iwvl] + r" Width$_{\rm FWHM}$ [km/s]", labelpad=20, fontsize=fontsz
    )

    if bz[0][0] is None:
        bz[0] = dd.trans2comm("bz")
        ibzloc = np.argmin(np.abs(zloc - dd.z / 1.0e8))
        bz[0] = np.squeeze(bz[:, :, ibzloc])
    bmax = np.max(np.abs(bz[0]))
    im4 = ax[3].imshow(
        np.rot90(bz[0], k=axis),
        cmap=cm.Greys_r,
        vmin=-1 * bmax,
        vmax=bmax,
        extent=extent_xy,
    )
    cb4 = Colorbar(
        ax=cbax[3], mappable=im4, orientation="horizontal", ticklocation="bottom"
    )
    cb4.ax.tick_params(labelsize="x-large")
    cb4.set_label(
        r"$B_z$ [Gauss] @ z = {:0.2f} Mm".format(zloc), labelpad=20, fontsize=fontsz
    )

    if bz[1][0] is None:
        bz[1] = dd.trans2comm("bz")
        ibzloc = np.argmin(np.abs(zloc - dd.z / 1.0e8))
        bz[1] = np.squeeze(bz[:, :, ibzloc])
    bmax = np.max(np.abs(bz[1]))
    im5 = ax[4].imshow(
        np.rot90(bz[1], k=axis),
        cmap=cm.Greys_r,
        vmin=-1 * bmax,
        vmax=bmax,
        extent=extent_xy,
    )
    cb5 = Colorbar(
        ax=cbax[4], mappable=im5, orientation="horizontal", ticklocation="bottom"
    )
    cb5.ax.tick_params(labelsize="x-large")
    cb5.set_label(
        r"$B_z$ [Gauss] @ z = {:0.2f} Mm".format(zloc), labelpad=20, fontsize=fontsz
    )

    im6 = ax[5].imshow(
        np.rot90(np.log10(qje), k=axis),
        cmap="jet",
        vmin=np.max(np.log10(qje)) - 3,
        vmax=np.max(np.log10(qje)) - 0.2,
        extent=extent_xy,
    )
    cb6 = Colorbar(
        ax=cbax[5], mappable=im4, orientation="horizontal", ticklocation="bottom"
    )
    cb6.ax.tick_params(labelsize="x-large")
    cb6.set_label(
        r"$1/\tau$ at time " + strftime(" %H:%M:%S", gmtime(dd.time)),
        labelpad=20,
        fontsize=fontsz,
    )

    ################## Now come the spectra #######################################

    ncols_spec = int(np.min([nslits, x.max() / (dspos) - 1]))
    nrows = 1
    wr = 0.05  # distance between profiles
    gs1 = GridSpec(
        nrows + 1,
        ncols_spec,
        width_ratios=[i for i in np.zeros(ncols_spec) + wr],
        height_ratios=[0.03, hgt_ratio],
        top=0.48,
        bottom=0.03,
    )

    axs = [fig.add_subplot(gs1[1, i]) for i in range(0, ncols_spec)]
    ims = list()

    ypos = [i for i in np.arange(ncols_spec) * dspos + s1pos]
    extent_prof = (lam[0], lam[-1], y.min(), y.max())
    vmax_prof = np.max(mom[0, iwvl, ...]) / 10.0
    for i, yp in zip(range(0, ncols_spec), ypos):
        axs[i].set_xlim(-150, 150)
        axs[i].set_xlabel(r"$\lambda$ [km/s]", fontsize=fontsz)
        if i != 0:
            axs[i].set_yticks([])
        #            axs[i].set_xticks([])
        else:
            axs[i].set_ylabel("y [arcsec]", fontsize=fontsz)
        iloc = np.argmin(np.abs(x - ypos[i]))
        prof = np.maximum(lprof[iwvl, iloc, :, :], tiny)  # DN
        ims.append(
            axs[i].imshow(
                (prof[::-1, :]),
                extent=extent_prof,
                aspect="auto",
                cmap="jet",
                norm=colors.PowerNorm(gamma, vmin=1, vmax=vmax_prof),
            )
        )
        slitnr = "{}".format(i)
        axs[i].set_title(slitnr)
        ax[0].plot(
            [yp, yp],
            [xy_lim[2], xy_lim[3]],
            "--",
            linewidth=0.7,
            color="white",
            alpha=1.0,
        )
        ax[5].plot(
            [yp, yp],
            [xy_lim[2], xy_lim[3]],
            "--",
            linewidth=0.7,
            color="white",
            alpha=1.0,
        )
        ax[0].annotate(slitnr, (yp - 1.0, 5.0), color="white")
        ax[5].annotate(slitnr, (yp - 1.0, 5.0), color="white")
    cbaxs = plt.subplot(gs1[0, :])  # Place it where it should be.
    cbs = Colorbar(
        ax=cbaxs, mappable=ims[0], orientation="horizontal", ticklocation="bottom"
    )
    cbs.ax.tick_params(labelsize="x-large")
    cbs.set_label(
        r"$I_\nu^{{\gamma={:0.2f}}}$ [DN]".format(gamma), labelpad=-60, fontsize=fontsz
    )
    return {
        "im1": im1,
        "im2": im2,
        "im3": im3,
        "im4": im4,
        "im5": im5,
        "im6": im6,
        "ims": ims,
        "cb1": cb1,
        "cb2": cb2,
        "cb3": cb3,
        "cb4": cb4,
        "cb5": cb5,
        "cb6": cb6,
        "cbs": cbs,
        "ypos": ypos,
        "x": x,
        "fig": fig,
    }
