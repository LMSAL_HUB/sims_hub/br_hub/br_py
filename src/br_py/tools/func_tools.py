import numpy as np
from scipy.interpolate import interp1d
import importlib

spam_spec = importlib.util.find_spec("pycuda")
found = spam_spec is not None


def refine(s, q, factor=2, unscale=lambda x: x):
    """
    Given 1D function q(s), interpolate so we have factor x many points.

    factor = 2 by default
    """
    ds = s[-1] - s[0]
    ss = np.arange(factor * len(s) + 1) / (factor * len(s)) * ds + s[0]
    if ds > 0.0:
        qq = unscale(np.interp(ss, s, q))
        return ss, qq
    elif ds < 0.0:
        qq = unscale(np.interp(ss[::-1], s[::-1], q[::-1]))
        qq = qq[::-1]
        return ss, qq


def pixres_xy(x, y, data, dx_new=0.16, dy_new=0.16):
    if dx_new is not None:
        f = interp1d(x, data, axis=0, kind="linear")
        nxaxis = (np.max(x) - np.min(x)) / dx_new
        if nxaxis > len(x):
            new_x = np.arange(nxaxis) / float(nxaxis) * (
                np.max(x) - np.min(x)
            ) + np.min(x)
            var_temp = f(new_x)
            x_axis = new_x
        else:
            nxaxis = nxaxis * 10
            new_x = np.arange(nxaxis) / float(nxaxis) * (
                np.max(x) - np.min(x)
            ) + np.min(x)
            var_temp = f(new_x[0 : int(len(new_x) / 10) * 10])
            newshape = np.append(
                np.array([int(nxaxis / 10), 10]), np.shape(data[0, ...])
            )
            var_temp = np.reshape(
                var_temp[: int(nxaxis / 10) * 10, ...], (newshape)
            ).mean(axis=1)
            x_axis = np.array((new_x[::10])[: int(nxaxis / 10)])
    else:
        var_temp = data
        x_axis = x

    if dy_new is not None:
        f = interp1d(y, var_temp, axis=1, kind="linear")
        nyaxis = (np.max(y) - np.min(y)) / dy_new
        if nyaxis > len(y):
            new_y = np.arange(nyaxis) / float(nyaxis) * (
                np.max(y) - np.min(y)
            ) + np.min(y)
            var_temp = f(new_y)
            y_axis = new_y
        else:
            nyaxis = nyaxis * 10
            new_y = np.arange(nyaxis) / float(nyaxis) * (
                np.max(y) - np.min(y)
            ) + np.min(y)
            var_temp = f(new_y[0 : int(len(new_y) / 10) * 10])
            newshape = (len(x_axis), int(nyaxis / 10), 10)
            var_temp = np.reshape(
                var_temp[..., : int(nyaxis / 10) * 10], (newshape)
            ).mean(axis=-1)
            y_axis = np.array((new_y[::10])[: int(nyaxis / 10)])
    else:
        y_axis = y

    return x_axis, y_axis, var_temp


def pixres_xt(x, data, dx_new=0.16):
    f = interp1d(x, data, axis=0, kind="linear")
    dxaxis_new = dx_new / 10
    nxaxis = (np.max(x) - np.min(x)) / dxaxis_new
    new_x = np.arange(nxaxis) / float(nxaxis) * (np.max(x) - np.min(x)) + np.min(x)
    var_temp = f(new_x)

    nyaxis = len(data[0, :])
    newshape = (int(nxaxis / 10), 10, nyaxis)
    return np.array((new_x[::10])[: int(nxaxis / 10)]), np.reshape(
        var_temp[: int(nxaxis / 10) * 10, :], (newshape)
    ).mean(axis=1)


def viggosplot(
    dd,
    ddsyn,
    qje,
    lprof,
    mom,
    iwvl=1,
    dx=0.167,
    dy=0.167,
    vartitle=r"$J^2/e$",
    axis=1,
    gamint=1.0,
    gamma=0.3,
    tiny=1.0e-5,
    widfix=0.0,
    velmax=100.0,
    zloc=[0.0, 2.0],
    bz=[[None], [None]],
):
    from time import strftime
    from time import gmtime
    from matplotlib.gridspec import GridSpec
    from matplotlib import pyplot as plt
    from matplotlib.colorbar import Colorbar
    from matplotlib import cm, colors
    from matplotlib import rc

    rc("xtick", labelsize="x-large")
    rc("ytick", labelsize="x-large")
    ncols = 3
    nrows = 2
    cc = 3.0e5
    arcsec2Mm = 0.725
    dspos = 4.45  # arcsec
    nslits = 37
    dspos * arcsec2Mm
    s1pos = 2.5 / arcsec2Mm
    title = ["Fe XIX [DN]", "Fe IX [DN]", "Fe XV [DN]"]
    if iwvl == 0:
        lambdao = 108.354
    elif iwvl == 1:
        lambdao = 171.073
    elif iwvl == 2:
        lambdao = 284.163
    elif iwvl == 3:
        lambdao = ddsyn.syn_sg[3]["LINE_WVL"]

    fontsz = "xx-large"

    if gamint > 0.99:
        title1 = " " + r"$\int I_{\nu}d_{\nu}$"
    else:
        title1 = " " + r"$(\int I_{{\nu}}d_{{\nu}})^{{\gamma={:0.2f}}}$".format(gamint)
    lam = ((ddsyn.syn_sg[iwvl]["DETWV"][0, :] - lambdao) / lambdao * cc)[
        : np.shape(lprof)[-1]
    ]

    int(np.shape(lprof)[-1] / 2)
    x = np.linspace(0, np.shape(mom)[-2] * dx, np.shape(mom)[-2])
    y = np.linspace(0, np.shape(mom)[-1] * dy, np.shape(mom)[-1])

    xyz_lim = (x.min(), x.max(), y.min(), y.max())
    hgt_ratio = (xyz_lim[3] - xyz_lim[2]) / (xyz_lim[1] - xyz_lim[0])

    fig_size = 8
    fig = plt.figure(
        figsize=(ncols * fig_size, fig_size * ((nrows + 1) * hgt_ratio + 0.04))
    )

    fig.suptitle(
        r"EUV %s Moments and Profiles vs %s" % (title[iwvl], vartitle),
        y=0.998,
        fontsize=fontsz,
    )

    (x.min(), x.max(), y.min(), y.max())
    xlabel = "x [arcsec]"
    ylabel = "y [arcsec]"

    height_ratio = []
    for j in range(0, nrows * 2, 2):
        height_ratio.append(0.04)
        height_ratio.append(hgt_ratio)

    gs = GridSpec(
        nrows * 2,
        ncols,
        width_ratios=[1 for i in range(ncols)],
        height_ratios=height_ratio,
        top=0.96,
        bottom=0.51,
    )
    for irow in range(0, nrows * 2, 2):
        cbax = [plt.subplot(gs[irow, i]) for i in range(ncols)]
        ax = [fig.add_subplot(gs[irow + 1, i]) for i in range(ncols)]

    extent_xy = (x.min(), x.max(), y.min(), y.max())
    xy_lim = (x.min(), x.max(), y.min(), y.max())

    for i in range(ncols):
        ax[i].set_xlim(xy_lim[0], xy_lim[1])
        ax[i].set_ylim(xy_lim[0], xy_lim[1])
        ax[i].set_xlabel(xlabel, fontsize=fontsz)
        ax[i].set_ylabel(ylabel, fontsize=fontsz)

    if iwvl == 0:
        ticks = [1, 3, 10, 30, 100, 300, 1000]
    else:
        ticks = [1, 300, 1000, 3000, 10000, 30000, 100000]
    vmin = 1.0
    #    vmin = np.max([np.min(mom[0,iwvl,...]),1.0])
    vmax = np.max(mom[0, iwvl, ...]) / 5.0
    im1 = ax[0].imshow(
        np.rot90((mom[0, iwvl, ...]), k=axis),
        cmap="jet",
        extent=extent_xy,
        norm=colors.PowerNorm(gamint, vmin=vmin, vmax=vmax),
    )
    print("min {}/max {}".format(vmin, vmax))
    cb1 = Colorbar(
        ax=cbax[0],
        mappable=im1,
        orientation="horizontal",
        ticklocation="bottom",
        ticks=ticks,
    )
    cb1.ax.tick_params(labelsize="x-large")
    #    cb1.ax.locator_params(nbins=5)
    cb1.set_label(
        title[iwvl] + title1 + " snap {}".format(dd.snap), labelpad=20, fontsize=fontsz
    )

    im2 = ax[1].imshow(
        np.rot90((mom[1, iwvl, ...]), k=axis),
        cmap=cm.RdBu_r,
        vmin=-1.0 * velmax,
        vmax=velmax,
        extent=extent_xy,
    )
    cb2 = Colorbar(
        ax=cbax[1], mappable=im2, orientation="horizontal", ticklocation="bottom"
    )
    cb2.ax.tick_params(labelsize="x-large")
    cb2.set_label(title[iwvl] + r" $u_{\rm Dopp}$ [km/s]", labelpad=20, fontsize=fontsz)

    im3 = ax[2].imshow(
        np.rot90((mom[2, iwvl, ...]), k=axis),
        cmap=cm.plasma,
        vmin=np.min(mom[2, iwvl, ...]),
        vmax=np.max(mom[2, iwvl, ...] - widfix),
        extent=extent_xy,
    )
    cb3 = Colorbar(
        ax=cbax[2], mappable=im3, orientation="horizontal", ticklocation="bottom"
    )
    cb3.ax.tick_params(labelsize="x-large")
    cb3.set_label(
        title[iwvl] + r" Width$_{\rm FWHM}$ [km/s]", labelpad=20, fontsize=fontsz
    )

    if bz[0][0] is None:
        bz[0] = dd.trans2comm("bz")
        ibzloc = np.argmin(np.abs(zloc - dd.z / 1.0e8))
        bz[0] = np.squeeze(bz[:, :, ibzloc])
    bmax = np.max(np.abs(bz[0]))
    im4 = ax[3].imshow(
        np.rot90(bz[0], k=axis),
        cmap=cm.Greys_r,
        vmin=-1 * bmax,
        vmax=bmax,
        extent=extent_xy,
    )
    cb4 = Colorbar(
        ax=cbax[3], mappable=im4, orientation="horizontal", ticklocation="bottom"
    )
    cb4.ax.tick_params(labelsize="x-large")
    cb4.set_label(
        r"$B_z$ [Gauss] @ z = {:0.2f} Mm".format(zloc), labelpad=20, fontsize=fontsz
    )

    if bz[1][0] is None:
        bz[1] = dd.trans2comm("bz")
        ibzloc = np.argmin(np.abs(zloc - dd.z / 1.0e8))
        bz[1] = np.squeeze(bz[:, :, ibzloc])
    bmax = np.max(np.abs(bz[1]))
    im5 = ax[4].imshow(
        np.rot90(bz[1], k=axis),
        cmap=cm.Greys_r,
        vmin=-1 * bmax,
        vmax=bmax,
        extent=extent_xy,
    )
    cb5 = Colorbar(
        ax=cbax[4], mappable=im5, orientation="horizontal", ticklocation="bottom"
    )
    cb5.ax.tick_params(labelsize="x-large")
    cb5.set_label(
        r"$B_z$ [Gauss] @ z = {:0.2f} Mm".format(zloc), labelpad=20, fontsize=fontsz
    )

    im6 = ax[5].imshow(
        np.rot90(np.log10(qje), k=axis),
        cmap="jet",
        vmin=np.max(np.log10(qje)) - 3,
        vmax=np.max(np.log10(qje)) - 0.2,
        extent=extent_xy,
    )
    cb6 = Colorbar(
        ax=cbax[5], mappable=im4, orientation="horizontal", ticklocation="bottom"
    )
    cb6.ax.tick_params(labelsize="x-large")
    cb6.set_label(
        r"$1/\tau$ at time " + strftime(" %H:%M:%S", gmtime(dd.time)),
        labelpad=20,
        fontsize=fontsz,
    )

    ################## Now come the spectra #######################################

    ncols_spec = int(np.min([nslits, x.max() / (dspos) - 1]))
    nrows = 1
    wr = 0.05  # distance between profiles
    gs1 = GridSpec(
        nrows + 1,
        ncols_spec,
        width_ratios=[i for i in np.zeros(ncols_spec) + wr],
        height_ratios=[0.03, hgt_ratio],
        top=0.48,
        bottom=0.03,
    )

    axs = [fig.add_subplot(gs1[1, i]) for i in range(0, ncols_spec)]
    ims = list()

    ypos = [i for i in np.arange(ncols_spec) * dspos + s1pos]
    extent_prof = (lam[0], lam[-1], y.min(), y.max())
    vmax_prof = np.max(mom[0, iwvl, ...]) / 10.0
    for i, yp in zip(range(0, ncols_spec), ypos):
        axs[i].set_xlim(-150, 150)
        axs[i].set_xlabel(r"$\lambda$ [km/s]", fontsize=fontsz)
        if i != 0:
            axs[i].set_yticks([])
        #            axs[i].set_xticks([])
        else:
            axs[i].set_ylabel("y [arcsec]", fontsize=fontsz)
        iloc = np.argmin(np.abs(x - ypos[i]))
        prof = np.maximum(lprof[iwvl, iloc, :, :], tiny)  # DN
        ims.append(
            axs[i].imshow(
                (prof[::-1, :]),
                extent=extent_prof,
                aspect="auto",
                cmap="jet",
                norm=colors.PowerNorm(gamma, vmin=1, vmax=vmax_prof),
            )
        )
        slitnr = "{}".format(i)
        axs[i].set_title(slitnr)
        ax[0].plot(
            [yp, yp],
            [xy_lim[2], xy_lim[3]],
            "--",
            linewidth=0.7,
            color="white",
            alpha=1.0,
        )
        ax[5].plot(
            [yp, yp],
            [xy_lim[2], xy_lim[3]],
            "--",
            linewidth=0.7,
            color="white",
            alpha=1.0,
        )
        ax[0].annotate(slitnr, (yp - 1.0, 5.0), color="white")
        ax[5].annotate(slitnr, (yp - 1.0, 5.0), color="white")
    cbaxs = plt.subplot(gs1[0, :])  # Place it where it should be.
    cbs = Colorbar(
        ax=cbaxs, mappable=ims[0], orientation="horizontal", ticklocation="bottom"
    )
    cbs.ax.tick_params(labelsize="x-large")
    cbs.set_label(
        r"$I_\nu^{{\gamma={:0.2f}}}$ [DN]".format(gamma), labelpad=-60, fontsize=fontsz
    )
    return {
        "im1": im1,
        "im2": im2,
        "im3": im3,
        "im4": im4,
        "im5": im5,
        "im6": im6,
        "ims": ims,
        "cb1": cb1,
        "cb2": cb2,
        "cb3": cb3,
        "cb4": cb4,
        "cb5": cb5,
        "cb6": cb6,
        "cbs": cbs,
        "ypos": ypos,
        "x": x,
        "fig": fig,
    }
