import numpy as np
import importlib
import torch
import time
from scipy.interpolate import interp1d

spam_spec = importlib.util.find_spec("pycuda")
found = spam_spec is not None

# Crucial the above uses a function similar to the following. It is crucial to use same function
# or at least save with same names and format in order to use the synthetic code.


def time_int(syn, time, timeint=1.85, mag=2, kind="linear"):
    """
    Allows, for spectral profiles, to integrate (or interpolate) in time taking
    into account the time integration.

    Inputs
    -------
        syn - narray of the moments as a function of time (nt,nw,nx,ny,nwvl)
                 where
                 nt is time
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                     size or a reduced one for a single slit (0).
        time - array (nt), time in seconds of each synthetic spectrum
        timeint - float  time integration in seconds

    Returns
    -------
        array - ndarray (nt_new,nw,nx,ny,nwl)
                 nt_new is time, but now it will be a factor that
                     depends on the time integration
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                     size or a reduced one for a single slit (0).
    """
    dt = np.gradient(time)
    dt_min = np.min(dt)
    syn_shape = np.shape(syn)

    fact = np.ceil(timeint / dt_min).astype(np.int64)
    ntime = np.ceil((time[-1] - time[0]) / timeint) * np.max([fact, mag])
    new_t = np.arange(ntime) * (time[-1] - time[0]) / ntime + time[0]

    fsyn = interp1d(time, syn, axis=0, kind=kind)
    syn_interp = fsyn(new_t)
    syn_interp_shape = np.shape(syn_interp)

    ncut = np.ceil(np.max([timeint * fact, timeint * mag]) / timeint).astype(np.int64)

    newshape = np.array(
        np.append(int(syn_interp_shape[0] / ncut), np.append(ncut, syn_shape[1:]))
    )

    return np.reshape(syn_interp, (newshape)).mean(axis=1)


def raster_scan(syn_int, nrasteps=None):
    """
    Allows, for spectral profiles, to "raster" in time assuming that each step
    is a full MUSE time step and a dense raster, i.e., 11 steps. It allows to
    have less than 37 slits as well as different size for wavelengths.

    Inputs
    -------
        syn - narray of the profiles as a function of time (nt,nw,nx,ny,nwvl)
                 where nt is time
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                 size or a reduced one for a single slit (0).
        nrasteps - int raster steps, for muse should be 11.

    Returns
    -------
        array - ndarray (nt_reduced,nw,nx,ny,nwl)
                 nt is time
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                 size or a reduced one for a single slit (0).
    """

    syn_int_shape = np.shape(syn_int)
    syn_raster = np.zeros(syn_int_shape)

    nslit = int(np.shape(syn_raster)[2] / nrasteps) - 1
    for it in range(syn_int_shape[0]):
        if it > 0:
            syn_raster[it, ...] = syn_raster[it - 1, ...]
        if nrasteps is None:
            syn_raster[it, :, it % nsteps, ...] = syn_int[it, :, it % nsteps, ...]
        else:
            syn_raster[
                it, :, np.arange(nslit) * nrasteps + it % nrasteps, ...
            ] = syn_int[it, :, np.arange(nslit) * nrasteps + it % nrasteps, ...]

    return syn_raster


def raster_int(syn, time, timeint=1.85, mag=2, kind="linear", nrasteps=None):
    """
    For spectral profiles, it will integrate (or interpolate) in time taking
    into account the time integration and "raster" in time assuming that each
    step is a full MUSE time step and a dense raster, i.e., 11 steps. It allows
    to have less than 37 slits as well as different size for wavelengths.

    Inputs
    -------
        syn - narray of the moments as a function of time (nt,nw,nx,ny,nwvl)
                 where
                 nt is time
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                     size or a reduced one for a single slit (0).
        time - array (nt), time in seconds of each synthetic spectrum
        timeint - float  time integration in seconds
        nrasteps - int raster steps, for muse should be 11.
    Returns
    -------
        array - ndarray (nt_reduced,nw,nx,ny,nwl)
                 nt is time
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                 size or a reduced one for a single slit (0).
    """
    syn_int = time_int(syn, time, timeint=timeint, mag=mag, kind=kind)
    return raster_scan(syn_int, nrasteps=nrasteps)


def doppler_to_lambda(doppler, wave0):
    """
    Inputs
    -------
        array - ndarray Doppler units of km/s (nwl)
        wave0 - real wavelength rest position
    Returns
    -------
        array - ndarray units of wave0 (Amgstrongs) (nwl)
    """
    cc = 2.99792458e10 / 1e5
    return wave0 * (1.0 + doppler / cc)  # outputs in units of wave0


def lambda_to_doppler(wave, wave0):
    """
    Inputs
    -------
        array - ndarray units of wave0 (Amgstrongs) (nwl)
        wave0 - real wavelength rest position
    Returns
    -------
        array - ndarray Doppler units of km/s (nwl)
    """
    cc = 2.99792458e10 / 1e5
    return (wave / wave0 - 1.0) * cc  # outputs in km/s


def cgs_ph(data, wvl):
    """
    Inputs
    -------
        data - np.array
            is erg/cm^2/s/sr and/or /angs
        wvl - float
            wvl is in angstroms.
    Returns
    -------
        array - ndarray
            data in ph/cm^2/s/sr and/or /angs
    """
    h = 6.626e-27  # Planck constant in ergs s
    c = 2.998e18  # speed of light in Angstrom s-1

    return data * wvl / (h * c)


def cm_pix(data, pix=[0.16, 0.16], wvl=None, units="perhz", sr=False):
    """
    Data is spectrum as [wvl, x, y ...

    ], i.e., first axis wvl
    """

    if not sr:
        radeg = 57.295776
        r2a = radeg * 3.6e3  # arcsec per radian
        c = 2.998e18  # speed of light in Angstrom s-1
        counts = data * pix[0] * pix[1] / (r2a * r2a)
    else:
        counts = data

    if np.any(wvl) is not None:
        if len(np.shape(data)) == 4:
            opers = "wijk,w->wijk"
        elif len(np.shape(data)) == 3:
            opers = "wij,w->wij"
        elif len(np.shape(data)) == 2:
            opers = "wi,w->wi"
        if units == "perhz":
            nu = c / wvl
            dnu = np.abs(np.gradient(nu))
        else:
            dnu = np.abs(np.gradient(wvl))
        counts = np.einsum(opers, counts, dnu)

    return counts


def calc_moments_spect(spect, dopp):
    """
    Computes 0th, 1st and 2nd moments from the synthetic profiles. Output is a
    list of 3 components which corresponds to the moments. Each component is a
    3D array (nwvl, nslits x nraster, ny)

    Velocity convention: Note the doppler shift convection is the spectroscopist and MUSE response
        function convection (Not helita convention). Consequently,
        in first moment negative means towards us.
    """

    # Functions for converting between numpy arrays and torch tensors
    def cfunc(x):
        return np.array(x)

    def dfunc(x):
        return x

    teinsum = np.einsum
    tsum = np.sum
    tsqrt = np.sqrt
    if torch.cuda.is_available():

        def cfunc(x):
            return torch.tensor(x.astype("float32")).cuda()

        def dfunc(x):
            return x.cpu().numpy()

        teinsum = torch.einsum
        tsum = torch.sum
        tsqrt = torch.sqrt

    # Note the doppler shift convection is the spectroscopist and MUSE response
    # function convection (Not helita convention)

    # I0 = \int(I)
    zeromom = dfunc(tsum(cfunc(spect), axis=1))
    # I1= \int(I v) / I0
    firstmom = dfunc(teinsum("wij,w->ij", cfunc(spect), cfunc(dopp))) / zeromom
    # sqrt( \int I (v - I1)^2 / I0)
    for iy in range(nx):
        for ix in range(ny):
            secondmom[iwvl, ix, iy] = dfunc(
                tsqrt(
                    tsum(cfunc(syn_troch[ix, :, iy] * (dopp - firstmom[ix, iy]) ** 2))
                    / cfunc(zeromom[ix, iy])
                )
            )

    if torch.cuda.is_available():
        torch.cuda.empty_cache()

    return zeromom, firstmom, secondmom


def from_hel2mom_museres(
    dd,
    ddsyn,
    snap,
    dopaxis,
    lgtaxis,
    axis=2,
    interp_vdem=False,
    radyn=False,
    onetile=False,
    cutresp=False,
    **kwargs,
):
    """
    Computes moments directly, muse spatial resolution and cgs (or km/s).

    Parameters
    dd - class of the helita model
    ddsyn - class of the muse_py synthesis
    snap - integer  snapshot number
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    axis - integer  x = 0, y = 1, z = 2: LOS integration
    interp_vdem - logic will run high interpolation along the LOS instead of suming pixels
    radyn - logic specific for Radyn models which will run get_radyn_vdem
    onetile - logic will tile one time the box so it is not clip when it is converted to MUSE resolution
    cutresp - logic will cut the number of slits in the response function imposing 11 raster steps
        between the slits.

    Returns
    -------
    array - ndarray (nm,nw,nx,ny)
            where nm is the 3 moments, i.e., intensity, Doppler and width
            nw are the spectral windows, i.e, 3= 108, 171 and 284,
            nx spatial axis (reduced to muse spatial resolution) (see synthetize_muse for extra information),
            ny spatial axis (reduced to muse spatial resolution),
    """
    from_hel2syn_museres(
        dd,
        ddsyn,
        snap,
        dopaxis,
        lgtaxis,
        axis,
        interp_vdem=interp_vdem,
        radyn=radyn,
        onetile=onetile,
        cutresp=cutresp,
        **kwargs,
    )

    return calc_moments(ddsyn)


def from_hel2mom_muse_res_dn(
    dd,
    ddsyn,
    snap,
    dopaxis,
    lgtaxis,
    axis=2,
    timeint=1.5,
    interp_vdem=False,
    radyn=False,
    onetile=False,
    cutresp=False,
    **kwargs,
):
    """
    Computes moments directly, muse spatial resolution and DNs.

    Parameters
    dd - class of the helita model
    ddsyn - class of the muse_py synthesis
    snap - integer  snapshot number
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    axis - integer  x = 0, y = 1, z = 2: LOS integration
    timeint - float  time integration in seconds
    interp_vdem - logic will run high interpolation along the LOS instead of suming pixels
    radyn - logic specific for Radyn models which will run get_radyn_vdem
    onetile - logic will tile one time the box so it is not clip when it is converted to MUSE resolution
    cutresp - logic will cut the number of slits in the response function imposing 11 raster steps
        between the slits.

    Returns
    -------
    array - ndarray (nm,nw,nx,ny)
            where nm is the 3 moments, i.e., intensity, Doppler and width
            nw are the spectral windows, i.e, 3= 108, 171 and 284,
            nx spatial axis (reduced to muse spatial resolution) (see synthetize_muse for extra information),
            ny spatial axis (reduced to muse spatial resolution),
    """

    all_prof = from_hel2syn_museres(
        dd,
        ddsyn,
        snap,
        dopaxis,
        lgtaxis,
        axis,
        interp_vdem=interp_vdem,
        radyn=radyn,
        onetile=onetile,
        cutresp=cutresp,
        **kwargs,
    )

    for iw in range(0, 3):
        if ddsyn.sg[iw]["BAND"] == 108:
            gain = 1.0 / 2.0
        elif ddsyn.sg[iw]["BAND"] == 171:  # same as AIA convention
            gain = 1.0 / 1.25
        elif ddsyn.sg[iw]["BAND"] == 284:
            gain = 1.0 / 0.75
        all_prof[iw, :, :, :] *= gain * timeint
    return calc_moments(ddsyn)


def from_hel2mom(
    dd, ddsyn, snap, dopaxis, lgtaxis, axis=2, interp_vdem=False, radyn=False, **kwargs
):
    """
    Computes moments directly, native spatial resolution and cgs (or km/s).

    Parameters
    dd - class of the helita model
    ddsyn - class of the muse_py synthesis
    snap - integer  snapshot number
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    axis - integer  x = 0, y = 1, z = 2: LOS integration
    interp_vdem - logic will run high interpolation along the LOS instead of suming pixels
    radyn - logic specific for Radyn models which will run get_radyn_vdem

    Returns
    -------
    array - ndarray (nm,nw,nx,ny)
            where nm is the 3 moments, i.e., intensity, Doppler and width
            nw are the spectral windows, i.e, 3= 108, 171 and 284,
            nx spatial axis (same as numerical model), but see synthetize_muse
            ny spatial axis (same as numerical model),
    """
    from_hel2syn(
        dd, ddsyn, snap, dopaxis, lgtaxis, axis, interp_vdem=interp_vdem, radyn=radyn
    )
    return calc_moments(ddsyn)


def from_hel2mom_time(
    dd,
    ddsyn,
    snaprange,
    dopaxis,
    lgtaxis,
    axis=2,
    muse_res=True,
    interp_vdem=False,
    radyn=False,
    onetile=False,
    cutresp=False,
    **kwargs,
):
    """
    Computes moments directly as a function of time, native or muse spatial
    resolution and cgs (or km/s).

    Parameters
    dd - class of the helita model
    ddsyn - class of the muse_py synthesis
    snap - integer  snapshot number
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    axis - integer  x = 0, y = 1, z = 2: LOS integration
    interp_vdem - logic will run high interpolation along the LOS instead of suming pixels
    radyn - logic specific for Radyn models which will run get_radyn_vdem
    onetile - logic will tile one time the box so it is not clip when it is converted to MUSE resolution
    muse_res - logic , True means MUSE spatial resolution, False means native resolution
    cutresp - logic will cut the number of slits in the response function imposing 11 raster steps
        between the slits.

    Returns
    -------
    array - ndarray (nt,nm,nw,nx,ny)
            where nt is time
            nm is the 3 moments, i.e., intensity, Doppler and width
            nw are the spectral windows, i.e, 3= 108, 171 and 284,
            nx spatial axis either muse or numerical resolution (see synthetize_muse for extra information),
            ny spatial axis either muse or numerical resolution,
    """

    if muse_res:
        moms = from_hel2mom_museres(
            dd,
            ddsyn,
            snaprange[0],
            dopaxis,
            lgtaxis,
            axis,
            interp_vdem=interp_vdem,
            radyn=radyn,
            onetile=onetile,
            cutresp=cutresp,
            **kwargs,
        )
    else:
        moms = from_hel2mom(
            dd,
            ddsyn,
            snaprange[0],
            dopaxis,
            lgtaxis,
            axis,
            interp_vdem=interp_vdem,
            radyn=radyn,
            **kwargs,
        )
    momshape = np.append(len(snaprange), np.shape(moms))
    mom_t = np.zeros(momshape)
    mom_t[0, ...] = moms
    for isnap, snap in enumerate(snaprange[1:]):
        start = time.time()

        if muse_res:
            mom_t[isnap, ...] = from_hel2mom_museres(
                dd,
                ddsyn,
                snap,
                dopaxis,
                lgtaxis,
                axis,
                interp_vdem=interp_vdem,
                radyn=radyn,
                onetile=onetile,
                cutresp=cutresp,
                **kwargs,
            )
        else:
            mom_t[isnap, ...] = from_hel2mom(
                dd,
                ddsyn,
                snap,
                dopaxis,
                lgtaxis,
                axis,
                interp_vdem=interp_vdem,
                radyn=radyn,
                **kwargs,
            )

        end = time.time()
        print("it=%i done in %i s" % (snap, end - start))

    return mom_t


def from_hel2mom_time_muse_dn(
    dd,
    ddsyn,
    snaprange,
    dopaxis,
    lgtaxis,
    axis=2,
    muse_res=True,
    timeint=1.5,
    interp_vdem=False,
    radyn=False,
    onetile=False,
    cutresp=False,
    **kwargs,
):
    """
    Computes moments directly as a function of time, native spatial resolution
    and DN (or km/s).

    Parameters
    dd - class of the helita model
    ddsyn - class of the muse_py synthesis
    snap - integer  snapshot number
    dopaxis - float vector  velocity axis in km/s
    lgtaxis - float vector  temperature axis in log10/K scale
    axis - integer  x = 0, y = 1, z = 2: LOS integration
    muse_res - logic , True means MUSE spatial resolution, False means native resolution
    timeint - float  time integration in seconds
    interp_vdem - logic will run high interpolation along the LOS instead of suming pixels
    radyn - logic specific for Radyn models which will run get_radyn_vdem
    onetile - logic will tile one time the box so it is not clip when it is converted to MUSE resolution
    cutresp - logic will cut the number of slits in the response function imposing 11 raster steps
        between the slits.

    Returns
    -------
    array - ndarray (nt,nm,nw,nx,ny)
            where nt is time
            nm is the 3 moments, i.e., intensity, Doppler and width
            nw are the spectral windows, i.e, 3= 108, 171 and 284,
            nx spatial axis either muse or numerical resolution (see synthetize_muse for extra information),
            ny spatial axis either muse or numerical resolution,
    """
    moms = from_hel2mom_time(
        dd,
        ddsyn,
        snaprange,
        dopaxis,
        lgtaxis,
        axis,
        muse_res,
        interp_vdem=interp_vdem,
        radyn=radyn,
        onetile=onetile,
        cutresp=cutresp,
        **kwargs,
    )

    for iw in range(0, 3):
        if ddsyn.sg[iw]["BAND"] == 108:
            gain = 1.0 / 2.0
        elif ddsyn.sg[iw]["BAND"] == 171:
            gain = 1.0 / 1.25
        elif ddsyn.sg[iw]["BAND"] == 284:
            gain = 1.0 / 0.75
        moms[:, 0, iw, :, :] *= gain * timeint
    return moms


def raster_mom_scan(syn_int, nrasteps=None):
    """
    Allows, for moments, to "raster" in time assuming that each step is a full
    MUSE time step and a dense raster, i.e., 11 steps. It allows to have less
    than 37 slits as well as different size for wavelengths.

    Inputs
    -------
        syn - narray of the profiles as a function of time (nt,nm,nw,nx,ny)
                 where nt is time
                 nm moments
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                 size or a reduced one for a single slit (0).
        timeint - float  time integration in seconds

    Returns
    -------
        array - ndarray (nt_reduced,nm,nw,nx,ny)
                 nt is time
                 nm moments
                 nw are the spectral windows, i.e, 3= 108, 171 and 284,
                 nx spatial axis,
                 ny spatial axis,
                 nwvl is the spectral axis. it could be either full spectral
                 size or a reduced one for a single slit (0).
    """

    syn_int_shape = np.shape(syn_int)
    syn_raster = np.zeros(syn_int_shape)

    if nrasteps is None:
        nslit = int(np.shape(syn_raster)[3] / nrasteps) - 1
    else:
        nslit = int(np.shape(syn_raster)[3])
    for it in range(syn_int_shape[0]):
        if it > 0:
            syn_raster[it, ...] = syn_raster[it - 1, ...]
        if nrasteps is None:
            syn_raster[it, :, :, it % nslit, ...] = syn_int[it, :, :, it % nslit, ...]
        else:
            syn_raster[
                it, :, :, np.arange(nslit) * nrasteps + it % nrasteps, ...
            ] = syn_int[it, :, :, np.arange(nslit) * nrasteps + it % nrasteps, ...]

    return syn_raster


def get_vdem_moment(self, axis_name, mom=0):
    """
    Calculate moments along Doppler velocity.
    """

    axis = int(np.where(self.posaxis == axis_name)[0])

    if not hasattr(self.dopaxis, "device"):
        self.dopaxis = torch.tensor(self.dopaxis)

    v_axis = self.dopaxis**mom
    if mom > 0:
        norm = 1.0 / (self.vdem.sum(axis=axis) + 1e-30)

    if self.vdem.ndim == 4:
        if hasattr(self.vdem, "device"):
            if self.vdem.device.type == "cuda":
                v_axis = v_axis.cuda()
            cube = torch.einsum("tvij,v->tij", self.vdem.float(), v_axis.float())
            if mom > 0:
                cube = torch.einsum("tij,tij->tij", cube, norm) ** (1.0 / (mom * 1.0))
            if cube.device.type == "cuda":
                cube = cube.detach().cpu().numpy()
        else:
            cube = np.einsum("tvij,v->tij", self.vdem, v_axis)
            if mom > 0:
                cube = np.einsum("tij,tij->tij", cube, norm) ** (1.0 / (mom * 1.0))
    else:
        if hasattr(self.vdem, "device"):
            if self.vdem.device.type == "cuda":
                v_axis = v_axis.cuda()
            cube = torch.einsum("tvi,v->ti", self.vdem.float(), v_axis.float())
            if mom > 0:
                cube = torch.einsum("ti,ti->ti", cube, norm) ** (1.0 / (mom * 1.0))
            if cube.device.type == "cuda":
                cube = cube.detach().cpu().numpy()
        else:
            cube = np.einsum("tvi,v->ti", self.vdem, v_axis)
            if mom > 0:
                cube = np.einsum("ti,ti->ti", cube, norm) ** (1.0 / (mom * 1.0))
        cube = np.reshape(cube, [np.shape(cube)[0], np.shape(cube)[1], 1])
    return cube


def calc_moments(self):
    """
    Computes 0th, 1st and 2nd moments from the synthetic profiles. Output is a
    list of 3 components which corresponds to the moments. Each component is a
    3D array (nwvl, nslits x nraster, ny)

    Velocity convention: Note the doppler shift convection is the spectroscopist and MUSE response
        function convection (Not helita convention). Consequently,
        in first moment negative means towards us.
    """

    ny_muse_res = np.shape(self.syn_arr_islist[0, 0])[-1]
    nwvl = len(self.syn_sg.keys())
    noffset = np.size(self.ioffset)
    # The following allows to compute the moments from the main lines.
    zeromom = np.zeros((nwvl, noffset * self.nslitaxis, ny_muse_res))
    firstmom = np.zeros((nwvl, noffset * self.nslitaxis, ny_muse_res))
    secondmom = np.zeros((nwvl, noffset * self.nslitaxis, ny_muse_res))

    for iwvl in range(0, nwvl):
        try:
            lambdao = self.syn_sg[iwvl]["LINE_WVL"]
        except:
            if iwvl == 0:
                lambdao = 108.354
            elif iwvl == 1:
                lambdao = 171.073
            elif iwvl == 2:
                lambdao = 284.163

        # Note the doppler shift convection is the spectroscopist and MUSE response
        # function convection (Not helita convention)
        dopp = lambda_to_doppler(self.syn_sg[iwvl]["DETWV"], lambdao)

        for ii in range(0, self.nslitaxis):
            syn_troch = self.syn_arr_islist[iwvl, ii]
            zeromom_cut, firstmom_cut, secondmom_cut = calc_moments_spect(
                syn_troch, dopp[ii, :]
            )
            # I0 = \int(I)
            zeromom[iwvl, ii * noffset : (ii + 1) * noffset, :] = zeromom_cut
            # I1= \int(I v) / I0
            firstmom[iwvl, ii * noffset : (ii + 1) * noffset, :] = firstmom_cut
            # sqrt( \int I (v - I1)^2 / I0)
            secondmom[iwvl, ii * noffset : (ii + 1) * noffset, :] = secondmom_cut

    return zeromom, firstmom, secondmom


def calc_moments_extsyn(self, all_syn):
    """
    Computes 0th, 1st and 2nd moments from external synthetic profiles. Output
    is a list of 3 components which corresponds to the moments. Each component
    is a 3D array (nwvl, nslits x nraster, ny)

    Velocity convention: Note the doppler shift convection is the spectroscopist and MUSE response
        function convection (Not helita convention). Consequently,
        in first moment negative means towards us.

    INPUT
        all_syn np array (nw, nx=nslit*nraster, ny, nwvl)
    """

    # ny_muse_res = np.shape(self.syn_arr_islist[0,0])[-1]
    ny_muse_res = np.shape(all_syn)[-2]
    nx_muse_res = np.shape(all_syn)[-3]
    nwvl = len(self.syn_sg.keys())
    noffset = int(nx_muse_res / 37)
    # The following allows to compute the moments from the main lines.
    zeromom = np.zeros((nwvl, nx_muse_res, ny_muse_res))
    firstmom = np.zeros((nwvl, nx_muse_res, ny_muse_res))
    secondmom = np.zeros((nwvl, nx_muse_res, ny_muse_res))

    for iwvl in range(0, nwvl):
        try:
            lambdao = self.syn_sg[iwvl]["LINE_WVL"]
        except:
            if iwvl == 0:
                lambdao = 108.354
            elif iwvl == 1:
                lambdao = 171.073
            elif iwvl == 2:
                lambdao = 284.163

        # Note the doppler shift convection is the spectroscopist and MUSE response
        # function convection (Not helita convention)
        dopp = lambda_to_doppler(self.syn_sg[iwvl]["DETWV"], lambdao)

        for ii in range(0, self.nslitaxis):
            syn_troch = all_syn[iwvl, ii * noffset : (ii + 1) * noffset, :, :]
            zeromom_cut, firstmom_cut, secondmom_cut = calc_moments_spect(
                syn_troch, dopp[ii, :]
            )
            # I0 = \int(I)
            zeromom[iwvl, ii * noffset : (ii + 1) * noffset, :] = zeromom_cut
            # I1= \int(I v) / I0
            firstmom[iwvl, ii * noffset : (ii + 1) * noffset, :] = firstmom_cut
            # sqrt( \int I (v - I1)^2 / I0)
            secondmom[iwvl, ii * noffset : (ii + 1) * noffset, :] = secondmom_cut

    return zeromom, firstmom, secondmom


def calc_moments_extsyn_slit0(self, all_syn):
    """
    Computes 0th, 1st and 2nd moments from external synthetic profiles. Output
    is a list of 3 components which corresponds to the moments. Each component
    is a 3D array (nwvl, nslits x nraster, ny)

    Velocity convention: Note the doppler shift convection is the spectroscopist and MUSE response
        function convection (Not helita convention). Consequently,
        in first moment negative means towards us.
    nwl_red is wavelength pixels for the first slit which comes from using interp_slit_spect

    INPUT
        all_syn np array (nw, nx=nslit*nraster, ny, nwvl_red)
    """

    # ny_muse_res = np.shape(self.syn_arr_islist[0,0])[-1]
    ny_muse_res = np.shape(all_syn)[-2]
    nx_muse_res = np.shape(all_syn)[-3]
    nwvl_red = np.shape(all_syn)[-1]
    nwvl = len(self.syn_sg.keys())
    # The following allows to compute the moments from the main lines.
    zeromom = np.zeros((nwvl, nx_muse_res, ny_muse_res))
    firstmom = np.zeros((nwvl, nx_muse_res, ny_muse_res))
    secondmom = np.zeros((nwvl, nx_muse_res, ny_muse_res))

    for iwvl in range(0, nwvl):
        try:
            lambdao = self.syn_sg[iwvl]["LINE_WVL"]
        except:
            if iwvl == 0:
                lambdao = 108.354
            elif iwvl == 1:
                lambdao = 171.073
            elif iwvl == 2:
                lambdao = 284.163

        # Note the doppler shift convection is the spectroscopist and MUSE response
        # function convection (Not helita convention)
        dopp = lambda_to_doppler(self.syn_sg[iwvl]["DETWV"], lambdao)

        ii = 0
        syn_troch = all_syn[iwvl, :, :, :]
        zeromom_cut, firstmom_cut, secondmom_cut = calc_moments_spect(
            syn_troch, dopp[ii, :nwvl_red]
        )
        # I0 = \int(I)
        zeromom[iwvl, :, :] = zeromom_cut
        # I1= \int(I v) / I0
        firstmom[iwvl, :, :] = firstmom_cut
        # sqrt( \int I (v - I1)^2 / I0)
        secondmom[iwvl, :, :] = secondmom_cut

    return zeromom, firstmom, secondmom


def calc_moments_extsyn_slit0_time(self, all_syn):
    """
    Computes 0th, 1st and 2nd moments from external synthetic profiles. Output
    is a list of 3 components which corresponds to the moments. Each component
    is a 3D array (nt, nwvl, nslits x nraster, ny)

    Velocity convention: Note the doppler shift convection is the spectroscopist and MUSE response
        function convection (Not helita convention). Consequently,
        in first moment negative means towards us.

    INPUT
        all_syn np array (nw, nx=nslit*nraster, ny, nwvl)
    """

    # ny_muse_res = np.shape(self.syn_arr_islist[0,0])[-1]
    ny_muse_res = np.shape(all_syn)[-2]
    nx_muse_res = np.shape(all_syn)[-3]
    nwvl_red = np.shape(all_syn)[-1]
    nt_red = np.shape(all_syn)[0]
    nwvl = len(self.syn_sg.keys())
    # The following allows to compute the moments from the main lines.
    zeromom = np.zeros((nt_red, nwvl, nx_muse_res, ny_muse_res))
    firstmom = np.zeros((nt_red, nwvl, nx_muse_res, ny_muse_res))
    secondmom = np.zeros((nt_red, nwvl, nx_muse_res, ny_muse_res))

    for iwvl in range(0, nwvl):
        try:
            lambdao = self.syn_sg[iwvl]["LINE_WVL"]
        except:
            if iwvl == 0:
                lambdao = 108.354
            elif iwvl == 1:
                lambdao = 171.073
            elif iwvl == 2:
                lambdao = 284.163

        # Note the doppler shift convection is the spectroscopist and MUSE response
        # function convection (Not helita convention)
        dopp = lambda_to_doppler(self.syn_sg[iwvl]["DETWV"], lambdao)

        ii = 0
        syn_troch = np.transpose(all_syn[:, iwvl, :, :, :], (1, 0, 2, 3))
        zeromom_cut, firstmom_cut, secondmom_cut = calc_moments_spect(
            syn_troch, dopp[ii, :nwvl_red]
        )
        # I0 = \int(I)
        zeromom[:, iwvl, :, :] = zeromom_cut
        # I1= \int(I v) / I0
        firstmom[:, iwvl, :, :] = firstmom_cut
        # sqrt( \int I (v - I1)^2 / I0)
        secondmom[:, iwvl, :, :] = secondmom_cut

    return zeromom, firstmom, secondmom
