import os
import platform
import time
import warnings
from datetime import datetime

from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np
import torch
import xarray as xr
import subprocess
from dateutil.tz import gettz

from muse import logger

import br_py.tools as btr  # Synthesis or VDEMs (including pycuda or pytorch)

try:
    import cupy

    CUPY_INSTALLED = True
except ImportError:
    CUPY_INSTALLED = False

try:
    import torch

    TORCH_INSTALLED = True
except ImportError:
    TORCH_INSTALLED = False

if TORCH_INSTALLED:
    import torch

    GPU_FOUND = torch.cuda.is_available()
else:
    if CUPY_INSTALLED:
        import cupy

        GPU_FOUND = cupy.cuda.runtime.getDeviceCount() > 0
    else:
        GPU_FOUND = False


WEST = gettz("US/Western")

warnings.filterwarnings("ignore")


class timer:
    # Timer class to measure the real and CPU time
    def __init__(self, rtime=0, ctime=0, logger=logger):
        self.rtime = rtime
        self.ctime = ctime
        self.logger = logger

    def reset(self):
        self.rtime = 0
        self.ctime = 0

    def start(self):
        self.rtime = time.perf_counter()
        self.ctime = time.process_time()

    def stop(self):
        self.rtime = time.perf_counter() - self.rtime
        self.ctime = time.process_time() - self.ctime

    def stop_stats(self, info=""):
        self.stop()
        self.logger.info("=========")
        self.logger.info(info)
        self.logger.info("---------")
        self.logger.info(f"Real time: {self.rtime:.2f} seconds")
        self.logger.info(f"CPU time: {self.ctime:.2f} seconds")
        self.logger.info("=========")


def vdem_pipeline(
    snap,
    *,
    chosen_code="muram",
    snapname0 = 'result_prim_0',
    iz0=125,
    trac=True,
    trac_coeff=3,
    dlgtaxis=0.1,
    dvdop=10,
    xyrotation=False,
    losaxis=2,
    highrun=True,
    vdem_time_interpolation=False,
    vdem_single_raster=False,
    time_step=0.75,
    diff_kargs={"oversample_x": 3, "slit_dim": 404, "lpi": 70},
    mintg_cut=4.7,
    maxtg_cut=None,
    minvel_cut=None,
    maxvel_cut=None,
    novelcut=False,
    tg_percent=0.1,
    vel_percent=0.1,
    author="JMS",
    modelname="M-flare",
    opacity=None,
    interp_vdem=False,
    nbins_hist=60,
    dens_cut_hist=1e9,
    min_tg_hist=5e5,
):
    """
    This is forward pipeline function that goes from an existing VDEMs (several
    targets) to synthesize MUSE spectrum including core PSF, different exposure
    times and adding noise.

    Parameters
    ----------
    snap : `list` or `nd.array`
        Snapshot number list.

    chosen_code : `str`
        3D Radiative numerical code name, used to select in helita the module, by default "muram".

    iz0 : `int` o
        pixel where the photosphere is located (only for Muram models).

    snapname0 : `str`
        only for muram models, by default "result_prim_0".

    trac : `bool`
        If true, considered TRAC method, by default True.

    trac_coeff : `int`
        TRAC coeffecient, by default 3.

    dlgtaxis : `float`
        temperature bin size in log scale, by default 0.1

    dvdop : `float`
        velocity bin size, by default 10 km/s.

    xyrotation : `bool`
        90 degree rotation of the FOV, (for MUSE propuse), by default False,

    highrun : `bool`
        if the run is too big and requires to break the array in small pieces, this will do it.
        It will go slower, by default True

    vdem_time_interpolation : `bool`
        this needs to be implemented, it will allow to interporate in time to mimic rasters.

    vdem_single_raster : `bool`
        This needs to be implemented, it will allow to build a full raster in a single VDEM array.

    time_step : `float`
        the interpolation in time.

    mintg_cut :`float`
        lower temperature in logarithmic scale. by default 4.7

    maxtg_cut :`float`
        maximum temperature in logarithmic scale.
        If None, it will find out the 0.1% hotest temperature in the box, by default None

    minvel_cut :`float`
        maximum velocity.
        If None, it will find out the 0.1% smallest velocity in the box, by default None

    maxvel_cut :`float`
        maximum velocity.
        If None, it will find out the 0.1% smallest velocity in the box, by default None

    novelcut: `bool`
        if True, it will include all the EM beyond the vocity limits in the last or first bin. 
        by default False.

    tg_percent :`float`
        The limits in temperature within tg_percent %, by default 0.1%.

    vel_percent :`float`
        The limits in velocity within tg_percent %, by default 0.1%.

    opacity : `float`
        It will consider bound-free and free-free absorption from H I, He I, and He II.
        The numbe is the wavelength considered. by default None.

    interp_vdem: `float`
        It will interpolate along the LOS using pycuda.
        by default None.

    nbins_hist: `int`
        For plotting propouses for the histogram

    dens_cut_hist: `float`
        For plotting propouses for the histogram

    min_tg_hist: `float`
        For plotting propouses for the histogram

    author: `str`
        author to run the script (string in the saved files), by default "JMS"

    modelname: `str`
        saved name to identify the simulation, by default "M-flare"
    """

    steins_gate = timer(logger=logger)

    logger.info("---------------------------------")
    logger.info("LOCAL INPUTS:")
    locals_input = locals()
    for ii in locals_input:
        logger.info(f"{ii} : {locals_input[ii]}")
    logger.info("---------------------------------")
    logger.info(
        f"TORCH_INSTALLED = {TORCH_INSTALLED}, CUPY_INSTALLED = {CUPY_INSTALLED}"
    )
    logger.info("---------------------------------")

    cpuCount = os.cpu_count()
    system_info = platform.uname()
    logger.info("SYSTEM INFORMATION:")
    logger.info(f"System: {system_info.system}")
    logger.info(f"Node Name: {system_info.node}")
    logger.info(f"Release: {system_info.release}")
    logger.info(f"Version: {system_info.version}")
    logger.info(f"Machine: {system_info.machine}")
    logger.info(f"Processor: {system_info.processor}")
    logger.info("Number of CPUs in the system:", cpuCount)
    if TORCH_INSTALLED and GPU_FOUND:
        logger.info(f"GPU device name : {torch.cuda.get_device_name()}")
        logger.info(f"number of GPU devices : {torch.cuda.device_count()}")
    else:
        logger.info("No GPUs available")
    logger.info("---------------------------------")

    logger.info(f"--- Inputs ---")
    logger.info(f"snap: {snap}")
    logger.info(f"chosen_code: {chosen_code}")
    if chosen_code == "muram": 
        logger.info(f"snapname0: {snapname0}")
        logger.info(f"iz0: {iz0}")
    logger.info(f"trac: {trac}")
    logger.info(f"trac_coeff: {trac_coeff}")
    logger.info(f"dvdop: {dvdop}")
    logger.info(f"xyrotation: {dvdop}")
    logger.info(f"highrun: {highrun}")
    logger.info(f"vdem_time_interpolation: {vdem_time_interpolation}")
    logger.info(f"time_step: {time_step}")
    logger.info(f"mintg_cut: {mintg_cut}")
    logger.info(f"maxtg_cut: {maxtg_cut}")
    logger.info(f"novelcut: {novelcut}")
    logger.info(f"tg_percent: {tg_percent}")
    logger.info(f"vel_percent: {vel_percent}")
    logger.info(f"opacity: {opacity}")
    logger.info(f"interp_vdem: {interp_vdem}")
    logger.info(f"nbins_hist: {nbins_hist}")
    logger.info(f"dens_cut_hist: {dens_cut_hist}")
    logger.info(f"min_tg_hist: {min_tg_hist}")
    logger.info(f"author: {author}")
    logger.info(f"modelname: {modelname}")

    logger.info("---------------------------------")

    logger.info(f"--- Generating VDEMs ---")

    for ii_snap, i_snap in enumerate(snap):
        snapname = ".{:06d}".format(i_snap)
        logger.info(f"snap: {i_snap}")

        if chosen_code == "muram":
            ddbtr = btr.UVOTRTData(
                template=snapname,
                prim=snapname0,
                iz0=iz0,
                _class=chosen_code,
                inttostring=(lambda x: "{0:06d}".format(x)),
            )
        else:
            ddbtr = btr.UVOTRTData(snapname, _class=chosen_code)

        tg = ddbtr.trans2comm("tg", snap=i_snap)
        nel = ddbtr.trans2comm("ne", snap=i_snap)

        if losaxis == 0:
            uz = ddbtr.trans2comm("ux", snap=i_snap) / 1e5  # cm/s -> km/s We use in fro
        elif losaxis == 0:
            uz = ddbtr.trans2comm("uy", snap=i_snap) / 1e5  # cm/s -> km/s We use in fro
        else:
            uz = ddbtr.trans2comm("uz", snap=i_snap) / 1e5  # cm/s -> km/s We use in fro

        hist, bin_edges = np.histogram(
            np.reshape(np.log10(tg), np.size(tg)), density=True, bins=60
        )
        mintg = (
            np.floor(
                np.min(bin_edges[np.where(hist > np.max(hist) * tg_percent / 100.0)])
                / dlgtaxis
            )
            * dlgtaxis
        )
        maxtg = (
            np.ceil(
                np.max(bin_edges[np.where(hist > np.max(hist) * tg_percent / 100.0)])
                / dlgtaxis
            )
            * dlgtaxis
        )
        if mintg_cut is not None:
            mintg = np.max([mintg, mintg_cut])
        if maxtg_cut is not None:
            maxtg = np.min([maxtg, maxtg_cut])
        hist, bin_edges = np.histogram(
            np.reshape(uz, np.size(uz)), density=True, bins=60
        )
        minvel = (
            np.floor(
                np.min(bin_edges[np.where(hist > np.max(hist) * vel_percent / 100.0)])
                / dvdop
            )
            * dvdop
        )
        maxvel = (
            np.ceil(
                np.max(bin_edges[np.where(hist > np.max(hist) * vel_percent / 100.0)])
                / dvdop
            )
            * dvdop
        )
        if minvel_cut is not None:
            minvel = np.max([minvel, minvel_cut])
        if maxvel_cut is not None:
            maxvel = np.min([maxvel, maxvel_cut])

        logger.info(f"mintg: {mintg}")
        logger.info(f"maxtg: {maxtg}")
        logger.info(f"minvel: {minvel}")
        logger.info(f"maxvel: {maxvel}")

        fig, ax = plt.subplots(3, 1, figsize=(6, 12))

        nel_c = nel[tg > min_tg_hist]
        ax[0].hist(np.reshape(np.log10(nel_c), np.size(nel_c)), bins=nbins_hist)
        ax[0].set_xlabel("Log(Ne)")
        ax[0].set_yscale("log")
        uz_c = uz[tg > min_tg_hist]
        uz_cc = uz_c[nel_c > dens_cut_hist]
        ax[1].hist(np.reshape(uz_cc, np.size(uz_cc)), bins=nbins_hist)
        ax[1].set_xlabel("velocity")
        ax[1].set_yscale("log")
        tg_c = tg[tg > min_tg_hist]
        tg_c = tg_c[nel_c > dens_cut_hist]
        ax[2].hist(np.reshape(np.log10(tg_c), np.size(tg_c)), bins=nbins_hist)
        ax[2].set_xlabel("Log(T)")
        ax[2].set_yscale("log")
        fig.tight_layout()
        fig.savefig(
            f"hist_tg_uz_nel_{chosen_code}_{modelname}_{author}_{datetime.now(WEST).date()!s}_{i_snap}.png"
        )
        logger.info(
            f"saved histogram figures: hist_tg_uz_nel_{chosen_code}_{modelname}_{author}_{datetime.now(WEST).date()!s}_{i_snap}"
        )

        lgtaxis = np.arange(mintg, maxtg, dlgtaxis)
        syn_dopaxis = np.arange(minvel, maxvel, dvdop)

        vdem = ddbtr.from_hel2vdem(
            i_snap,
            syn_dopaxis,
            lgtaxis,
            axis=losaxis,
            xyrotation=xyrotation,
            highrun=highrun,
            trac=trac,
            trac_coeff=trac_coeff,
            opacity=opacity,
            interp_vdem=interp_vdem,
        )

        vdem.to_zarr(
            f"vdem_{chosen_code}_{modelname}_los{losaxis}_{author}_{datetime.now(WEST).date()!s}_{i_snap}.zarr"
        )
        logger.info(
            f"saved files: vdem_{chosen_code}_{modelname}_los{losaxis}_{author}_{datetime.now(WEST).date()!s}_{i_snap}"
        )
        os.system(f"tar cvfz vdem_{chosen_code}_{modelname}_los{losaxis}_{author}_{datetime.now(WEST).date()!s}_{i_snap}.zarr.tar.gz vdem_{chosen_code}_{modelname}_los{losaxis}_{author}_{datetime.now(WEST).date()!s}_{i_snap}.zarr")

        fig, ax = plt.subplots(1, 2, figsize=(12, 6))
        vdem.vdem.sum(dim=["logT", "vdop"]).plot.imshow(
            ax=ax[0], norm=colors.PowerNorm(0.2)
        )
        zeroth = vdem.vdem.sum(dim=["logT", "vdop"])
        first = xr.dot(vdem.vdop, vdem.vdem) / zeroth
        first = first.sum(dim=["logT"])
        first.plot.imshow(ax=ax[1], cmap="bwr")
        fig.tight_layout()
        fig.savefig(
            f"vdem_maps_{chosen_code}_{modelname}_{author}_{datetime.now(WEST).date()!s}_{i_snap}.png"
        )
        logger.info(
            f"saved vdem figures: vdem_{chosen_code}_{modelname}_los{losaxis}_{author}_{datetime.now(WEST).date()!s}_{i_snap}"
        )
