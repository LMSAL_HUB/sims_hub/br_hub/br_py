.. br_pyDocs documentation master file, created by
   sphinx-quickstart on Mon Jun  4 10:12:08 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to br_hub/br_py documentation
============================================
.. only:: html

   By Juan Martinez-Sykora, Carmel Baharav, & Tiago M. D. Pereira.

   Last updated on |today|.

Contents:

.. toctree::
   :maxdepth: 3

   br_py.ipynb
